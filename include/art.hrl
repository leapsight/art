-define(MAX_PREFIX_LEN, 10).
%% We use the ASCII unit separator ($\31) which was designed to separate
%% fields of a record.
-define(KEY_SEP, $\31).

-define(PACK_KEY(Tuple),
    iolist_to_binary(
        lists:join(
            ?KEY_SEP,
            [
                begin
                    case E of
                        '_' ->
                            <<>>;
                        E when is_binary(E) ->
                            E;
                        _ ->
                            error(badarg)
                    end
                end || E <- tuple_to_list(Tuple)
            ]
        )
    )
).


-type version()             ::  non_neg_integer().

%% A single info per trie, stored on main_tab
-record(trie, {
    id                      ::  {ets:tab(), version()},
    tab                     ::  ets:tab(),
    leaf_tab                ::  ets:tab(),
    size = 0                ::  non_neg_integer(),
    root                    ::  art_node:e()
}).

-type trie()                ::  #trie{}.
