-module(art_server).
-behaviour(gen_server).

-include_lib("kernel/include/logger.hrl").

-if(?OTP_RELEASE >= 25).
    -define(SPAWN_OPTS,
        [
            {
                message_queue_data,
                application:get_env(art, message_queue_data, off_heap)
            }
        ]
    ).
-else.
    -define(SPAWN_OPTS,
        [{message_queue_data, on_heap}]
    ).
-endif.

-record(state, {
    trie         ::  art:t()
}).

%% API
-export([start/1]).
-export([start_link/1]).
-export([stop/1]).

-export([add/2]).
-export([delete/1]).
-export([delete/2]).
-export([find/2]).
-export([find_matches/2]).
-export([find_matches/3]).
-export([first/1]).
-export([fold/3]).
-export([get/2]).
-export([last/1]).
-export([lookup/2]).
-export([match/2]).
-export([match/3]).
-export([set/3]).
-export([size/1]).
-export([store/3]).
-export([take/2]).
-export([to_list/1]).

%% GEN_SERVER CALLBACKS
-export([init/1]).
-export([handle_info/2]).
-export([terminate/2]).
-export([code_change/3]).
-export([handle_call/3]).
-export([handle_cast/2]).




%% =============================================================================
%% API
%% =============================================================================




%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
start(Name) when is_atom(Name) ->
    Opts = [{spawn_opt, ?SPAWN_OPTS}],
    gen_server:start({local, Name}, ?MODULE, [Name], Opts).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
start_link(Name) when is_atom(Name) ->
    Opts = [{spawn_opt, ?SPAWN_OPTS}],
    gen_server:start_link({local, Name}, ?MODULE, [Name], Opts).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
stop(Trie) ->
    gen_server:stop(Trie).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
size(Trie) ->
    art:size(Trie).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec set(Key :: art:key(), Value :: art:value(), Trie :: art:t()) -> ok.

set(Key, Value, Trie) ->
    gen_server:call(Trie, {set, Key, Value}).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec store(Key :: art:key(), Value :: art:value(), Trie :: art:t()) ->
    OldValue :: art:value() | undefined.

store(Key, Value, Trie) ->
    gen_server:call(Trie, {store, Key, Value}).



%% -----------------------------------------------------------------------------
%% @doc Stores all the key-value pairs in the list.
%% @end
%% -----------------------------------------------------------------------------
-spec add([{art:key(), art:value()}], art:t()) -> ok | no_return().

add(KeyValues, Trie) ->
    gen_server:call(Trie, {add, KeyValues}).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec get(Key :: art:key(), Trie :: art:t()) -> art:value() | not_found.

get(Key, Trie) ->
    gen_server:call(Trie, {get, Key}).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec find(Key :: art:key(), Trie :: art:t()) -> art:value() | error.

find(Key, Trie) ->
    gen_server:call(Trie, {find, Key}).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec delete(Trie :: art:t()) -> boolean().

delete(Trie) ->
    gen_server:call(Trie, delete).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec delete(Key :: art:key(), Trie :: art:t()) -> ok.

delete(Key, Trie) ->
    gen_server:call(Trie, {delete, Key}).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec take(Key :: art:key(), Trie :: art:t()) -> {value, art:value()} | error.

take(Tuple, Trie) ->
    gen_server:call(Trie, {take, Tuple}).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec first(art:t()) ->
    {art_node:leaf(), {art_node:id(), art_node:index()}}
    | error | no_return().

first(Trie) ->
    gen_server:call(Trie, first).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec last(art:t()) -> art:leaf() | error | no_return().

last(Trie) ->
    gen_server:call(Trie, last).



%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec lookup(art:key(), art:t()) -> [{art:key(), art:value()}].

lookup(Key, Trie) ->
    gen_server:call(Trie, {lookup, Key}).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec match(art:key(), art:t()) -> [{art:key(), art:value()}].

match(Key, Trie) ->
    gen_server:call(Trie, {match, Key}).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec match(art:key(), art:t(), art:match_opts()) -> [{art:key(), art:value()}].

match(Key, Trie, Opts) ->
    gen_server:call(Trie, {match, Key, Opts}).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec find_matches(art_node:key(), art:t()) -> [{art:key(), art:value()}].

find_matches(Key, Trie) ->
    gen_server:call(Trie, {find_matches, Key}).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec find_matches(art:key(), ets:match_spec(), art:t()) ->
    [{art:key(), art:value()}].

find_matches(Key, MS, Trie) ->
    gen_server:call(Trie, {find_matches, Key, MS}).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
fold(Trie, Fun, Acc0) ->
    gen_server:call(Trie, {fold, Fun, Acc0}).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
to_list(Trie) ->
    gen_server:call(Trie, to_list).



%% =============================================================================
%% GEN_SERVER CALLBACKS
%% =============================================================================


init([Name]) ->
    Trie = art:new(Name, []),
    {ok, #state{trie = Trie}}.


handle_call({set, Key, Value}, _From, State) ->
    Res = try
        art:set(Key, Value, State#state.trie)
    catch
        _:Reason ->
            {error, Reason}
    end,
    {reply, Res, State};

handle_call({store, Key, Value}, _From, State) ->
    Res = try
        art:store(Key, Value, State#state.trie)
    catch
        _:Reason:Stacktrace ->
            ?LOG_ERROR(#{
                reason => Reason,
                stacktrace => Stacktrace
            }),
            {error, Reason}
    end,
    {reply, Res, State};

handle_call({add, KeyValues}, _From, State) ->
    Res = try
        art:add(KeyValues, State#state.trie)
    catch
        _:Reason ->
            {error, Reason}
    end,
    {reply, Res, State};

handle_call(delete, _From, State) ->
    Res = try
        art:delete(State#state.trie)
    catch
        _:Reason ->
            {error, Reason}
    end,
    {stop, normal, Res, State};

handle_call({delete, Key}, _From, State) ->
    Res = try
        art:delete(Key, State#state.trie)
    catch
        _:Reason ->
            {error, Reason}
    end,
    {reply, Res, State};

handle_call({take, Key}, _From, State) ->
    Res = try
        art:take(Key, State#state.trie)
    catch
        _:Reason ->
            {error, Reason}
    end,
    {reply, Res, State};

handle_call({get, Key}, _From, State) ->
    Res = try
        art:get(Key, State#state.trie)
    catch
        _:Reason ->
            {error, Reason}
    end,
    {reply, Res, State};

handle_call({find, Key}, _From, State) ->
    Res = try
        art:find(Key, State#state.trie)
    catch
        _:Reason ->
            {error, Reason}
    end,
    {reply, Res, State};

handle_call({lookup, Key}, _From, State) ->
    Res = try
        art:lookup(Key, State#state.trie)
    catch
        _:Reason ->
            {error, Reason}
    end,
    {reply, Res, State};

handle_call(first, _From, State) ->
    Res = try
        art:first(State#state.trie)
    catch
        _:Reason ->
            {error, Reason}
    end,
    {reply, Res, State};

handle_call(last, _From, State) ->
    Res = try
        art:last(State#state.trie)
    catch
        _:Reason ->
            {error, Reason}
    end,
    {reply, Res, State};

handle_call({match, Key}, _From, State) ->
    Res = try
        art:match(Key, State#state.trie)
    catch
        _:Reason ->
            {error, Reason}
    end,
    {reply, Res, State};

handle_call({match, Key, Opts}, _From, State) ->
    Res = try
        art:match(Key, State#state.trie, Opts)
    catch
        _:Reason ->
            {error, Reason}
    end,
    {reply, Res, State};

handle_call({find_matches, Key}, _From, State) ->
    Res = try
        art:find_matches(Key, State#state.trie)
    catch
        _:Reason ->
            {error, Reason}
    end,
    {reply, Res, State};

handle_call({find_matches, Key, MS}, _From, State) ->
    Res = try
        art:find_matches(Key, MS, State#state.trie)
    catch
        _:Reason ->
            {error, Reason}
    end,
    {reply, Res, State};

handle_call({fold, Fun, Acc0}, _From, State) ->
    Res = try
        art:fold(State#state.trie, Fun, Acc0)
    catch
        _:Reason ->
            {error, Reason}
    end,
    {reply, Res, State};

handle_call(to_list, _From, State) ->
    Res = try
        art:to_list(State#state.trie)
    catch
        _:Reason ->
            {error, Reason}
    end,
    {reply, Res, State};

handle_call(Event, From, State) ->
    ?LOG_ERROR(#{
        description => "Unknown event",
        reason => unsupported_event,
        event => Event,
        from => From
    }),
    {noreply, State}.


handle_cast(Event, State) ->
    ?LOG_ERROR(#{
        description => "Unknown event",
        reason => unsupported_event,
        event => Event
    }),
    {noreply, State}.

handle_info({'ETS-TRANSFER', _, _, _}, State) ->
    {noreply, State};

handle_info(Info, State) ->
    ?LOG_DEBUG(#{
        description => "Unknown event",
        reason => unsupported_event,
        event => Info
    }),
    {noreply, State}.


terminate(normal, _State) ->
    ok;
terminate(shutdown, _State) ->
    ok;
terminate({shutdown, _}, _State) ->
    ok;
terminate(_Reason, _State) ->
    ok.


code_change(_OldVsn, State, _Extra) ->
    {ok, State}.