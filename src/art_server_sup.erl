-module(art_server_sup).
-behaviour(supervisor).

-export([start_trie/1]).
-export([start_link/0]).
-export([init/1]).



%% =============================================================================
%% API
%% =============================================================================


start_trie(Name) ->
    supervisor:start_child(?MODULE, [Name]).

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).


%% =============================================================================
%% SUPERVISOR CALLBACKS
%% =============================================================================


init([]) ->
    Spec = #{
        id => art_server,
        start => {
            art_server,
            start_link,
            []
        },
        restart => transient,
        shutdown => 5000,
        type => worker,
        modules => [art_server]
    },
    Specs = {{simple_one_for_one, 0, 1}, [Spec]},
    {ok, Specs}.