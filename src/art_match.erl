%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-module(art_match).
-include("art.hrl").

-export([do/3]).
-export([matches/4]).



%% =============================================================================
%% API
%% =============================================================================



%% -----------------------------------------------------------------------------
%% @doc Searches for all entries where their key matches Pattern.
%% A pattern can be a binary or a tuple of binary terms. Using a binary is
%% semantically equivalent to using a 1-element tuple.
%%
%% Term can be a key or a pattern. If it is a key it is regarded as a prefix
%% pattern.
%%
%% The function returns a list of tuples with form {Key, Value} where Term is a
%% prefix of Key or a pattern that matches Key.
%%
%% @end
%% -----------------------------------------------------------------------------
-spec do(Pattern :: binary() | {binary()}, trie(), art:match_options()) ->
    [{art:key(), art:value()}] | no_return().

do(Pattern, Trie, Opts) when is_tuple(Pattern) ->
    do(?PACK_KEY(Pattern), Trie, Opts);

do(Pattern, Trie, #{mode := exact}) ->
    {Prefix, Rest} = art_utils:unpack_pattern(Pattern),
    #trie{root = Node} = Trie,
    Res = match(Prefix, 0, Rest, exact, {Trie, Node, []}),
    lists:reverse(Res);

do(Pattern, Trie, #{mode := pattern}) ->
    #trie{root = Node} = Trie,
    Res = case art_utils:is_wildcard_pattern(Pattern) of
        {false, {Prefix, Rest}} ->
            match(Prefix, 0, Rest, prefix, {Trie, Node, []});
        {true, {wildcard, Terms, _Prefix, Rest}} ->
            match_wildcard(Terms, 0, Rest, {Trie, Node, []})
    end,
    lists:reverse(Res);

do(_, _, #{mode := wildcard}) ->
    error({not_implemented, wildcard}).



%% =============================================================================
%% PRIVATE
%% =============================================================================



%% -----------------------------------------------------------------------------
%% @private
%% @doc Function used by match/2
%% @end
%% -----------------------------------------------------------------------------
match(_, _, _, _, {_, undefined, Acc}) ->
    Acc;

match(Prefix, _Depth, Rest, Mode, {_, {Key, Value}, Acc}) ->
    case matches(Key, Prefix, Rest, Mode) of
        true ->
            [{art_utils:maybe_unpack(Key), Value} | Acc];

        false ->
            Acc
    end;

match(Prefix, Depth0, Rest, Mode, {Trie, NodeId, Acc0})
when byte_size(Prefix) =:= Depth0 ->
    Fun = fun(K, V, Acc) ->
        case matches(K, Prefix, Rest, Mode) of
            true ->
                [{art_utils:maybe_unpack(K), V} | Acc];
            false ->
                %% TODO Check if we should break here!
                throw({break, Acc})
        end
    end,
    {_, _, Acc2} = do_fold(NodeId, {Trie, Fun, Acc0}),
    Acc2;

match(Prefix, Depth0, Rest, Mode, {Trie, NodeId, Acc0}) ->
    PrefixLen0 = art_node:prefix_len(Trie, NodeId),

    case PrefixLen0 > 0 of
        true ->
            Res = art:prefix_mismatch(Trie, NodeId, Prefix, Depth0, PrefixLen0),

            case Res of
                0 ->
                    %% There is no match, terminate
                    Acc0;

                PrefixLen1 when PrefixLen1 + Depth0 =:= byte_size(Prefix) ->
                    %% We fold this node
                    Fun = fun(Key, Value, Acc) ->
                        case matches(Key, Prefix, Rest, Mode) of
                            true ->
                                [{art_utils:maybe_unpack(Key), Value} | Acc];
                            false ->
                                Acc
                        end
                    end,
                    {_, _, Acc1} = do_fold(NodeId, {Trie, Fun, Acc0}),
                    Acc1;

                PrefixLen1 ->
                    %% There is a partial match, increase depth and continue
                    Depth1 = Depth0 + PrefixLen1,
                    <<_:Depth1/binary, Index:8, _/binary>> = Prefix,
                    Child = art_node:get(Trie, NodeId, Index),
                    match(Prefix, Depth1 + 1, Rest, Mode, {Trie, Child, Acc0})
            end;

        false ->
            <<_:Depth0/binary, Index:8, _/binary>> = Prefix,
            Child = art_node:get(Trie, NodeId, Index),
            match(
                Prefix, Depth0 + 1, Rest, Mode, {Trie, Child, Acc0}
            )
    end.


%% -----------------------------------------------------------------------------
%% @private
%% @doc Returns true if `Pattern' matches `Key'
%% `Mode' allows to control the semantics of the check.
%%
%% If `mode == prefix' the check returns `true' only when the second term is a
%% prefix of the first.
%%
%% If `mode == wildcard' the check returns `true' only when the `Key'
%% matches the wildcard `Pattern'.
%%
%% If `mode == exact' it also returns `true' in case the two terms fully match
%% or when `Key' is a composite key and its first element is equal to `Pattern'
%% and `PatternRest' subsumes the rest of the remaining key elements.
%% @end
%% -----------------------------------------------------------------------------
-spec matches(
    Key :: binary(),
    Pattern :: binary(),
    PatternSuffix :: binary(),
    Mode :: exact | prefix | wildcard) -> boolean().

matches(Key, Pattern, PatternSuffix, Mode) ->
    Size = byte_size(Pattern),

    case {Key, PatternSuffix} of
        {<<Pattern:Size/binary, PatternSuffix/binary>>, PatternSuffix}
        when Mode == exact ->
            %% Completely matches simple key or composite key
            true;

        {<<Pattern:Size/binary, ?KEY_SEP, Suffix/binary>>, PatternSuffix}
        when Mode == exact ->
            %% We exactly matched the first key component, now we need to match
            %% the rest. This allows to match "wildcard" components
            %% e.g. fields using '_' or <<>>
            matches_suffix(Suffix, PatternSuffix, strict);

        {<<Pattern:Size/binary, _Suffix/binary>>, <<>>}
        when Mode == prefix ->
            %% Pattern is a prefix of key
            true;

        {<<Pattern:Size/binary, ?KEY_SEP, _/binary>>, <<?KEY_SEP>>}
        when Mode == prefix ->
            true;

        {<<Pattern:Size/binary, ?KEY_SEP, Suffix/binary>>, PatternSuffix}
        when Mode == prefix ->
            %% Pattern exactly matches Key, but we need to continue matching
            %% suffix
            matches_suffix(Suffix, PatternSuffix, relaxed);


        {<<Pattern:Size/binary, _Suffix/binary>>, <<?KEY_SEP>>}
        when Mode == prefix ->
            false;

        {<<Pattern:Size/binary, _Suffix/binary>>, _} when Mode == prefix ->
            false;

        {<<Pattern:Size/binary, Suffix/binary>>, _} when Mode == wildcard ->
            %% We matched the prefix but there are more chars till the separator
            %% So we drop all chars
            matches_suffix(Suffix, PatternSuffix, relaxed);

        _ ->
            %% Not a prefix
            false
    end.


%% @private
-spec matches_suffix(binary(), binary(), strict | relaxed) -> boolean().

matches_suffix(<<>>, <<>>, _) ->
    true;

matches_suffix(_, <<>>, strict) ->
    false;

matches_suffix(_, <<>>, relaxed) ->
    false;

matches_suffix(Suffix, PatternSuffix0, Mode) ->
    %% Trim only the first KEY_SEP
    PatternSuffix = string:prefix(PatternSuffix0, <<?KEY_SEP>>),
    L1 = binary:split(Suffix, <<?KEY_SEP>>, [global]),
    L2 = binary:split(PatternSuffix, <<?KEY_SEP>>, [global]),
    art_utils:subsumes(L2, L1, Mode).


%% -----------------------------------------------------------------------------
%% @private
%% @doc
%% @end
%% -----------------------------------------------------------------------------
match_wildcard(_L, 0, _Rest, {_Trie, _Node, _}) ->
    error(not_implemented).


%% -----------------------------------------------------------------------------
%% @private
%% @doc Function used by fold/3
%% @end
%% -----------------------------------------------------------------------------
do_fold(Child, {Trie, Fun, Acc0}) ->
    do_fold(Child, #{}, {Trie, Fun, Acc0}).


%% -----------------------------------------------------------------------------
%% @private
%% @doc Function used by fold/3
%% @end
%% ---------------------
do_fold({K, V}, _, {Trie, Fun, Acc0}) ->
    Acc = try
        Fun(K, V, Acc0)
    catch
        throw:{break, Acc1} -> Acc1;
        throw:{not_a_pattern, Acc1} -> Acc1
    end,
    {Trie, Fun, Acc};

do_fold(NodeId, Opts, {Trie, Fun, Acc} = State) when is_map(Opts) ->
    case ets:lookup(Trie#trie.tab, NodeId) of
        [] ->
            State;
        [Node] ->
            art_node:foldl(Node, node_fold(Opts), Opts, {Trie, Fun, Acc})
    end.


%% @private
node_fold(Opts) ->
    fun(_Index, Value, {Trie, Fun, Acc}) ->
        do_fold(Value, Opts, {Trie, Fun, Acc})
    end.

