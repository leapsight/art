-module(art_sup).
-behaviour(supervisor).

-define(CHILD(I, Type, Args, Restart, Timeout), #{
    id => I,
    start => {I, start_link, Args},
    restart => Restart,
    shutdown => Timeout,
    type => Type,
    modules => [I]
}).

-define(CHILD(I, Type, Args), ?CHILD(I, Type, Args, permanent, 5000)).


-export([start_link/0]).
-export([init/1]).



%% =============================================================================
%% API
%% =============================================================================


start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).


%% =============================================================================
%% SUPERVISOR CALLBACKS
%% =============================================================================


init([]) ->
    RestartStrategy = {one_for_one, 10, 10},
    Children = [
        ?CHILD(art_server_sup, supervisor, [], permanent, infinity),
        ?CHILD(art_table_owner, worker, [], permanent, 5000)
    ],
    {ok, {RestartStrategy, Children}}.