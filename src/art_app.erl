-module(art_app).
-behaviour(application).

-export([start/2]).
-export([stop/1]).


start(_Type, _Args) ->
    case art_sup:start_link() of
        {ok, Pid} ->
            %% More init here
            {ok, Pid};
        Other ->
            Other
    end.


stop(_State) ->
    ok.





%% =============================================================================
%% PRIVATE
%% =============================================================================
