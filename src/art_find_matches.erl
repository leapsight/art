%% -----------------------------------------------------------------------------
%% @doc A module implementing the search strategy for wildcard matching
%% @end
%% -----------------------------------------------------------------------------
-module(art_find_matches).
-include("art.hrl").

-define(IS_EVEN(X), (X >= 0 andalso (X band 1) == 0)).
-define(IS_ODD(X), (X > 0 andalso not ?IS_EVEN(X))).

-record(state, {
    %% Immutable
    trie                    ::  art:t(),
    root                    ::  art_node:e(),
    term                    ::  binary(),
    parts                   ::  tuple(),
    first = <<>>            ::  binary(),
    suffix = []             ::  [binary()],
    ms                      ::  optional(ets:comp_match_spec()),
    %% Mutable
    part_index = 1          ::  optional(pos_integer()),
    node                    ::  art_node:id(),
    depth = 0               ::  art:depth(),
    term_depth = 0          ::  art:depth(),
    wildcard_iter           ::  wildcard_iter()
}).

-type state()               ::  #state{}.
-type optional(T)           ::  T | undefined.
-type wildcard_iter()       ::  #{
                                    n := non_neg_integer(),
                                    i := non_neg_integer(),
                                    skip := non_neg_integer()
                                }.

-export([do/3]).


%% =============================================================================
%% API
%% =============================================================================



%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec do([binary()], art:find_matches_opts(), art:t()) ->
    [{art:key(), art:value()}] | no_return().

do([], _, _) ->
    [];

do([SearchTerm|Suffix], Opts, Trie) ->
    %% Term cannot be a pattern
    {Res, _} = art_utils:is_pattern(SearchTerm),
    Res =:= false orelse error(badarg),


    case art:root(Trie) of
        undefined ->
            %% The trie is empty
            [];

        Root ->
            {First, From} =
                case maps:get(first, Opts, undefined) of
                    undefined ->
                        {<<>>, SearchTerm};
                    First0 ->
                        case string:prefix(SearchTerm, First0) of
                            nomatch ->
                                error({badarg, first});
                            From0 ->
                                {First0, From0}
                        end
                end,

            Parts = list_to_tuple(binary:split(From, <<$.>>, [global])),

            MS = maps:get(match_spec, Opts, undefined),
            CMS = maybe_compile_ms(MS),
            WildcardIter = wildcard_iter(tuple_size(Parts)),

            State0 = #state{
                trie = Trie,
                root = Root,
                term = SearchTerm,
                first = First,
                parts = Parts,
                suffix = Suffix,
                ms = CMS,
                wildcard_iter = WildcardIter,
                node = Root
            },

            case search_move(First, State0) of
                {ok, #state{term_depth = N} = State1} ->
                    State = State1#state{
                        term_depth = N + byte_size(First)
                    },
                    lists:reverse(search(State, []));
                error ->
                    []
            end
    end.



%% =============================================================================
%% PRIVATE
%% =============================================================================



%% -----------------------------------------------------------------------------
%% @private
%% @doc
%% @end
%% -----------------------------------------------------------------------------
maybe_compile_ms(undefined) ->
    undefined;

maybe_compile_ms(L) ->
    ets:match_spec_compile(L).


%% -----------------------------------------------------------------------------
%% @private
%% @doc
%% @end
%% -----------------------------------------------------------------------------
search(#state{part_index = I, parts = T}, Acc) when I == tuple_size(T) + 1 ->
    Acc;

search(#state{} = State0, Acc0) ->
    %% We are starting the search from root or a new part.

    %% 1. We search for the wildcard and exact matches with current depth.
    %% E.g. term is "com.myapp.event.one" and term_depth is 0 we search for ".",
    %% if depth is 3 we search for "com.".
    %% We need to keep the wildcard_iter state for step (3) but we ignore the
    %% rest of the state as we want to start from current depth again.
    {#state{wildcard_iter = Iter}, Acc1} = maybe_search_wildcard(State0, Acc0),

    case search_prefixes(undefined, State0, Acc1) of
        {ok, State1, Acc2} ->
            %% 2. Continue with remaining parts
            PartIndex = State1#state.part_index,
            State = State1#state{
                wildcard_iter = Iter,
                part_index = PartIndex + 1
            },
            search(State, Acc2)
    end.


%% -----------------------------------------------------------------------------
%% @private
%% @doc Searches for matching terms (exact, prefixes and wildcard matches)
%% iterating N times where N is the size in bytes of the current part being
%% considered.
%%
%% For example: when `Term = <<"com.myapp.event.one">>' and considering
%% the part `<<"com">>' we want to search for the following keys:
%% ```
%% [<<"*">>, <<"c*">>, <<"co*">>, <<"com*">>]
%% '''
%% Equally, when considering the next part `<<"myapp">>' we want to search for
%% the following keys:
%% ```
%% [
%%      <<"com.*">>,
%%      <<"com.m*">>,
%%      <<"com.my*">>,
%%      <<"com.mya*">>,
%%      <<"com.myap*">>,
%%      <<"com.myapp*">>
%% ]
%% '''
%% @end
%% -----------------------------------------------------------------------------
search_prefixes(undefined, State0, Acc0) ->
    %% We are starting the search from root or a new part.
    Term = State0#state.term,
    TermDepth = State0#state.term_depth,
    Parts = State0#state.parts,
    PartIndex = State0#state.part_index,

    %% 1. We search for the prefix with current depth.
    %% E.g. term is "com.myapp.event.one" and term_depth is 0 we search for "*",
    %% if depth is 4 we search for "com.*".
    %% We ignore the new state as we want to start from current depth again in
    %% step (2).
    Prefix = <<(binary:part(Term, {0, TermDepth}))/binary, $*>>,
    {ok, _, Acc1} = search_aux(Prefix, State0, Acc0),

    %% 3. We start iterating over the chars in current part
    N = byte_size(element(PartIndex, Parts)),
    State = State0#state{term_depth = TermDepth + 1},
    search_prefixes(N, State, Acc1);

search_prefixes(N, State0, Acc0) when N > 0 ->
    Term = State0#state.term,
    TermDepth = State0#state.term_depth,

    Key = binary:part(Term, {0, TermDepth}),

    %% We first search for the prefix with depth without adding the $*.
    case search_move(Key, State0) of
        {ok, State1} ->
            %% We search for the prefix with depth + $*, where we iteratively
            %% and recursively increase depth (N + 1) up to byte_size(Part) so
            %% that we consume each character in Part.
            %% E.g. term is "com.myapp.event.one" and depth is 0 we search for
            %% "c*", "co*", "com*".
            %% We ignore the new state as we want to start from current depth
            %% again when we consider the next part.
            {ok, State2, Acc1} = search_aux(<<Key/binary, $*>>, State1, Acc0),

            %% We do not search for wildcards here as we need to consume all the
            %% characters in the current part first.
            State = State2#state{
                term_depth = TermDepth + 1
            },
            search_prefixes(N - 1, State, Acc1);
        error ->
            %% No more matches possible
            {ok, State0, Acc0}
    end;

search_prefixes(N, #state{parts = T} = State0, Acc0)
when N =:= 0 andalso 1 == tuple_size(T)  ->
    %% This is the last part so we search for the exact match, no more prefixes
    %% or wildcards from here.
    Term = State0#state.term,
    search_aux(Term, State0, Acc0);

search_prefixes(N, State, Acc) when N =:= 0 ->
    %% We have finished consuming the characters in the current part.
    %% We cannot search for the exact match until we consume all parts.
    %% We will search for wildcards when we start consuming the next part, not
    %% now.
    {ok, State, Acc}.


%% -----------------------------------------------------------------------------
%% @private
%% @doc
%% @end
%% -----------------------------------------------------------------------------
maybe_search_wildcard(
    #state{wildcard_iter = #{i := I, n := N}} = State0,
    Acc
) when I =:= 0 andalso N > 1 ->
    Term = State0#state.term,
    TermDepth = State0#state.term_depth,
    Index = State0#state.part_index,

    FirstKey = <<(binary:part(Term, {0, TermDepth}))/binary, $.>>,

    case search_move(FirstKey, State0) of
        {ok, State} ->
            %% We only search wildcards if we found the prefix $. at this level
            search_wildcard(State, Acc);
        error when Index > 1 ->
            Iter = State0#state.wildcard_iter,
            State = State0#state{
                wildcard_iter = wildcard_iter_skip(Iter, Index)
            },
            {State, Acc};
        error ->
            {State0, Acc}
    end;

maybe_search_wildcard(State, Acc) ->
    search_wildcard(State, Acc).


%% -----------------------------------------------------------------------------
%% @private
%% @doc
%% @end
%% -----------------------------------------------------------------------------
search_wildcard(State0, Acc0) ->
    %% We call this at root (not wildcards consumed yet) or when we have
    %% consumed the first part. In both cases the iterator i is even (starting
    %% at 0).
    %% When asking the next wildcard (i is odd) we can continue on the same
    %% branch but only once, when we ask for the next one after (i will be odd)
    %% we will need to backtrack. The beauty of binary numbers.
    %% An example:
    %% (1) <<"...">>        i = 0 (odd)
    %% (2) <<"...one">>     i = 1 (even)  we can continue searching
    %% (3) <<"..event.">>   i = 2 (odd)   we need to backtrack
    %% In (3) we need to backtrack because we will never find <<"..event.">>
    %% after <<"...one">>
    case next_wildcard(State0) of
        {ok, W1, #state{wildcard_iter = #{i := I} = Iter0} = State1}
        when ?IS_ODD(I) ->
            case search_aux(W1, State1, Acc0, strict) of
                {ok, State2, Acc1} ->
                    case next_wildcard(State2) of
                        {ok, W2, #state{} = State3} ->
                            {ok, State4, Acc2} = search_aux(W2, State3, Acc1),
                            %% We backtrack to State0
                            State = State0#state{
                                wildcard_iter = State4#state.wildcard_iter
                            },
                            search_wildcard(State, Acc2);
                        {skip, State3} ->
                            search_wildcard(State3, Acc1);

                        {error, State3} ->
                            {State3, Acc1}
                    end;
                {error, State2, Acc1} ->
                    %% We need to advance the wildcard iterator to account for
                    %% the next one that we are skipping
                    Iter = Iter0#{i => I + 1},
                    State = State2#state{wildcard_iter = Iter},
                    search_wildcard(State, Acc1)
            end;
        {error, State1} ->
            {State1, Acc0}
    end.


%% -----------------------------------------------------------------------------
%% @private
%% @doc
%% @end
%% -----------------------------------------------------------------------------
search_move(<<>>, #state{} = State0) ->
    {ok, State0};

search_move(_, #state{node = {_, _}} = State0) ->
    {ok, State0};

search_move(Key, #state{node = NodeId} = State0) when is_integer(NodeId) ->
    Trie = State0#state.trie,
    NodeId = State0#state.node,
    Depth0 = State0#state.depth,

    {PLen0, LCP} = art_node:longest_common_prefix(Trie, NodeId, Key, Depth0),
    PLen = min(?MAX_PREFIX_LEN, PLen0),
    Depth = Depth0 + PLen0,

    case LCP of
        LCP when LCP == 0 andalso byte_size(Key) =:= Depth ->
            error;

        LCP when LCP =:= PLen ->
            Index = art_utils:index(Key, Depth),

            case art_node:get(Trie, NodeId, Index) of
                undefined ->
                    %% There is no exact match but there are more keys starting
                    %% with Key as prefix.
                    %% We stay on same node
                    {ok, State0};

                {_, _} ->
                    %% An exact match of the Key we are searching, there might
                    %% be more keys starting with Key as prefix.
                    %% We stay on same node
                    {ok, State0};
                Child ->
                    %% We move to child node
                    State = State0#state{
                        depth = Depth + 1,
                        node = Child
                    },
                    {ok, State}
            end;

        LCP when PLen > LCP andalso (LCP > 0 orelse PLen == 1)  ->
            %% There is no exact match but there are more keys starting
            %% with Key as prefix. e.g. a compound key with KEY_SEP
            %% We stay on same node

            {ok, State0};

        _ ->
            error
    end.


%% -----------------------------------------------------------------------------
%% @private
%% @doc
%% @end
%% -----------------------------------------------------------------------------
search_aux(Key, State0, Acc0) ->
    search_aux(Key, State0, Acc0, relaxed).

%% -----------------------------------------------------------------------------
%% @private
%% @doc
%% @end
%% -----------------------------------------------------------------------------
search_aux(Key, State0, Acc0, Mode)
when Mode == strict orelse Mode == relaxed ->
    case search_next(Key, State0) of
        {ok, {{LeafKey, _} = Leaf, Idx}, _Depth1} ->
            %% An exact match of the Key we are searching, now we need to
            %% continue searching for suffixes i.e. Key ++ ?KEY_SEP ++ ...
            MS = State0#state.ms,
            Suffix = State0#state.suffix,
            Opts = #{},

            Acc =
                try matches(Key, Leaf, Opts, MS, Suffix) of
                    {ok, Unpacked} ->
                        Acc1 = [Unpacked|Acc0],
                        maybe_search_suffix(
                            Key, LeafKey, Idx, State0, Acc1, Opts
                        );
                    error ->
                        maybe_search_suffix(
                            Key, LeafKey, Idx, State0, Acc0, Opts
                        )
                catch
                    throw:break ->
                        %% This case should not happen here as we have matches
                        %% Key with Leaf.Key but we capture it anyway
                        Acc0
                end,

            {ok, State0, Acc};

        {ok, {undefined, _Idx}, _Depth1}  ->
            Acc = fold(Key, State0, Acc0, #{}),
            {ok, State0, Acc};

        {ok, {Child, _}, Depth1} when Depth1 + 1 < byte_size(Key) ->
            State = State0#state{
                depth = Depth1 + 1,
                node = Child
            },
            {ok, _, Acc} = search_aux(Key, State, Acc0),
            {ok, State0, Acc};

        {ok, {Child, _}, Depth1} when Depth1 + 1 >= byte_size(Key) ->
            %% Everything below this node is a suffix of Key
            State = State0#state{
                depth = Depth1 + 1,
                node = Child
            },
            Acc = fold(Key, State, Acc0, #{first => 0}),
            {ok, State0, Acc};

        error when Mode == relaxed ->
            {ok, State0, Acc0};

        error when Mode == strict ->
            {error, State0, Acc0}
    end.


%% -----------------------------------------------------------------------------
%% @private
%% @doc
%% @end
%% -----------------------------------------------------------------------------
maybe_search_suffix(Key, LeafKey, Idx0, State0, Acc0, Opts) ->
    case binary:match(LeafKey, <<?KEY_SEP>>) of
        nomatch ->
            search_suffix(Key, Idx0, State0, Acc0, Opts);
        _ ->
            %% LeafKey contains separator, becuase this is a leaf we will not
            %% find anymore suffixes
            Acc0
    end.


%% -----------------------------------------------------------------------------
%% @private
%% @doc
%% @end
%% -----------------------------------------------------------------------------
search_suffix(Key, Idx0, #state{} = State0, Acc0, Opts) ->
    Suffix = <<Key/binary, ?KEY_SEP>>,
    search_suffix_aux(Suffix, Idx0, #state{} = State0, Acc0, Opts).


%% -----------------------------------------------------------------------------
%% @private
%% @doc
%% @end
%% -----------------------------------------------------------------------------
search_suffix_aux(Suffix, Idx0, #state{} = State0, Acc0, Opts0) ->
    case search_next(Suffix, State0) of
        {ok, {{_, _}, Idx0}, _} ->
            %% Idx0 is the index in which we found the last leaf, so adding
            %% this would result in a duplicate,
            fold(Suffix, State0, Acc0, Opts0#{first => Idx0 + 1});

        {ok, {{_, _} = Leaf, Idx}, _} ->
            match_and_fold(Suffix, Leaf, Idx, Opts0, State0, Acc0);

        {ok, {undefined, _Idx}, _} ->
            Acc0;

        {ok, {Child, Idx}, Depth1} when Depth1 + 1 < byte_size(Suffix) ->
            State = State0#state{
                depth = Depth1 + 1,
                node = Child
            },
            search_suffix(Suffix, Idx, State, Acc0, Opts0);

        {ok, {Child, Idx}, Depth1} when Depth1 + 1 >= byte_size(Suffix) ->
            State = State0#state{
                depth = Depth1 + 1,
                node = Child
            },
            Opts = Opts0#{first_node => Child, first => Idx},
            fold(Suffix, State, Acc0, Opts);

        error ->
            Acc0
    end.


%% -----------------------------------------------------------------------------
%% @private
%% @doc
%% @end
%% -----------------------------------------------------------------------------
search_next(<<>>, _) ->
    error;

search_next(_, #state{node = {_, _} = Leaf} = State) ->
    Depth = State#state.depth,
    %% This is wrong here
    {ok, {Leaf, 0}, Depth};

search_next(Key, #state{node = NodeId} = State) when is_integer(NodeId) ->
    Trie = State#state.trie,
    NodeId = State#state.node,
    Depth0 = State#state.depth,

    {PLen0, LCP} = art_node:longest_common_prefix(Trie, NodeId, Key, Depth0),
    PLen = min(?MAX_PREFIX_LEN, PLen0),
    Depth = Depth0 + PLen0,

    case LCP of
        LCP when LCP =:= PLen orelse (LCP > 0 andalso LCP < PLen) ->
            Index = art_utils:index(Key, Depth),

            case art_node:get(Trie, NodeId, Index) of
                undefined when byte_size(Key) - Depth == 1 ->
                    error;

                undefined ->
                    %% There is no exact match but there are more keys starting
                    %% with Key as prefix. e.g. a compound key with KEY_SEP
                    %% We return ok so that we can call search_suffix.
                    {ok, {undefined, Index}, Depth};

                Child ->
                    {ok, {Child, Index}, Depth}
            end;

        LCP when LCP > 0 andalso LCP < PLen andalso Depth == byte_size(Key) ->

            %% There is no exact match but there are more keys sharing
            %% Key as prefix. e.g. a compound key with KEY_SEP
            %% We stay on same node
            {ok, {undefined, 0}, Depth};

        _ ->
            error
    end.


%% -----------------------------------------------------------------------------
%% @private
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec wildcard_iter(Len :: non_neg_integer()) -> wildcard_iter().

wildcard_iter(0) ->
    #{
        n => 1,
        i => 0,
        skip => 0
    };

wildcard_iter(1) ->
    #{
        n => 1,
        i => 0,
        skip => 0
    };

wildcard_iter(Len) ->
    %% We calculate all posible combinations.
    %% Easy since for every element of the tuple we have either a part or the
    %% wildcard symbol $. (denoted by the bit 1 and 0 respectively)
    N = 1 bsl Len,

    #{
        n => N,
        i => 0,
        skip => 0
    }.


wildcard_iter_skip(#{skip := Skip0} = Iter, Skip) ->
    Iter#{skip => Skip0 bor Skip}.


%% -----------------------------------------------------------------------------
%% @private
%% @doc Returns the next wildcard combination in a lexicographical order
%% Example:
%% ```
%% > Iter0 = wildcard_iter([<<"a">>, <<"b">>, <<"c">>]).
%% > {<<"..">>, Iter1} = next_wildcard(Iter0).
%% > {<<"..c">>, Iter2} = next_wildcard(Iter1).
%% ...
%% > {<<"a.b.c">>, Iter9} = next_wildcard(Iter8).
%% > error = next_wildcard(Iter8).
%% '''
%% @end
%% -----------------------------------------------------------------------------
-spec next_wildcard(state()) ->
    {ok, binary(), state()} | {skip, state()} | {error, state()}.

next_wildcard(#state{wildcard_iter = #{i := I, n := N}} = State0)
when I >= N orelse N == 1 ->
    %% We have consumed all wildcards
    {error, State0};


next_wildcard(#state{wildcard_iter = #{i := I} = Iter0} = State0) ->
    First = State0#state.first,
    Parts = State0#state.parts,
    IsFirstPartGround = I =:= 1 bsl (tuple_size(Parts) - 1),

    case ?IS_EVEN(I) andalso IsFirstPartGround of
        true when State0#state.part_index =:= 1 ->
            %% We are searching for wildcards on the root of the trie i.e. the
            %% ones commencing with $. so we return and error in order to
            %% postpone searching for this wildcard until we have evaluated the
            %% first part.
            {error, State0};

        _ ->
            case do_next_wildcard(First, Parts, Iter0) of
                {ok, W, Iter1} ->
                    State = State0#state{wildcard_iter = Iter1},
                    {ok, W, State};
                {skip, Iter1} ->
                    State = State0#state{wildcard_iter = Iter1},
                    {skip, State}
            end
    end.


%% -----------------------------------------------------------------------------
%% @private
%% @doc
%% @end
%% -----------------------------------------------------------------------------
do_next_wildcard(First, Parts, #{i := I, skip := Skip} = Iter) ->
    case I band Skip == 0 of
        true ->
            W = iolist_to_binary(make_wildcard(First, Parts, I)),
            {ok, W, Iter#{i := I + 1}};
        false ->
            %% We skip this wilcard
            {skip, Iter#{i := I + 1}}
    end.



%% -----------------------------------------------------------------------------
%% @private
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec make_wildcard(binary(), tuple(), non_neg_integer()) -> [binary()].

make_wildcard(First, T, Bitmask) ->
    make_wildcard(First, T, Bitmask, tuple_size(T), []).


%% -----------------------------------------------------------------------------
%% @private
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec make_wildcard(
    binary(), tuple(), non_neg_integer(), non_neg_integer(), [binary()]) ->
    [binary()].

make_wildcard(First, _, _, 0, [H|T]) ->
    %% We finished iterating over the tuple
    [<<First/binary, H/binary>> | T];

make_wildcard(First, T, Bitmask, N, Acc0) ->
    Pos = (1 bsl (tuple_size(T) - N)),
    Acc =
        case Bitmask band Pos of
            0 when N == 1 ->
                Acc0;
            0 when N > 1 ->
                [<<$.>> | Acc0];
            Pos when N == 1 ->
                [element(N, T) | Acc0];
            Pos ->
                [<<$.>>, element(N, T) | Acc0]
        end,
    make_wildcard(First, T, Bitmask, N - 1, Acc).


%% -----------------------------------------------------------------------------
%% @private
%% @doc
%% @end
%% -----------------------------------------------------------------------------
match_and_fold(Term, Leaf0, Idx, Opts, State, Acc) ->
    try matches(Term, Leaf0, Opts, State#state.ms, State#state.suffix) of
        {ok, Unpacked} ->
            %% We need to continue matching suffixes
            fold(Term, State, [Unpacked | Acc], Opts#{first => Idx + 1});

        error ->
            Acc
    catch
        throw:break ->
            Acc
    end.


%% -----------------------------------------------------------------------------
%% @private
%% @doc
%% @end
%% -----------------------------------------------------------------------------
matches(Bin0, {K, _} = Leaf, _Opts, MS, TermSuffix) ->
    %% The case for search_suffixes, Bin will end with ?KEY_SEP
    Bin = binary:replace(Bin0, <<?KEY_SEP>>, <<>>),
    Size = byte_size(Bin),

    Result =
        case K of
            Bin ->
                {true, <<>>};

            <<Bin:Size/binary, ?KEY_SEP, KeyRest/binary>> ->
                {true, KeyRest};

            _ ->
                false
        end,

    case Result of
        {true, <<>>} ->
            maybe_run_ms(Leaf, MS);
        {true, Rest} ->
            %% The searched term
            KeySuffix = binary:split(Rest, <<?KEY_SEP>>, [global]),

            case art_utils:subsumes(TermSuffix, KeySuffix, relaxed) of
                true ->
                    maybe_run_ms(Leaf, MS);
                false ->
                    error
            end;
        false ->
            throw(break)
    end.


%% -----------------------------------------------------------------------------
%% @private
%% @doc
%% @end
%% -----------------------------------------------------------------------------
maybe_run_ms({K, V}, #state{ms = MS}) ->
    maybe_run_ms({K, V}, MS);

maybe_run_ms({K, V}, undefined) ->
    Unpacked = {art_utils:maybe_unpack(K), V},
    {ok, Unpacked};

maybe_run_ms({K, V}, MS) ->
    Unpacked = {art_utils:maybe_unpack(K), V},

    case ets:match_spec_run([Unpacked], MS) of
        [Unpacked] ->
            {ok, Unpacked};
        [] ->
            error
    end.


%% -----------------------------------------------------------------------------
%% @private
%% @doc
%% @end
%% -----------------------------------------------------------------------------
matches_fold_fun(Key, MS, Suffix, Opts) ->
    fun(K, V, Acc) ->
        try matches(Key, {K, V}, Opts, MS, Suffix) of
            {ok, Unpacked} ->
                [Unpacked | Acc];

            error ->
                Acc
        catch
            throw:break ->
               throw({break, Acc})
        end
    end.


%% -----------------------------------------------------------------------------
%% @private
%% @doc
%% @end
%% -----------------------------------------------------------------------------
fold(Key, State, Acc, Opts0) ->
    MS = State#state.ms,
    Parent = State#state.node,
    Match = matches_fold_fun(Key, MS, State#state.suffix, Opts0),
    Opts = Opts0#{first_node => Parent},
    art:fold(State#state.trie, Match, Acc, Opts).



%% =============================================================================
%% EUNIT
%% =============================================================================



-ifdef(TEST).
-include_lib("eunit/include/eunit.hrl").

gen_wildcards(Bin) ->
    Parts = list_to_tuple(binary:split(Bin, <<$.>>, [global])),
    Iter = wildcard_iter(tuple_size(Parts)),
    gen_wildcards(Parts, Iter, []).

gen_wildcards(Parts, Iter0, Acc) ->
    case do_next_wildcard(<<>>, Parts, Iter0) of
        {ok, W, Iter} ->
            gen_wildcards(Parts, Iter, [W|Acc]);
        _ ->
            lists:reverse(Acc)
    end.


wildcards_test() ->
    L = gen_wildcards(<<"com.myapp.event.one">>),
    Expected = [
        <<"...">>,
        <<"...one">>,
        <<"..event.">>,
        <<"..event.one">>,
        <<".myapp..">>,
        <<".myapp..one">>,
        <<".myapp.event.">>,
        <<".myapp.event.one">>,
        <<"com...">>,
        <<"com...one">>,
        <<"com..event.">>,
        <<"com..event.one">>,
        <<"com.myapp..">>,
        <<"com.myapp..one">>,
        <<"com.myapp.event.">>,
        <<"com.myapp.event.one">>
    ],
    ?assertEqual(Expected, L).

-endif.
