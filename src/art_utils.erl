-module(art_utils).
-include("art.hrl").


-export([index/2]).
-export([is_pattern/1]).
-export([is_prefix_pattern/1]).
-export([is_wildcard_pattern/1]).
-export([key_suffix_pattern/1]).
-export([matches_prefix/4]).
-export([maybe_unpack/1]).
-export([pad_to/2]).
-export([prefix/3]).
-export([subsumes/3]).
-export([unpack_pattern/1]).



%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
prefix(Bin, Pos, Len) when byte_size(Bin) < (Pos + Len) ->
    <<0:8>>;

prefix(Bin, Pos, Len) ->
    <<_:Pos/binary, Prefix:Len/binary, _/binary>> = Bin,
    Prefix.



%% -----------------------------------------------------------------------------
%% @doc Returns true is `Pattern' is a prefix of `Term'
%% Mode allows to control the semantics of the check. If `strict' the check
%% returns `true' only when the second term is a prefix of the first. If
%% `relaxed' it also returns `true' in case the two terms fully match.
%% @end
%% -----------------------------------------------------------------------------
-spec matches_prefix(
    Term :: binary(),
    Pattern :: art:key(),
    Mode :: strict | relaxed,
    Rest :: binary()) -> boolean().

%% matches_prefix(Bin, Bin, true, _) ->
%%     false;

%% matches_prefix(Bin, Bin, false, _) ->
%%     true;

matches_prefix(Term, Pattern, Mode, PatternRest0) ->
    PSize = byte_size(Pattern),

    case Term of
        <<Pattern:PSize/binary, ?KEY_SEP, _/binary>> when Mode == strict ->
            %% Prefix exactly matches Key, so not strictly a prefix
            false;
        <<Pattern:PSize/binary, Rest/binary>> ->
            %% We either have more chars till the separator or we do not have
            %% components at all
            case binary:split(Rest, <<?KEY_SEP>>) of
                [Rest] ->
                    %% No components, so it is a prefix
                    true;
                [_, TermRest] ->
                    %% We have to match the key components
                    PatternRest1 = key_suffix_pattern(PatternRest0),
                    LCP = binary:longest_common_prefix(
                        [TermRest, PatternRest1]
                    ),
                    LCP == byte_size(PatternRest1)
            end;
        _ ->
            false
    end.




%% TODO We need to consider doing at the matches level instead of down here

%% @private
%% key_suffix_pattern(<<?KEY_SEP, Rest/binary>>) ->
key_suffix_pattern(<<?KEY_SEP, Rest/binary>>) ->
    %% This means the pattern had an equivalente of a wilcard
    %% e.g. {<<"foo">>, <<>>, <<"bar"}, so we need to treat this as a prefix
    %% i.e. equivalente to matching {<<"foo">>}
    %% Rest;
    key_suffix_pattern(Rest);

key_suffix_pattern(Bin) ->
    case binary:matches(Bin, <<?KEY_SEP, ?KEY_SEP>>) of
        [] ->
            Bin;
        [{Pos, 2}|_] ->
            binary:part(Bin, {0, Pos})
    end.


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
index(Bin, Pos) when byte_size(Bin) < (Pos + 1) ->
    0;

index(Bin, Pos) ->
    <<_:Pos/binary, Index:8, _/binary>> = Bin,
    Index.


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
pad_to(Width, Binary) ->
    case (Width - size(Binary) rem Width) rem Width of
        0 -> Binary;
        N -> <<Binary/binary, 0:(N*8)>>
    end.



%% -----------------------------------------------------------------------------
%% @private
%% @doc
%% The character `*' is a prefix marker. The characters before the prefix
%% marker are considered a prefix pattern.
%% The characters following the prefix marker are considered
%% a key suffix. This allows to have multiple objects store for the same key
%%
%% @end
%% -----------------------------------------------------------------------------
is_pattern(Bin) ->
    case is_prefix_pattern(Bin) of
        false ->
            is_wildcard_pattern(Bin);
        {true, _} = Prefix ->
            Prefix
    end.


%% @private
is_prefix_pattern(Bin) ->
    case binary:split(Bin, <<$*>>, []) of
        [Bin] ->
            %% Bin does not contain "*"
            false;
        [Prefix, <<>> = Rest] ->
            %% Bin ends with *
            {true, {prefix, Prefix, Rest}};
        [Prefix, <<?KEY_SEP, _/binary>> = Rest] ->
            %% Bin ends with * followed by separator
            {true, {prefix, Prefix, Rest}};
        _ ->
            %% * is not in the last position
            false
    end.



%% @private
is_wildcard_pattern(Bin) ->
    {Prefix, Rest} = unpack_pattern(Bin),
    case binary:split(Prefix, [<<$.>>], [global]) of
        [Prefix] ->
            {false, {Prefix, Rest}};
        L ->
            %% At least one needs to be a wildcard, otherwise is a full match
            case lists:member(<<>>, L) of
                true ->
                    {true, {wildcard, L, Prefix, Rest}};
                false ->
                    {false, {Prefix, Rest}}
            end
    end.



%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
unpack_pattern(Bin) when is_binary(Bin) ->
    unpack_pattern(binary:split(Bin, <<?KEY_SEP>>, []));

unpack_pattern([Key]) ->
    {Key, <<>>};

unpack_pattern([Key, Rest]) ->
    {Key, <<?KEY_SEP, Rest/binary>>}.


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
maybe_unpack(Bin) when is_binary(Bin) ->
    maybe_unpack(binary:split(Bin, <<?KEY_SEP>>, [global]));

maybe_unpack([Key]) ->
    Key;

maybe_unpack(Fields) when is_list(Fields) ->
    list_to_tuple(Fields);

maybe_unpack(Fields) when is_tuple(Fields) ->
    Fields.


%% -----------------------------------------------------------------------------
%% @doc Returns true if each element of the first list subsumes the
%% corresponding element on the second list.
%% If the 3rd argument is 'strict' then both lists have to have the same lenght,
%% if it is 'relaxed' then the first list can have a lenght that is less or
%% equal than the second list i.e. it acts as a prefix matching.
%% A term subsumes another term when is equal or when the first term is the
%% empty binary (wildcard).
%% @end
%% -----------------------------------------------------------------------------
subsumes(Term, Term, _) ->
    true;

subsumes(H1, H2, strict) when length(H1) =/= length(H2) ->
    false;

subsumes(H1, H2, relaxed) when length(H1) > length(H2) ->
    false;

subsumes([H|T1], [H|T2], Mode) ->
    subsumes(T1, T2, Mode);

subsumes(['_'|T1], [_|T2], Mode) ->
    subsumes(T1, T2, Mode);

subsumes([<<>>|T1], [_|T2], Mode) ->
    subsumes(T1, T2, Mode);

subsumes([], _, relaxed) ->
    true;

subsumes([], [], strict) ->
    true;

subsumes(_, _, _) ->
    false.




%% =============================================================================
%% PRIVATE
%% =============================================================================


