%% -----------------------------------------------------------------------------
%% @doc An implementation of a a single writer, multi-reader trie (radix tree)
%% using Erlang ets.
%%
%% The trie has to simultaneous search algorithms:
%% * `match' - as any traditional radix tree, finds all stored elements whose
%% keys are subsumed by the provided argument.
%% * `find_matches' - finds all stored elements whose keys subsume the provided
%% argument. That is, this algorithm treats the stored keys as patterns and
%% supports prefix and wildcard matching.
%%
%% Specifically this module implements the
%% [Adaptive Radix Tree algorithm](https://doi.org/10.1109/ICDE.2013.6544812).
%%
%% Two techniques ensure that each inner node has at least two children:
%%
%% 1. Lazy Expansion - Inner nodes are only created if they are required to
%% distinguish at least two leaf nodes. This optimisation requires that the key
%% is stored at the leaf which can be accessed using `NodeCallback:prefix/1'.
%% 2. Path Compression - removes all inner nodes that have only a single child.
%% At each inner node, a variable length prefix key (binary) is stored. It
%% contains the keys of all preceding one-way nodes which have been removed.
%%
%% ## Limitations
%%
%% At the moment the algorithm is not adaptive since we always use a
%% 256 size node.
%%
%% ## References
%%
%% * [The adaptive radix tree: ARTful indexing for main-memory databases](https://doi.org/10.1109/ICDE.2013.6544812)
%%
%% {trie, ?ROOT_ID, 256, Sz, 54}
%% {trie_node, 54, 256, Sz, PLen, P, ...,102,...,{Ki,Vi},...}
%% {trie_node, 102, 256, Sz, PLen, P, ...}
%% @end
%% -----------------------------------------------------------------------------
-module(art).

-include_lib("kernel/include/logger.hrl").
-include("art.hrl").


-type t()                   ::  ets:tab().
-type name()                ::  atom().
-type key()                 ::  art_node:key() | tuple().
-type value()               ::  art_node:value().
-type size()                ::  non_neg_integer().
-type index()               ::  art_node:index().
-type depth()               ::  art_node:depth().
-type find_matches_opts()   ::  #{
                                    match_spec => ets:match_spec(),
                                    first => binary()
                                }.


-type option()          ::  named_table
                            | public | protected | private
                            | {write_concurrency, boolean()}
                            | {read_concurrency, boolean()}
                            | compressed
                            | {heir, Pid :: pid(), HeirData :: term()}
                            | {heir, none}.

-type match_opts()      ::  #{
                                mode => exact | key | prefix | wildcard
                            }.

-export_type([t/0]).
-export_type([key/0]).
-export_type([value/0]).

-export([add/2]).
-export([all/0]).
-export([delete/1]).
-export([delete/2]).
-export([find/2]).
-export([lookup/2]).
-export([find_matches/2]).
-export([find_matches/3]).
-export([first/1]).
-export([first/2]).
-export([fold/3]).
-export([fold/4]).
-export([from_list/1]).
-export([from_orddict/1]).
-export([from_set/1]).
-export([get/2]).
-export([i/0]).
-export([i/1]).
-export([info/1]).
-export([last/1]).
-export([last/2]).
-export([match/2]).
-export([match/3]).
-export([new/0]).
-export([new/2]).
-export([prefix_mismatch/4]).
-export([prefix_mismatch/5]).
-export([root/1]).
-export([set/3]).
-export([size/1]).
-export([store/3]).
-export([take/2]).
-export([to_dot/2]).
-export([to_list/1]).

-export([search_child/4]).

-ifdef(TEST).
-export([get_trie/1]).
-endif.


%% =============================================================================
%% API
%% =============================================================================



%% -----------------------------------------------------------------------------
%% @doc Creates a new anonymous trie.
%% This is equivalent to calling `new(undefined, [])'
%% @end
%% -----------------------------------------------------------------------------
-spec new() -> t().

new() ->
    new(undefined, []).


%% -----------------------------------------------------------------------------
%% @doc Creates a new trie with nam `Name' and options `Opts'.
%% In the current version options are ignored.
%%
%% The trie is implemented using ets and will create a protected
%% named ets table of type `set'.
%%
%% Returns the trie i.e. the ets tid.
%% @end
%% -----------------------------------------------------------------------------
-spec new(Name :: name(), Options :: [option()]) -> t().

new(Name, _Opts) when is_atom(Name) ->
    %% TODO Support merging user options

    %% We set the table as protected because we support only one writer
    Opts0 = [protected, {read_concurrency, true}],
    {LeafTabName, Opts} = case Name of
        undefined ->
            {undefined, Opts0};
        _ ->
            LeafTabName0 = list_to_atom(atom_to_list(Name) ++ "_leaf"),
            {LeafTabName0, [named_table | Opts0]}
    end,

    {ok, Tab} = art_table_owner:add_or_claim(
        Name, [set, {keypos, 2} | Opts]
    ),
    {ok, LeafTab} = art_table_owner:add_or_claim(
        LeafTabName, [bag, {keypos, 2} | Opts]
    ),

    Trie = #trie{
        id = {Tab, 0},
        tab = Tab,
        leaf_tab = LeafTab,
        size = 0,
        root = undefined
    },
    true = ets:insert(Tab, Trie),

    Tab.


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec from_set(Set :: sets:set()) -> t().

from_set(Set) ->
    from_orddict(sets:to_list(Set)).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec from_list(List :: list()) -> t().

from_list(List) ->
    from_orddict(orddict:from_list(List)).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec from_orddict(Orddict :: orddict:orddict()) -> t().

from_orddict(Orddict) ->
    T = art:new(),
    add(Orddict, T),
    T.


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
all() ->
    art_table_owner:list().


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
i() ->
    hform('name', 'size', 'nodes', 'mem', 'owner'),
    io:format(" -------------------------------------"
	      "---------------------------------\n"),
    lists:foreach(fun print_info/1, all()),
    ok.


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------

i(T) ->
    ets:i(T).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec info(t()) -> list() | no_return().

info(T) ->
    #trie{tab = M, leaf_tab = L} = get_trie(T),
    [
        {nodes, ets:info(M, size) + ets:info(L, size)},
        {memory, ets:info(M, memory) + ets:info(L, memory)},
        {owner, ets:info(M, owner)},
        {heir, ets:info(M, heir)}
    ].



%% -----------------------------------------------------------------------------
%% @doc Returns the number of elements in the trie.
%% @end
%% -----------------------------------------------------------------------------
-spec size(t() | trie()) -> non_neg_integer() | no_return().

size(#trie{size = Val}) ->
    Val;

size(T) ->
    Id = {T, get_latest_version(T)},

    try
        ets:lookup_element(T, Id, #trie.size)
    catch
        error:badarg ->
            error({badtrie, T})
    end.


%% -----------------------------------------------------------------------------
%% @doc Sets `Value' for `Key' in Trie.
%% Calls {@link store/3} ignoring the result and returning the atom `ok'.
%% @end
%% -----------------------------------------------------------------------------
-spec set(Key :: key(), Value :: value(), T :: t() | trie()) ->
    ok | no_return().

set(Tuple, Value, T) when is_tuple(Tuple) ->
    set(?PACK_KEY(Tuple), Value, T);

set(Key, Value, T) ->
    _ = store(Key, Value, T),
    ok.


%% -----------------------------------------------------------------------------
%% @doc Stores the pair `Key' - `Value' in the trie.
%%
%% If the key already exists, it replaces the `OldValue' with `Value'.
%% Returns the `OldValue' if there was one, otherwise returns
%% the atom `undefined'.
%%
%% The call fails with a `{badtrie, Trie}' exception if `Trie' is not a trie,
%% and a `badarg' exception if `Value' is not valid.
%% @end
%% -----------------------------------------------------------------------------
-spec store(Key :: key(), Value :: value(), Trie :: t() | trie()) ->
    {value, OldValue :: value()} | undefined | no_return().

store(Tuple, Value, T) when is_tuple(Tuple) ->
    store(?PACK_KEY(Tuple), Value, T);

store(Key, Value, T) when is_binary(Key) ->
    Value =/= undefined orelse error(badarg),

    Trie = get_trie(T),
    Tab = Trie#trie.tab,
    Depth = 0,

    case Trie#trie.root of
        undefined ->
            %% We have an empty trie. We update and return the old value.
            true = update_root({Key, Value}, Trie, 1),
            undefined;

        {Key, Value} ->
            %% We have a trie with a single leaf and is equal to the arguments,
            %% so we do nothing and return the existing value.
            {value, Value};

        {Key, Prev} ->
            %% We have a trie with a single leaf, but we have a new value for
            %% its key. We update and return the old value.
            true = update_root({Key, Value}, Trie, 0),
            {value, Prev};

        {_, _} = Leaf ->
            %% We have a trie with a single leaf, but this is a new key.
            %% We need to merge the leaves into a new node.
            Depth = 0,
            NodeId = merge_leaves(Leaf, {Key, Value}, Trie, Depth),
            true = update_root(NodeId, Trie, 1),
            undefined;

        NodeId when is_integer(NodeId) ->
            case do_store(Key, Value, Trie, NodeId, Depth) of
                {ok, Prev, NodeId} ->
                    %% The root node has not changed. We return the previous value
                    Prev;
                {ok, undefined = Prev, Child} ->
                    %% TODO in the future we will create a new root for a new version

                    %% We have a new root for a previously empty trie
                    Size = Trie#trie.size,
                    NewTrie = Trie#trie{size = Size + 1, root = Child},
                    true = ets:insert(Tab, NewTrie),

                    %% We return the previous value
                    Prev;
                {ok, {value, _} = Prev, Child} ->
                    %% TODO in the future we will create a new root for a new version

                    %% We have a new root that replaced the previous one
                    true = update_root(Child, Trie, 1),
                    %% We return the previous value
                    Prev
            end
        end.


%% -----------------------------------------------------------------------------
%% @doc Stores all the key-value pairs in the list.
%% @end
%% -----------------------------------------------------------------------------
-spec add([{key(), value()}], t() | trie()) -> ok | no_return().

add([{Key, Value} | Rest], T) ->
    _ = set(Key, Value, T),
    add(Rest, T);

add([], _) ->
    ok.


%% -----------------------------------------------------------------------------
%% @doc Returns the value for key `Key' in trie `Trie' or the atom
%% `error' if the there was no matching element.
%% @end
%% -----------------------------------------------------------------------------
-spec find(Key :: key(), T :: t() | trie()) ->
    {ok, value()} | error | no_return().

find(Tuple, T) when is_tuple(Tuple) ->
    find(?PACK_KEY(Tuple), T);

find(Key, T) ->
    case get_trie(T) of
        #trie{size = 0} ->
            error;
        #trie{root = Node} = Trie ->
            search(Key, Trie, Node, 0)
    end.


%% -----------------------------------------------------------------------------
%% @doc Returns a list of key value pairs were `Key' is equal to key or when
%% key is a composite key and `Key' is a prefix of key i.e. the first element
%% of the composite key has to match exactly.
%%
%% This the same as match(Key, Trie, #{mode => exact}).
%%
%% @end
%% -----------------------------------------------------------------------------
-spec lookup(Key :: key(), T :: t() | trie()) ->
    [art_node:leaf()] | no_return().

lookup(Key, T) ->
    Trie = get_trie(T),
    art_match:do(Key, Trie, #{mode => exact}).


%% -----------------------------------------------------------------------------
%% @doc Returns value `Value' associated with key `Key'
%% if `Trie' contains `Key'.
%% The call fails with a `{badtrie, Trie}' exception if Trie is not a trie,
%% or with a `{badkey, Key}' exception if no value is associated with `Key'.
%% @end
%% -----------------------------------------------------------------------------
-spec get(Key :: key(), T :: t()) -> value() | no_return().

get(Key, T) ->
    case find(Key, T) of
        {ok, Value} -> Value;
        error -> error({badkey, Key})
    end.



%% -----------------------------------------------------------------------------
%% @doc Deletes the trie. This call is important as tries are implemented with
%% ets tables.
%% There is no garbage collection of ets tables. However, the trie is
%% deleted if the process that created it terminates.
%% @end
%% -----------------------------------------------------------------------------
-spec delete(T :: t() | trie()) -> boolean() | no_return().

delete(T) ->
    #trie{tab = Tab, leaf_tab = LTab} = get_trie(T),
    _ = art_table_owner:delete(Tab),
    art_table_owner:delete(LTab).


%% -----------------------------------------------------------------------------
%% @doc Removes the key `Key' from trie `Trie'.
%% Calls {@link take/2} ignoring the result and returning the atom `ok'.
%% @end
%% -----------------------------------------------------------------------------
-spec delete(Key :: key(), Trie :: t() | trie()) -> ok | no_return().

delete(Tuple, T) when is_tuple(Tuple) ->
    delete(?PACK_KEY(Tuple), T);

delete(Key, T) ->
    _ = take(Key, T),
    ok.


%% -----------------------------------------------------------------------------
%% @doc Removes a key value pair from the trie.
%% If the key already exists, it replaces the `OldValue' with `Value'.
%% Returns the `OldValue' if there was one, otherwise returns
%% the atom `undefined'.
%% @end
%% -----------------------------------------------------------------------------
-spec take(Key :: key(), T :: t() | trie()) ->
    {value, value()} | error | no_return().

take(Tuple, T) when is_tuple(Tuple) ->
    take(?PACK_KEY(Tuple), T);

take(Key, T) ->
    #trie{id = Id, root = Node} = Trie = get_trie(T),
    delete_search(Key, Trie, Node, 0, [Id]).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec root(t() | trie()) -> art_node:e() | no_return().

root(T) ->
    #trie{root = Val} = get_trie(T),
    Val.


%% -----------------------------------------------------------------------------
%% @doc Returns the first key-value pair from the trie.
%% @end
%% -----------------------------------------------------------------------------
-spec first(t() | trie()) ->
    {art_node:leaf(), {art_node:id(), index()}}
    | error | no_return().

first(T) ->
    #trie{root = Root} = Trie = get_trie(T),
    first(Root, Trie).


%% -----------------------------------------------------------------------------
%% @doc Returns the first `{key(), value()}' pair found on the trie or `error'
%% if none is found.
%% @end
%% -----------------------------------------------------------------------------
-spec first(art_node:id() | art_node:leaf(), t() | trie()) ->
    {art_node:leaf(), {art_node:id(), index()}}
    | error | no_return().

first(undefined, _) ->
    error;

first({_, _} = Leaf, _) ->
    Leaf;

first(NodeId, T) when is_integer(NodeId) ->
    Trie = get_trie(T),
    case art_node:first(Trie, NodeId) of
        {Index, {_, _} = Leaf} ->
            {Leaf, {NodeId, Index}};
        {_Index, Child} ->
            first(Child, Trie)
    end.


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec last(t() | trie()) ->
    {Index :: index(), art_node:t()} | error | no_return().

last(T) ->
    #trie{root = Root} = Trie = get_trie(T),
    last(Root, Trie).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec last(art_node:e(), t() | trie()) ->
    {Index :: index(), art_node:t()} | error | no_return().

last(undefined, _) ->
    error;

last({_, _} = Leaf, _) ->
    Leaf;

last(NodeId, T) when is_integer(NodeId) ->
    Trie = get_trie(T),
    case art_node:last(Trie, NodeId) of
        {_, Child} ->
            last(Child, Trie);
        undefined ->
            error
    end.


%% -----------------------------------------------------------------------------
%% @doc Searches for all entries where their key matches pattern `Pattern'.
%% A pattern can be a binary or a tuple where all elements are binaries.
%%
%% The pattern can be a key, a prefix or wildcard pattern. If it is a key it is
%% regarded as a prefix (an not only an exact match). If you want an exact
%% match only use {@link find/2}.
%%
%% This function matches all compound keys whose first element match `Pattern',
%% that is, it uses `Pattern' as a prefix of the compound key. If you need an
%% exact match of the compound key elements use {@link match/3}
%% with `Opts = #{mode => exact}'.
%%
%% Returns a list of tuples with form `{Key, Value}'.
%%
%% @end
%% -----------------------------------------------------------------------------
-spec match(Pattern :: binary() | {binary()}, t() | trie()) ->
    [{key(), value()}] | no_return().

match(Pattern, T)  ->
    match(Pattern, T, #{mode => pattern}).


%% -----------------------------------------------------------------------------
%% @doc Searches for all entries where their key matches pattern `Pattern'.
%% A pattern can be a binary or a tuple where all elements are binaries.
%%
%% The pattern can be a key, a prefix or wildcard pattern. If it is a key it is
%% regarded as a prefix (an not only an exact match). If you want an exact
%% match only use {@link find/2}.
%%
%% Using `Opts' you can control wether `Pattern' acts as a prefix for compound
%% keys or not. If you need an exact match use `Opts = #{mode => exact}'
%% otherwise, if you want to match any compound key whose first element matches
%% `Pattern' then use `Opts = #{mode => pattern}'.
%%
%% Returns a list of tuples with form `{Key, Value}'.
%%
%% @end
%% -----------------------------------------------------------------------------
-spec match(Pattern :: binary() | {binary()}, t() | trie(), match_opts()) ->
    [{key(), value()}] | no_return().

match(Pattern, T, Opts)  ->
    Trie = get_trie(T),
    art_match:do(Pattern, Trie, Opts).



%% -----------------------------------------------------------------------------
%% @doc Same as `find_matches(Term, undefined, Trie)'.
%% @end
%% -----------------------------------------------------------------------------
-spec find_matches(Term :: key(), T :: t() | trie()) ->
    [{key(), value()}] | no_return().

find_matches(Term, T) ->
    find_matches(Term, #{}, T).


%% -----------------------------------------------------------------------------
%% @doc Returns a list of all `{Key, Value}` pairs, where `Key' subsumes `Term'
%% i.e. this function treats the stored keys as patterns that are used to match
%% `Term'.
%%
%% Key matching supports _prefix matching_ and _wildcard matching_.
%%
%% _Prefix matching_ will match all those stored keys terminating with
%% the character `*`.
%%
%% _Wilcard matching_ will match all empty segments, where a segment is
%% delimited by the character `.' e.g. the term `foo..bar' matches (subsumes)
%% the keys `foo.my.bar' and `foo.your.bar' because the empty segment acts as
%% a wilcard for `my' and `your' respectively.
%%
%% > `Term' cannot be a pattern, if it is, the function fails with a `badarg'
%% reason. If you want to find all stored keys matching a pattern use the
%% function {@link match/2} instead.
%%
%% If a `MS' is a valid `ets:match_spec()', it will be used to filter the
%% results generated by the key matching procedure explained above.
%% @end
%% -----------------------------------------------------------------------------
-spec find_matches(Key :: key(), find_matches_opts(), T :: t() | trie()) ->
    [{key(), value()}] | no_return().

find_matches(Term, Opts, T) when is_binary(Term) ->
    Trie = get_trie(T),
    L = binary:split(Term, <<?KEY_SEP>>, [global]),

    art_find_matches:do(L, Opts, Trie);

find_matches(Term, Opts, T) when is_tuple(Term) ->
    Trie = get_trie(T),
    L = [
        begin
            case E of
                '_' ->
                    <<>>;
                E when is_binary(E) ->
                    E;
                _ ->
                    error(badarg)
            end
        end || E <- tuple_to_list(Term)
    ],
    art_find_matches:do(L, Opts, Trie).




%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
fold(T, Fun, Acc0) when is_function(Fun, 3) ->
    fold(T, Fun, Acc0, #{}).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
fold(T, Fun, Acc0, Opts) when is_function(Fun, 3) ->
    Trie = get_trie(T),

    Node = case maps:find(first_node, Opts) of
        {ok, Val} ->
            Val;
        error ->
            root(Trie)
    end,
    try
        {_, _, Acc} = do_fold(Node, Opts, {Trie, Fun, Acc0}),
        Acc
    catch
        throw:{break, {_, _, Acc1}} ->
            Acc1
    end.


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec to_list(t() | trie()) -> list().

to_list(T) ->
    FoldFun = fun(Key, Val, Acc) -> [{maybe_unpack(Key), Val} | Acc] end,
    fold(T, FoldFun, []).


%% -----------------------------------------------------------------------------
%% @doc Returns the position at which the prefixes mismatch
%% @end
%% -----------------------------------------------------------------------------
-spec prefix_mismatch(t() | trie(), art_node:id(), key(), depth()) -> index().

prefix_mismatch(T, Id, Key, Depth) ->
    Trie = get_trie(T),
    {PrefixLen, Index} = art_node:longest_common_prefix(Trie, Id, Key, Depth),

    case PrefixLen > ?MAX_PREFIX_LEN of
        false ->
            Index;
        true ->
            KeyLen = byte_size(Key),
            Max0 = min(min(?MAX_PREFIX_LEN, PrefixLen), (KeyLen - Depth)),

            %% We need to find a leaf to compare the extra
            %% N bytes when N = PrefixLen - ?MAX_PREFIX_LEN
            case Index =:= Max0 of
                true ->
                    {{LeafKey, _}, _} = first(Id, Trie),
                    LeafKeyLen = byte_size(LeafKey),
                    Max = min(LeafKeyLen, KeyLen) - Index - Depth,
                    Prefix1 = art_utils:prefix(LeafKey, Index + Depth, Max),
                    Prefix2 = art_utils:prefix(Key, Index + Depth, Max),
                    Index + binary:longest_common_prefix([Prefix1, Prefix2]);
                _ ->
                    Index
            end
    end.


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
prefix_mismatch(T, NodeId, Prefix, Depth0, PrefixLen0) ->
    case prefix_mismatch(T, NodeId, Prefix, Depth0) of
        Val when Val > PrefixLen0 ->
            %% To cater for MAX_PREFIX_LEN
            PrefixLen0;
        Val ->
            Val
    end.



%% =============================================================================
%% PRIVATE
%% =============================================================================



%% @private
-spec get_trie(t() | trie()) -> trie() | no_return().

get_trie(T) when is_atom(T) orelse is_reference(T) ->
    get_trie(T, get_latest_version(T));

get_trie(#trie{} = Trie) ->
    Trie.


%% @private
-spec get_trie(t() | trie(), version()) -> trie() | no_return().

get_trie(T, Version) when is_atom(T) orelse is_reference(T) ->
    Id = {T, Version},

    case ets:lookup(T, Id) of
        [#trie{} = Trie] ->
            Trie;
        _ ->
            error({badtrie, T})
    end;

get_trie(#trie{id = {_, Version}} = Trie, Version) ->
    Trie;

get_trie(#trie{id = {T, _}}, Version) ->
    get_trie(T, Version).


%% @private
update_root(Node, #trie{} = Trie, Incr) ->
    %% TODO use versioning
    Tab = Trie#trie.tab,
    Size = Trie#trie.size + Incr,
    NewRoot = Trie#trie{root = Node, size = Size},
    true = ets:insert(Tab, NewRoot).


%% @private
get_latest_version(_T) ->
    %% TODO
    0.


%% -----------------------------------------------------------------------------
%% @doc Traverse the tree until we find the position for the new Leaf.
%% Usually, the leaf can simply be inserted into an existing inner node, after
%% growing it if necessary.
%% However, two special situations might occur:
%% 1. The position is occupied by another leaf, in this case a new small node
%% is created with the existing and the new leaf
%% 2. The key of the new leaf differs from a compressed path: A new inner node
%% is created above the current node and the compressed paths are adjusted
%% accordingly.
%% @end
%% -----------------------------------------------------------------------------
-spec do_store(
    art_node:key(), art_node:value(), trie(), art_node:e(), depth()) ->
    {ok, {value, value()} | undefined, art_node:id() | art_node:leaf()}.


do_store(K1, V1, #trie{} = Trie, {K0, V0}, Depth) ->
    case K0 =:= K1 of
        true ->
            %% We have a new value for an existing key.
            %% We update and return the old value.
            {ok, {value, V0}, {K1, V1}};
        false ->
            %% We need to merge the leaves into a node
            NodeId = merge_leaves({K0, V0}, {K1, V1}, Trie, Depth),
            {ok, undefined, NodeId}
    end;

do_store(K, V, #trie{} = Trie, NodeId0, Depth) when is_integer(NodeId0) ->
    case art_node:prefix_len(Trie, NodeId0) of
        0 ->
            push_down(K, V, Trie, NodeId0, Depth);

        PrefixLen0 ->
            case prefix_mismatch(Trie, NodeId0, K, Depth) of
                Diff when Diff < PrefixLen0 ->
                    %% If the prefixes differ we need to split, creating a new
                    %% node on top of NodeId0
                    NodeId1 = split_node(
                        PrefixLen0, Diff, Depth, Trie, NodeId0
                    ),
                    %% ISSUE HERE
                    %% Finally, we insert a new leaf
                    Index = art_utils:index(K, Depth + Diff),
                    ok = add_child(Index, {K, V}, Trie, NodeId1),
                    {ok, undefined, NodeId1};

                Diff when Diff >= PrefixLen0 ->
                    push_down(K, V, Trie, NodeId0, Depth + PrefixLen0)
            end
    end.


%% -----------------------------------------------------------------------------
%% @private
%% @doc Function used by do_store/4
%% @end
%% -----------------------------------------------------------------------------
-spec push_down(key(), value(), t(), art_node:id(), depth()) ->
    {ok, {value, value()} | undefined, art_node:id()}.

push_down(Key, Value, #trie{} = Trie, NodeId0, Depth)
when is_integer(NodeId0) ->

    Index = art_utils:index(Key, Depth),

    case art_node:get(Trie, NodeId0, Index) of
        undefined ->
            %% We add a leaf to the current node
            ok = add_child(Index, {Key, Value}, Trie, NodeId0),
            {ok, undefined, NodeId0};

        Child0 ->
            %% Child is another node or leaf
            %% We push the insertion down the tree
            {ok, Reply, Child1} = do_store(Key, Value, Trie, Child0, Depth + 1),
            _ = art_node:set(Trie, NodeId0, Index, Child1),
            {ok, Reply, NodeId0}
    end.


%% -----------------------------------------------------------------------------
%% @private
%% @doc Function used by do_store/4
%% A new inner node is created above the current node and the compressed
%% paths are adjusted accordingly
%% @end
%% -----------------------------------------------------------------------------
-spec split_node(size(), size(), depth(), t(), art_node:id()) ->
    NewNode :: art_node:id().

split_node(PrefixLen0, PrefixDiff, Depth, Trie, NodeId0) ->
    NewNode0 = art_node:new(),
    NewNode1 = art_node:set_prefix_len(NewNode0, PrefixDiff),

    Prefix0 = art_node:prefix(Trie, NodeId0),
    NewNodePrefix = art_utils:prefix(
        Prefix0, 0, min(?MAX_PREFIX_LEN, PrefixDiff)
    ),

    NewNode2 = art_node:set_prefix(NewNode1, NewNodePrefix),

    true = ets:insert(Trie#trie.tab, NewNode2),
    NewNodeId = art_node:id(NewNode2),

    case PrefixLen0 =< ?MAX_PREFIX_LEN of
        true ->
            %% We update the prefix of the old node
            PrefixLen1 = PrefixLen0 - (PrefixDiff + 1),
            _ = art_node:set_prefix_len(Trie, NodeId0, PrefixLen1),

            Prefix1Len = min(?MAX_PREFIX_LEN, PrefixLen1),
            Prefix1 = art_utils:prefix(Prefix0, PrefixDiff + 1, Prefix1Len),
            _ = art_node:set_prefix(Trie, NodeId0, Prefix1),

            %% We add old Node as a child of NewNode
            Index = art_utils:index(Prefix0, PrefixDiff),
            ok = add_child(Index, NodeId0, Trie, NewNodeId),
            NewNodeId;

        false ->
            %% We update the prefix of the old node
            PrefixLen1 = PrefixLen0 - (PrefixDiff + 1),
            {{LeafKey, _}, _} = first(NodeId0, Trie),
            _ = art_node:set_prefix_len(Trie, NodeId0, PrefixLen1),

            Prefix1Size = min(?MAX_PREFIX_LEN, PrefixLen1),
            Prefix1 = art_utils:prefix(
                LeafKey, Depth + PrefixDiff + 1, Prefix1Size
            ),
            _ = art_node:set_prefix(Trie, NodeId0, Prefix1),

            %% We add old Node as a child of NewNode
            Index = art_utils:index(LeafKey, Depth + PrefixDiff),
            ok = add_child(Index, NodeId0, Trie, NewNodeId),
            NewNodeId
    end.


%% -----------------------------------------------------------------------------
%% @private
%% @doc Function used by do_store/4
%% @end
%% -----------------------------------------------------------------------------
-spec merge_leaves(art_node:leaf(), art_node:leaf(), t(), depth()) ->
    art_node:id().

merge_leaves(OldLeaf, NewLeaf, #trie{} = Trie, Depth0) ->
    {OldKey, _} = OldLeaf,
    {NewKey, _} = NewLeaf,
    {PrefixLen, Prefix} = longest_common_prefix(OldLeaf, NewLeaf, Depth0),

    %% We create a new node
    Node0 = art_node:new(),
    Node1 = art_node:set_prefix_len(Node0, PrefixLen),
    Node2 = art_node:set_prefix(Node1, Prefix),

    %% We extract the index for each leaf
    Depth1 = Depth0 + PrefixLen,
    OldIndex = art_utils:index(OldKey, Depth1),
    NewIndex = art_utils:index(NewKey, Depth1),

    %% We add the leafs to the node
    Node3 = art_node:set(Node2, OldIndex, OldLeaf),
    Node4 = art_node:set(Node3, NewIndex, NewLeaf),

    %% We store the node in ets
    true = ets:insert(Trie#trie.tab, Node4),
    ok = incr_size(Trie),
    art_node:id(Node4).


%% @private
longest_common_prefix({OldKey, _}, {NewKey, _}, Depth) ->
    MaxLen = min(byte_size(OldKey), byte_size(NewKey)) - Depth,
    <<_:Depth/binary, OldSuffix:MaxLen/binary, _/binary>> = OldKey,
    <<_:Depth/binary, NewSuffix:MaxLen/binary, _/binary>> = NewKey,
    Len = binary:longest_common_prefix([OldSuffix, NewSuffix]),
    PrefixLen = min(Len, ?MAX_PREFIX_LEN),
    <<Prefix:PrefixLen/binary, _/binary>> = NewSuffix,
    {Len, Prefix}.


%% -----------------------------------------------------------------------------
%% @private
%% @doc Function used by do_store/4
%% @end
%% -----------------------------------------------------------------------------
add_child(Index, {_, Value} = Leaf, #trie{} = Trie, NodeId)
when is_integer(NodeId) ->
    case art_node:store(Trie, NodeId, Index, Leaf) of
        undefined when Value == undefined ->
            ok;
        undefined ->
            incr_size(Trie);
        {value, _} ->
            ok
    end;

add_child(Index, Child, #trie{} = Trie, NodeId) when is_integer(Child) ->
    _ = art_node:store(Trie, NodeId, Index, Child),
    ok.


%% -----------------------------------------------------------------------------
%% @private
%% @doc
%% @end
%% -----------------------------------------------------------------------------
remove_child(Index, Leaf, Trie, NodeId) when is_integer(NodeId) ->
    %% It cannot fail, otherwise we've done something wrong before
    Before = art_node:to_list(Trie, NodeId),
    Leaf = art_node:take(Trie, NodeId, Index),

    %% If the node has only one element we need to compress paths
    case art_node:size(Trie, NodeId) =:= 1 of
        true ->
            %% We demote the node to a leaf
            First = art_node:first(Trie, NodeId),
            case First of
                undefined ->
                    exit(#{
                        index => Index,
                        before => Before,
                        node_size => 1,
                        'after' => art_node:to_list(Trie, NodeId),
                        taken => Leaf
                    });
                {_, {_, _} = LeafChild} ->
                    true = ets:delete(Trie#trie.tab, NodeId),
                    LeafChild;
                {RIndex, ChildId} ->
                    %% We concatenate the prefixes
                    NodePLen0 = art_node:prefix_len(Trie, NodeId),
                    NodeP0 = art_node:prefix(Trie, NodeId),
                    ChildPLen = art_node:prefix_len(Trie, ChildId),

                    {NodeP1, NodePLen1} = case NodePLen0 < ?MAX_PREFIX_LEN of
                        true ->
                            Char = list_to_binary([RIndex]),
                            {<<NodeP0/binary, Char/binary>>, NodePLen0 + 1};
                        false ->
                            {NodeP0, NodePLen0}
                    end,

                    {NodeP2, NodePLen2} = case NodePLen1 < ?MAX_PREFIX_LEN of
                        true ->
                            ChildP = art_node:prefix(Trie, ChildId),
                            SubPLen = min(
                                ChildPLen, ?MAX_PREFIX_LEN - NodePLen1),
                            SubP = art_utils:prefix(ChildP, 0, SubPLen),
                            {
                                <<NodeP1/binary, SubP/binary>>,
                                NodePLen1 + SubPLen
                            };
                        false ->
                            {NodeP1, NodePLen1}
                    end,

                    FinalPrefix = art_utils:prefix(
                        NodeP2, 0, min(NodePLen2, ?MAX_PREFIX_LEN)),
                    art_node:set_prefix(Trie, ChildId, FinalPrefix),
                    art_node:set_prefix_len(
                        Trie, ChildId, ChildPLen + NodePLen0 + 1),

                    true = ets:delete(Trie#trie.tab, NodeId),
                    ChildId
            end;
        false ->
            NodeId
    end.


%% -----------------------------------------------------------------------------
%% @private
%% @doc Function used by get/2 to find an exact match of the Key.
%% @end
%% -----------------------------------------------------------------------------
search(_, _, _, undefined) ->
    error;

search(K, _, {K, V}, _) ->
    {ok, V};

search(_, _, {_, _}, _) ->
    error;

search(Key, Trie, NodeId, Depth0) when is_integer(NodeId) ->
    case search_child(Key, Trie, NodeId, Depth0) of
        {ok, {Child, _}, Depth1} ->
            search(Key, Trie, Child, Depth1 + 1);
        error ->
            error
    end.


%% -----------------------------------------------------------------------------
%% @private
%% @doc Returns the next child (and its index and depth) matching Key
%% or the atom `error'.
%% used by search/3 and delete_search/4
%% @end
%% -----------------------------------------------------------------------------
search_child(<<>>, _, _, _) ->
    error;

search_child(Key, Trie, NodeId, Depth0) ->
    {PrefixLen, LCP} = art_node:longest_common_prefix(
        Trie, NodeId, Key, Depth0
    ),
    case LCP =:= min(?MAX_PREFIX_LEN, PrefixLen) of
        true ->
            Depth1 = Depth0 + PrefixLen,
            Index = art_utils:index(Key, Depth1),
            case art_node:get(Trie, NodeId, Index) of
                undefined ->
                    error;
                Child ->
                    {ok, {Child, Index}, Depth1}
            end;
        false ->
            error
    end.


%% -----------------------------------------------------------------------------
%% @private
%% @doc Function used by take/2
%% @end
%% -----------------------------------------------------------------------------

delete_search(_, _, undefined, _, _) ->
    %% The trie was empty
    error;

delete_search(Key, #trie{id = Id} = Trie, {Key, OldValue}, _, [Id]) ->
    ok = decr_size(Trie),
    delete_search_unroll([], OldValue, undefined, Trie);

delete_search(Key, Trie, {Key, OldValue} = Leaf, _,  [{NodeId, Index} | Acc]) ->
    %% We found a match, we delete the leaf from the parent node
    NodeIdOrLeaf = remove_child(Index, Leaf, Trie, NodeId),
    ok = decr_size(Trie),
    %% We start unrolling towards the root
    delete_search_unroll(Acc, OldValue, NodeIdOrLeaf, Trie);

delete_search(_, _, {_, _}, _, _) ->
    %% We reached a leaf whose key does not match
    error;

delete_search(Key, Trie, NodeId, Depth0, Acc0) ->
    case search_child(Key, Trie, NodeId, Depth0) of
        {ok, {Child, Index}, Depth1} ->
            Acc1 = [{NodeId, Index} | Acc0],
            delete_search(Key, Trie, Child, Depth1 + 1, Acc1);
        error ->
            error
    end.


%% @private
delete_search_unroll([], OldValue, NewChild, Trie) ->
    Id = Trie#trie.id,
    %% We reached the top
    true = ets:update_element(Trie#trie.tab, Id, {#trie.root, NewChild}),
    {value, OldValue};

delete_search_unroll([Id], OldValue, NewChild, #trie{id = Id} = Trie) ->
    %% We reached the top
    true = ets:update_element(Trie#trie.tab, Id, {#trie.root, NewChild}),
    {value, OldValue};

delete_search_unroll([{ParentId, Index} | T], OldValue, NewChild, Trie) ->
    _ = art_node:set(Trie, ParentId, Index, NewChild),
    delete_search_unroll(T, OldValue, ParentId, Trie).



%% -----------------------------------------------------------------------------
%% @private
%% @doc Function used by fold/3
%% @end
%% ---------------------
do_fold({K, V}, _, {Trie, Fun, Acc0}) ->
    Acc = try
        Fun(K, V, Acc0)
    catch
        throw:{break, Acc1} ->
            throw({break, {Trie, Fun, Acc1}});
        throw:{not_a_pattern, Acc1} ->
            throw({break, {Trie, Fun, Acc1}})
    end,
    {Trie, Fun, Acc};

do_fold(NodeId, Opts, {Trie, Fun, Acc} = State) when is_map(Opts) ->
    case ets:lookup(Trie#trie.tab, NodeId) of
        [] ->
            State;
        [Node] ->
            art_node:foldl(Node, node_fold(Opts), Opts, {Trie, Fun, Acc})
    end.


%% @private
node_fold(Opts) ->
    fun(_Index, Value, {Trie, Fun, Acc}) ->
        do_fold(Value, Opts, {Trie, Fun, Acc})
    end.


%% @private
incr_size(Trie) ->
    update_counter(Trie, 1).


%% @private
decr_size(Trie) ->
    update_counter(Trie, -1).


%% @private
update_counter(#trie{id = Id, tab = Tab} = Trie, N) ->
    try
        _ = ets:update_counter(Tab, Id, {#trie.size, N}),
        ok
    catch
        error:badarg ->
            error({badtrie, Trie})
    end.


to_dot(T, Filename) ->
    to_dot(T, Filename, #{}).

to_dot(T, Filename, Opts) ->
    Trie = get_trie(T),
    Node = root(T),

    case file:open(Filename, [write, {encoding, utf8}]) of
        {ok, File} ->
            %% Header
            io:format(File, "digraph art {~n", []),
            io:format(
                File, "node [shape=Mrecord, fontname=\"Helvetica\"];~n", []),
            io:format(File, "trie [label=\"trie|~p\"];~n", [T]),
            case Node of
                undefined ->
                    ok;
                {Key, _} ->
                    Id = erlang:phash2(Key),
                    io:format(File, "trie -> ~w;~n", [Id]);
                NodeId ->
                    io:format(File, "trie -> ~w;~n", [NodeId])
            end,
            ok = to_dot(Node, Trie, File, Opts),

            %% Finish and close file
            io:format(File, "}~n", []),
            file:close(File);
        {error, _} = Error ->
            Error
    end.


%% @private
to_dot({Key, Value}, #trie{}, File, _Opts) ->
    io:format(
        File,
        "~w [shape=\"record\" label=\"{key   |'~s'}|{value|~s}\"];~n",
        [erlang:phash2(Key), v2s(Key, <<$\31>>, "[sep]"), v2s(Value)]
    );

to_dot(NodeId, #trie{} = Trie, File, Opts) ->
    Seq = lists:seq(0, art_node:capacity(Trie, NodeId) - 1),
    Escape = fun
        (<<0>>) -> "0";
        (<<?KEY_SEP>>) -> "[sep]";
        (<<>>) -> "[nil]";
        (X) -> X
    end,
    Utf8 = fun
        (0) -> "";
        (25) -> "[sep]";
        (N) -> io_lib:format("&#~w;", [N])
    end,

    Fun = fun(X, Acc) ->
        Str = Utf8(X),
        case art_node:get(Trie, NodeId, X) of
            undefined ->
                Acc;
            {_, _} ->
                Field = io_lib:format(
                    "{<f~w> ~w: ~s}", [X, X, Str]),
                [Field|Acc];
            _ChildId ->
                Field = io_lib:format(
                    "{<f~w> ~w: ~s}", [X, X, Str]),
                [Field|Acc]
        end
    end,
    Elements0 = lists:foldl(Fun, [], Seq),

    Elements1 = lists:join(" | ", lists:reverse(Elements0)),
    Prefix = art_node:prefix(Trie, NodeId),
    Len = art_node:prefix_len(Trie, NodeId),

    %% The dot vertex line
    case maps:get(node_identifiers, Opts, true) of
        true ->
            io:format(
                File,
                "~w [label=\"{~w | {prefix: ~s|len: ~s}| {~s}}\"];~n",
                [NodeId, NodeId, Escape(Prefix), v2s(Len), Elements1]
            );
        false ->
            io:format(
                File,
                "~w [label=\"{prefix: ~s | {~s}}\"];~n",
                [NodeId, Escape(Prefix), Elements1]
            )
    end,

    %% We create the edges and recurse to generate the nodes
    [
        begin
            case art_node:get(Trie, NodeId, X) of
                undefined ->
                    ok;
                {K, _}  = Leaf ->
                    %% We generate the leaf node
                    ok = to_dot(Leaf, Trie, File, Opts),

                    io:format(
                        File,
                        "~w:<f~w> -> ~w;~n",
                        [NodeId, X, erlang:phash2(K)]
                    );
                ChildId ->
                     %% We generate the child node
                     ok = to_dot(ChildId, Trie, File, Opts),

                    io:format(
                        File,
                        "~w:<f~w> -> ~w;~n",
                        [NodeId, X, ChildId]
                    )
            end
        end || X <- Seq
    ],
    ok.


%% @private
maybe_unpack(Bin) when is_binary(Bin) ->
    maybe_unpack(binary:split(Bin, <<?KEY_SEP>>, [global]));

maybe_unpack([Key]) ->
    Key;

maybe_unpack(Fields) when is_list(Fields) ->
    list_to_tuple(Fields).


%% @private
v2s(Term) when is_binary(Term) ->
    Term;
v2s(Term) ->
    lists:flatten(io_lib:format("~w", [Term])).


v2s(Term, Pattern, Replacement) when is_binary(Term) ->
    re:replace(Term, Pattern, Replacement, [global]);

v2s(Term, Pattern, Replacement) ->
    Term1 = re:replace(Term, Pattern, Replacement, [global]),
    lists:flatten(io_lib:format("~w", [Term1])).





%% =============================================================================
%% Borrowed from ets.erl
%% =============================================================================


print_info(Tab) ->
    case catch print_info2(Tab) of
	{'EXIT', _} ->
	    io:format("~-10s ... unreadable \n", [to_string(Tab)]);
	ok ->
	    ok
    end.

print_info2(Tab) ->
    Size = ?MODULE:size(Tab),
    Nodes = ets:info(Tab, size),
    Mem = ets:info(Tab, memory),
    Owner = ets:info(Tab, owner),
    hform(Tab, Size, Nodes, Mem, is_reg(Owner)).


is_reg(Owner) ->
    case process_info(Owner, registered_name) of
	{registered_name, Name} -> Name;
	_ -> Owner
    end.

%%% Arndt: this code used to truncate over-sized fields. Now it
%%% pushes the remaining entries to the right instead, rather than
%%% losing information.
hform(A0, B0, C0, D0, E0) ->
    [A,B,C,D,E] = [to_string(T) || T <- [A0,B0,C0,D0,E0]],
    A1 = pad_right(A, 15),
    B1 = pad_right(B, 12),
    C1 = pad_right(C, 12),
    D1 = pad_right(D, 12),
    %% no need to pad the last entry on the line
    io:format(" ~s ~s ~s ~s ~s\n", [A1,B1,C1,D1,E]).

pad_right(String, Len) ->
    if
	length(String) >= Len ->
	    String;
	true ->
	    [Space] = " ",
	    String ++ lists:duplicate(Len - length(String), Space)
    end.

to_string(X) ->
    lists:flatten(io_lib:format("~p", [X])).



