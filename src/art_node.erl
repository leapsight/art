%% -----------------------------------------------------------------------------
%% @doc An ordered dictionary that can contain up to 255 entries,
%% where keys are integers in the range 0..255 and values are binaries
%% with a maximum size of 2^32.
%% Search time is O(n) where n is the total number of entries, so it gets
%% very slow with n > 16. As such, it is intended not as a general purpose
%% data structure but as part of the Adaptive Radix Trie data structure
%% to implement the NODE4 and NODE16 types.
%% For this use case, when compared to a binary search alternative
%% implemented using binaries, this data structure uses less memory as it
%% keeps the indices (keys) at 1 byte and has no additional storage overhead.
%% @end
%% -----------------------------------------------------------------------------
-module(art_node).
-include("art.hrl").

-ifdef(TEST).
-define(UNIQUE_INTEGER, erlang:unique_integer([positive])).
-else.
-define(UNIQUE_INTEGER, erlang:unique_integer()).
-endif.

%% -define(MAX_SIZE, 16).
%% -define(COUNT_SIZE, 8).
-define(MAX_SIZE, 256).
-define(COUNT_SIZE, 16).
%% The art_node is a tuple of 6 + N storage elements where N is 4, 16, 48 or 256
-define(POS_OFFSET, 7).
-define(INDEX_POS(Index), Index + ?POS_OFFSET).
-define(ID_POS, 2).
-define(CAPACITY_POS, 3).
-define(SIZE_POS, 4).
-define(PREFIX_LEN_POS, 5).
-define(PREFIX_POS, 6).

-type t()           ::  t4() | t16() | t48() | t256().
-type t4()          ::  {
    art_node, id(), 4, size(), pos_integer(), binary(),
    e(), e(), e(), e(), e(), e(), e(), e()
}.
-type t16()          ::  {
    art_node, id(), 16, size(), pos_integer(), binary(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e()
}.
-type t48()          ::  {
    art_node, id(), 48, size(), pos_integer(), binary(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e()
}.
-type t256()          ::  {
    art_node, id(), 48, size(), pos_integer(), binary(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e(),
    e(), e(), e(), e(), e(), e(), e(), e()
}.
-type e()           ::  undefined | leaf() | id().
-type id()          ::  integer().
-type leaf()        ::  {key(), value()}.
-type key()         ::  binary().
-type value()       ::  any().
-type capacity()    ::  4 | 16 | 48 | 256.
-type size()        ::  0..4 | 0..16 | 0..48 | 0..256.
-type index()       ::  0..255.
-type depth()       ::  non_neg_integer().
-type fold_fun()    ::  fun(
                            (Key :: key(), Value :: value(), AccIn :: any()) ->
                                AccOut :: any()
                        ).
-type fold_opts()   ::  #{first => index()}.

-export_type([capacity/0]).
-export_type([depth/0]).
-export_type([e/0]).
-export_type([fold_fun/0]).
-export_type([fold_opts/0]).
-export_type([id/0]).
-export_type([index/0]).
-export_type([key/0]).
-export_type([leaf/0]).
-export_type([size/0]).
-export_type([t/0]).
-export_type([value/0]).


% -export([foldr/4]).
-export([capacity/1]).
-export([capacity/2]).
-export([delete/2]).
-export([delete/3]).
-export([first/1]).
-export([first/2]).
-export([foldl/3]).
-export([foldl/4]).
-export([foldr/3]).
-export([from_list/1]).
-export([get/2]).
-export([get/3]).
-export([id/1]).
-export([is_full/1]).
-export([is_full/2]).
-export([is_node/1]).
-export([last/1]).
-export([last/2]).
-export([longest_common_prefix/3]).
-export([longest_common_prefix/4]).
-export([new/0]).
-export([new/1]).
-export([next/2]).
-export([next/3]).
-export([prefix/1]).
-export([prefix/2]).
-export([prefix_len/1]).
-export([prefix_len/2]).
-export([prev/2]).
-export([prev/3]).
-export([set/3]).
-export([set/4]).
-export([set_prefix/2]).
-export([set_prefix/3]).
-export([set_prefix_len/2]).
-export([set_prefix_len/3]).
-export([size/1]).
-export([size/2]).
-export([store/3]).
-export([store/4]).
-export([take/2]).
-export([take/3]).
-export([to_list/1]).
-export([to_list/2]).
-export([to_orddict/1]).
-export([to_orddict/2]).

-compile({no_auto_import, [size/1]}).



%% =============================================================================
%% API
%% =============================================================================



%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec new() -> t().

new() ->
    new(256).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec new(Size :: size()) -> t().

new(4 = N) ->
    %% For each element we have two fields
    %% index1, element1, ... indexN, elementN
    %% thus N * 2 fields
    %% TODO maybe use a binary node i.e. bintrie idea
    erlang:make_tuple(6 + (N * 2), undefined, init(N));

new(16 = N) ->
    %% For each element we have two fields
    %% index1, element1, ... indexN, elementN
    %% thus N * 2 fields
    %% TODO maybe use a binary node i.e. bintrie idea
    erlang:make_tuple(6 + (N * 2), undefined, init(N));

new(48 = N) ->
    %% For each element we have two fields
    %% index1, element1, ... indexN, elementN
    %% thus N * 2 fields
    %% TODO maybe use a binary node i.e. bintrie idea
    erlang:make_tuple(6 + (N * 2), undefined, init(N));

new(256 = N) ->
    erlang:make_tuple(6 + N, undefined, init(N)).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec is_node(t()) -> boolean().

is_node(T) when element(1, T) =:= art_node ->
    true;

is_node(_) ->
    false.


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec from_list([{key(), value()}]) -> t().

from_list(L) when length(L) =< ?MAX_SIZE ->
    do_from_list(L, new(256)).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec id(T :: t()) -> id() | no_return().

id(T) ->
    is_node(T) orelse error(badarg, [T]),
    element(?ID_POS, T).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec capacity(T :: t()) -> capacity() | no_return().

capacity(T) when is_tuple(T), element(1, T) == art_node ->
    is_node(T) orelse error(badarg, [T]),
    element(?CAPACITY_POS, T).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec capacity(trie(), id()) -> capacity() | no_return().

capacity(#trie{tab = Tab}, Id) when is_integer(Id) ->
    ets:lookup_element(Tab, Id, ?CAPACITY_POS).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec size(T :: t()) -> size() | no_return().

size(T) when is_tuple(T), element(1, T) == art_node ->
    is_node(T) orelse error(badarg, [T]),
    element(?SIZE_POS, T).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec size(trie(), id()) -> size() | no_return().

size(#trie{tab = Tab}, Id) when is_integer(Id) ->
    ets:lookup_element(Tab, Id, ?SIZE_POS).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec is_full(t()) -> boolean().

is_full(T) when is_tuple(T), element(1, T) == art_node ->
    size(T) =:= capacity(T).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec is_full(trie(), id()) -> boolean().

is_full(Trie, Id) when is_integer(Id) ->
    size(Trie, Id) =:= capacity(Trie, Id).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec prefix_len(t()) -> non_neg_integer() | no_return().

prefix_len(T) when is_tuple(T), element(1, T) == art_node ->
    is_node(T) orelse error(badarg, [T]),
    erlang:element(?PREFIX_LEN_POS, T).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec prefix_len(trie(), id()) -> non_neg_integer() | no_return().

prefix_len(#trie{tab = Tab}, Id) when is_integer(Id) ->
    ets:lookup_element(Tab, Id, ?PREFIX_LEN_POS).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
set_prefix_len(T, PrefixLen)
when is_integer(PrefixLen) andalso PrefixLen >= 0 ->
    is_node(T) orelse error(badarg, [T, PrefixLen]),
    erlang:setelement(?PREFIX_LEN_POS, T, PrefixLen);

set_prefix_len(_, _) ->
    error(badarg).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
set_prefix_len(#trie{tab = Tab}, Id, PrefixLen)
when is_integer(PrefixLen) andalso PrefixLen >= 0 ->
    ets:update_element(Tab, Id, {?PREFIX_LEN_POS, PrefixLen});

set_prefix_len(_, _, _) ->
    error(badarg).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
prefix(T) ->
    is_node(T) orelse error(badarg, [T]),
    erlang:element(?PREFIX_POS, T).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
prefix(#trie{tab = Tab}, Id) ->
    ets:lookup_element(Tab, Id, ?PREFIX_POS).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
set_prefix(T, Prefix) when is_binary(Prefix) ->
    is_node(T) orelse error(badarg, [T]),
    erlang:setelement(?PREFIX_POS, T, Prefix).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
set_prefix(#trie{tab = Tab}, Id, Prefix) ->
    ets:update_element(Tab, Id, {?PREFIX_POS, Prefix}).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
set(Node, Index, Value) ->
    element(?ID_POS, store(Node, Index, Value)).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
set(Trie, Id, Index, Value) ->
    store(Trie, Id, Index, Value).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec store(T :: t(), Index :: index(), Value :: value()) ->
    {undefined | value(), NewT :: t()}.

store(T, Index, Value) when element(?CAPACITY_POS, T) =:= 256 ->
    case element(?INDEX_POS(Index), T) of
        Value ->
            Value;
        Old when Value == undefined ->
            {Old, setelement(?INDEX_POS(Index), decr_size(T), Value)};
        undefined ->
            {undefined, setelement(?INDEX_POS(Index), incr_size(T), Value)};
        Old ->
            {Old, setelement(?INDEX_POS(Index), T, Value)}
    end;

store(_T, _Index, _Value) ->
    error(not_implemented).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec store(Trie :: trie(), id(), index(), value()) ->
    undefined | value().

store(#trie{tab = Tab} = Trie, Id, Index, Value) ->
    Pos = ?INDEX_POS(Index),
    case ets:lookup_element(Tab, Id, Pos) of
        Value ->
            Value;
        Old when Value == undefined ->
            _ = ets:update_element(Tab, Id, {Pos, Value}),
            _ = decr_size(Trie, Id),
            Old;
        undefined ->
            _ = ets:update_element(Tab, Id, {Pos, Value}),
            _ = incr_size(Trie, Id),
            undefined;
        Old ->
            _ = ets:update_element(Tab, Id, {Pos, Value}),
            Old
    end.


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec first(T :: t()) ->
    {FirstIndex :: index(), id() | leaf()}
    | undefined
    | no_return().

first(T) when element(?CAPACITY_POS, T) =:= 256 ->
    Index = 0,

    case get(T, 0) of
        undefined ->
            next(T, Index);
        Value ->
            {Index, Value}
    end;

first(_) ->
    error(not_implemented).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec first(Trie :: trie(), NodeId :: id()) ->
    {FirstIndex :: index(), id() | leaf()}
    | undefined
    | no_return().

first(Trie, Id) ->
    Index = 0,

    case get(Trie, Id, 0) of
        undefined ->
            next(Trie, Id, Index);
        Value ->
            {Index, Value}
    end.


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec next(T :: t(), Index :: index()) ->
    {ElementIndex :: index(), id() | leaf()}
    | undefined
    | no_return().

next(T, Index0) when element(?CAPACITY_POS, T) =:= 256 ->
    Index = Index0 + 1,

    case get(T, Index) of
        undefined when Index == 255 ->
            undefined;
        undefined ->
            next(T, Index);
        Value ->
            {Index, Value}
    end;

next(_, _) ->
    error(not_implemented).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec next(Trie :: trie(), NodeId :: id(), Index :: index()) ->
    {ElementIndex :: index(), id() | leaf()} | undefined | no_return().

next(Trie, Id, Index0) ->
    Index = Index0 + 1,

    case get(Trie, Id, Index) of
        undefined when Index == 255 ->
            undefined;
        undefined ->
            next(Trie, Id, Index);
        Value ->
            {Index, Value}
    end.


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec prev(T :: t(), Index :: index()) ->
    {ElementIndex :: index(), id() | leaf()}
    | undefined
    | no_return().

prev(T, Index0) ->
    Index = Index0 - 1,

    case get(T, Index) of
        undefined when Index == 0 ->
            undefined;
        undefined ->
            prev(T, Index);
        Value ->
            {Index, Value}
    end.


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec prev(Trie :: trie(), NodeId :: id(), Index :: index()) ->
    {ElementIndex :: index(), id() | leaf()}
    | undefined
    | no_return().

prev(Trie, Id, Index0) ->
    Index = Index0 - 1,

    case get(Trie, Id, Index) of
        undefined when Index == 0 ->
            undefined;
        undefined ->
            prev(Trie, Id, Index);
        Value ->
            {Index, Value}
    end.


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec last(t()) ->
    {LastIndex :: index(), id() | leaf()}
    | undefined
    | no_return().

last(T) when element(?CAPACITY_POS, T) =:= 256 ->
    Index = 255,

    case get(T, Index) of
        undefined ->
            prev(T, Index);
        Value ->
            {Index, Value}
    end;

last(_T) ->
    error(not_implemented).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec last(Trie :: trie(), NodeId :: id()) ->
    {LastIndex :: index(), id() | leaf()}
    | undefined
    | no_return().

last(Trie, Id) ->
    Index = 255,

    case get(Trie, Id, Index) of
        undefined ->
            prev(Trie, Id, Index);
        Value ->
            {Index, Value}
    end.


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec get(T :: t(), Index :: index()) ->
    e() | no_return().

get(T, Index) when element(?CAPACITY_POS, T) =:= 256 ->
    element(?INDEX_POS(Index), T);

get(_T, _Index) ->
    error(not_implemented).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec get(Trie :: trie(), NodeId :: id(), Index :: index()) ->
    e() | no_return().

get(#trie{tab = Tab}, NodeId, Index) when Index >= 0 andalso Index =< 255 ->
    %% This is for t_256 only!
    ets:lookup_element(Tab, NodeId, ?INDEX_POS(Index)).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec delete(T :: t(), Index :: index()) -> t() | no_return().

delete(T0, Index) ->
    {_, T} = take(T0, Index),
    T.


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec delete(Trie :: trie(), NodeId :: id(), Index :: index()) ->
    ok | no_return().

delete(Trie, Id, Index) ->
    _ = take(Trie, Id, Index),
    ok.


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec take(T :: t(), INdex :: index()) ->
    {Value :: value() | undefined, NewT :: t()} | no_return().

take(T, Index) when element(?CAPACITY_POS, T) =:= 256 ->
    case element(?INDEX_POS(Index), T) of
        undefined ->
            {undefined, T};
        Old ->
            {Old, setelement(?INDEX_POS(Index), decr_size(T), undefined)}
    end;

take(_T, _Index) ->
    error(not_implemented).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec take(Trie :: trie(), NodeId :: id(), Index :: index()) ->
    Value :: value() | undefined | no_return().

take(Trie, Id, Index) ->
    Tab = Trie#trie.tab,
    Pos = ?INDEX_POS(Index),
    case ets:lookup_element(Tab, Id, Pos) of
        undefined ->
            undefined;
        Old ->
            _ = ets:update_element(Tab, Id, {Pos, undefined}),
            _ = decr_size(Trie, Id),
            Old
    end.


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec foldl(T :: t(), Fun :: fold_fun(), AccIn :: any()) -> AccOut :: any().

foldl(T, Fun, Acc) ->
    foldl(T, Fun, #{}, Acc).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec foldl(T :: t(), Fun :: fold_fun(), Opts :: map(), AccIn :: term()) ->
    AccOut :: term().

foldl(T, Fun, Opts, Acc) when is_function(Fun, 3) ->
    case size(T) > 0 of
        true -> do_foldl(Fun, Acc, T, Opts);
        false -> Acc
    end.


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec foldr(T :: t(), Fun :: fold_fun(), AccIn :: any()) -> AccOut :: any().

foldr(T, Fun, Acc) when is_function(Fun, 3) ->
    Size = size(T),
    case Size > 0 of
        true -> do_foldr(Fun, {Acc, Size}, T);
        false -> Acc
    end.


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
% -spec foldr(T :: t(), Fun :: fold_fun(), Opts :: map(), AccIn :: term()) ->
%     AccOut :: term().

% foldr(T, Fun, Opts, Acc) when is_function(Fun, 3) ->
%     case size(T) > 0 of
%         true -> do_foldr(Fun, Acc, T, Opts);
%         false -> Acc
%     end.


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
to_list(Node) ->
    to_orddict(Node).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
to_list(#trie{tab = Tab}, NodeId) ->
    [Node] = ets:lookup(Tab, NodeId),
    to_list(Node).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
to_orddict(Node) ->
    foldr(Node, fun(K, V, Acc) -> [{K, V} | Acc] end, []).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
to_orddict(#trie{tab = Tab}, NodeId) ->
    [Node] = ets:lookup(Tab, NodeId),
    foldr(Node, fun(K, V, Acc) -> [{K, V} | Acc] end, []).


%% -----------------------------------------------------------------------------
%% @doc Returns the number of prefix characters shared between the node and the
%% provided key.
%% @end
%% -----------------------------------------------------------------------------
-spec longest_common_prefix(T :: t(), Key :: key(), Depth :: depth()) ->
    Len :: non_neg_integer().

longest_common_prefix(T, Key, Depth) ->
    case size(T) > 0 of
        false ->
            0;
        true ->
            Prefix = prefix(T),
            KeySize = erlang:byte_size(Key),
            Limit = min(
                min(prefix_len(T), ?MAX_PREFIX_LEN), KeySize - Depth
            ),
            Part1 = art_utils:prefix(Prefix, 0, Limit),
            Part2 = art_utils:prefix(Key, Depth, Limit),
            binary:longest_common_prefix([Part1, Part2])
    end.


%% -----------------------------------------------------------------------------
%% @doc Returns the tuple `{PrefixLen, LCP}'. Where `LCP' is the number of
%% prefix characters shared between the node and the provided key together, and
%% `PrefixLen' the node's prefix length.
%% The LCP is limited to ?MAX_PREFIX_LEN.
%% @end
%% -----------------------------------------------------------------------------
-spec longest_common_prefix(
    Trie :: trie(), NodeId :: id(), Key :: key(), Depth :: depth()) ->
    {PrefLen :: non_neg_integer(), LCP :: non_neg_integer()}.

longest_common_prefix(Trie, Id, Key, Depth) ->
    Len = prefix_len(Trie, Id),
    case size(Trie, Id) > 0 of
        false ->
            {0, 0};
        true ->
            Prefix = prefix(Trie, Id),
            KeySize = erlang:byte_size(Key),
            Limit = min(
                min(Len, ?MAX_PREFIX_LEN),
                KeySize - Depth
            ),
            Part1 = art_utils:prefix(Prefix, 0, Limit),
            Part2 = art_utils:prefix(Key, Depth, Limit),
            LCP = binary:longest_common_prefix([Part1, Part2]),
            {Len, LCP}
    end.



%% =============================================================================
%% PRIVATE
%% =============================================================================



%% @private
init(N) ->
    [
        {1, art_node},
        {?ID_POS, ?UNIQUE_INTEGER},
        {?CAPACITY_POS, N},
        {?SIZE_POS, 0},
        {?PREFIX_LEN_POS, 0}
    ].

%% @private
incr_size(T) ->
    setelement(?SIZE_POS, T, size(T) + 1).


%% @private
incr_size(#trie{tab = Tab}, Id) ->
    ets:update_counter(Tab, Id, {?SIZE_POS, 1}).


%% @private
decr_size(T) ->
    setelement(?SIZE_POS, T, size(T) - 1).


%% @private
decr_size(#trie{tab = Tab}, Id) ->
    ets:update_counter(Tab, Id, {?SIZE_POS, -1, 0, 0}).


%% @private
do_from_list([], T) ->
    T;

do_from_list([{K, V} | T], Node) ->
    do_from_list(T, set(Node, K, V));

do_from_list(_, _) ->
    error(badarg).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec do_foldl(Fun :: fold_fun(), Acc0 :: any(), T :: t(), Opts :: map()) ->
    Acc1 :: any().

do_foldl(F, Acc, T, Opts) when is_tuple(T) ->
    First =?INDEX_POS(maps:get(first, Opts, 0)),
    do_foldl(F, T, tuple_size(T), First, Acc).


%% @private
do_foldl(F, T, Sz, N, Acc0) when N =< Sz ->
    case element(N, T) of
        undefined ->
            do_foldl(F, T, Sz, N + 1, Acc0);
        V ->
            try F(N - ?POS_OFFSET, V, Acc0) of
                Acc1 ->
                    do_foldl(F, T, Sz, N + 1, Acc1)
            catch
                throw:{break, Acc} ->
                    Acc

            end
    end;

do_foldl(_, _, _, _, Acc) ->
    Acc.


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec do_foldr(
    Fun :: fold_fun(), {Acc0 :: any(), Cnt :: non_neg_integer()}, T :: t()) ->
    AccOut :: any().

do_foldr(F, Acc, T) when is_tuple(T) ->
    do_foldr(F, T, tuple_size(T), 7, Acc).


%% @private
do_foldr(_, _, _, _, {Acc, 0}) ->
    %% We already covered the existing non undefined slots
    %% So we avoid continuing iterating
    Acc;

do_foldr(F, T, Sz, N, {Acc, Cnt}) when N =< Sz ->
    case element(N, T) of
        undefined ->
            do_foldr(F, T, Sz, N + 1, {Acc, Cnt});
        V ->
            try
                F(
                    N - ?POS_OFFSET,
                    V,
                    do_foldr(F, T, Sz, N + 1, {Acc, Cnt - 1})
                )
            catch
                throw:{break, Acc} -> Acc
            end
    end;

do_foldr(_, _, _, _, {Acc, _}) ->
    Acc.