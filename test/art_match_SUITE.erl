-module(art_match_SUITE).
-include_lib("common_test/include/ct.hrl").
-include_lib("stdlib/include/assert.hrl").

-compile(export_all).



all() ->
    [
        match,
        match_stored_pattern
    ].


loop() ->
    receive
        stop -> exit(normal);
        _ -> loop()
    end.

init_per_suite(Config) ->
    application:ensure_all_started(art),
    {ok, _Pid} = art_server:start(test),
    [{trie, test} | Config].

end_per_suite(Config) ->
    T = ?config(trie, Config),
    art_server:delete(T),
    {save_config, Config}.


match(Config) ->
    T = ?config(trie, Config),

    undefined = art_server:store(<<"a.b.c">>, 1, T),
    A = {<<"a.b.c">>, 1},

    ?assertEqual([A], art:match(<<"a.b.c">>, T, #{mode => exact})),
    ?assertEqual([], art:match(<<"a.b.c*">>, T, #{mode => exact})),
    ?assertEqual([], art:match({<<"a.b.c">>, <<>>}, T, #{mode => exact})),
    ?assertEqual([], art:match({<<"a.b.c">>, <<"x">>}, T, #{mode => exact})),
    ?assertEqual([], art:match({<<"a.b.c*">>, <<>>}, T, #{mode => exact})),
    ?assertEqual([], art:match({<<"a.b.c*">>, <<"x">>}, T, #{mode => exact})),


    ?assertEqual([A], art:match(<<"a.b.c">>, T, #{mode => pattern})),
    ?assertEqual([], art:match(<<"a.b.c*">>, T, #{mode => pattern})),
    ?assertEqual([], art:match({<<"a.b.c">>, <<>>}, T, #{mode => pattern})),
    ?assertEqual([], art:match({<<"a.b.c">>, <<"x">>}, T, #{mode => pattern})),
    ?assertEqual([], art:match({<<"a.b.c*">>, <<>>}, T, #{mode => pattern})),
    ?assertEqual([], art:match({<<"a.b.c*">>, <<"x">>}, T, #{mode => pattern})),

    undefined = art_server:store({<<"a.b.c">>, <<"x">>}, 2, T),
    B = {{<<"a.b.c">>, <<"x">>}, 2},

    ?assertEqual([A], art:match(<<"a.b.c">>, T, #{mode => exact})),
    ?assertEqual([B], art:match({<<"a.b.c">>, <<>>}, T, #{mode => exact})),
    ?assertEqual([B], art:match({<<"a.b.c">>, '_'}, T, #{mode => exact})),
    ?assertEqual([], art:match(<<"a.b.c*">>, T, #{mode => exact})),
    ?assertEqual([B], art:match({<<"a.b.c">>, <<>>}, T, #{mode => exact})),
    ?assertEqual([B], art:match({<<"a.b.c">>, <<"x">>}, T, #{mode => exact})),
    ?assertEqual([], art:match({<<"a.b.c">>, <<"y">>}, T, #{mode => exact})),
    ?assertEqual([], art:match({<<"a.b.c*">>, <<>>}, T), #{mode => exact}),
    ?assertEqual([], art:match({<<"a.b.c*">>, <<"x">>}, T, #{mode => exact})),

    ?assertEqual([A, B], art:match(<<"a.b.c">>, T, #{mode => pattern})),


    ?assertEqual([], art:match(<<"a.b.c*">>, T, #{mode => pattern})),


    ?assertEqual([B], art:match({<<"a.b.c">>, <<>>}, T, #{mode => pattern})),
    ?assertEqual([B], art:match({<<"a.b.c">>, <<"x">>}, T, #{mode => pattern})),


    ?assertEqual([], art:match({<<"a.b.c">>, <<"y">>}, T, #{mode => pattern})),


    ?assertEqual([], art:match({<<"a.b.c*">>, <<>>}, T), #{mode => pattern}),


    ?assertEqual([], art:match({<<"a.b.c*">>, <<"x">>}, T, #{mode => pattern})),


    undefined = art_server:store(<<"a.b.c*">>, 3, T),
    C = {<<"a.b.c*">>, 3},

    ?assertEqual([A], art:match(<<"a.b.c">>, T, #{mode => exact})),
    ?assertEqual([C], art:match(<<"a.b.c*">>, T, #{mode => exact})),
    ?assertEqual([B], art:match({<<"a.b.c">>, <<>>}, T, #{mode => exact})),
    ?assertEqual([B], art:match({<<"a.b.c">>, <<"x">>}, T, #{mode => exact})),
    ?assertEqual([], art:match({<<"a.b.c">>, <<"y">>}, T, #{mode => exact})),
    ?assertEqual([], art:match({<<"a.b.c*">>, <<>>}, T), #{mode => exact}),
    ?assertEqual([], art:match({<<"a.b.c*">>, <<"x">>}, T, #{mode => exact})),

    ?assertEqual([A, B, C], art:match(<<"a.b.c">>, T, #{mode => pattern})),
    ?assertEqual([C], art:match(<<"a.b.c*">>, T, #{mode => pattern})),
    ?assertEqual([B], art:match({<<"a.b.c">>, <<>>}, T, #{mode => pattern})),
    ?assertEqual([], art:match({<<"a.b.c*">>, <<>>}, T), #{mode => pattern}),
    ?assertEqual([], art:match({<<"a.b.c*">>, <<"x">>}, T, #{mode => pattern})),
    ok.


match_stored_pattern(Config) ->
    T = ?config(trie, Config),


    undefined = art_server:store(<<"foo">>, 1, T),
    undefined = art_server:store(<<"foo*">>, 1, T),

    ?assertEqual([], art:match(<<"f">>, T, #{mode => exact})),
    ?assertEqual([], art:match(<<"fo">>, T, #{mode => exact})),
    ?assertEqual([{<<"foo">>, 1}], art:match(<<"foo">>, T, #{mode => exact})),
    ?assertEqual([], art:match(<<"food">>, T, #{mode => exact})),

    ?assertEqual([], art:match({<<"f">>, <<>>}, T, #{mode => exact})),
    ?assertEqual([], art:match({<<"fo">>, <<>>}, T, #{mode => exact})),
    ?assertEqual([], art:match({<<"foo">>, <<>>}, T, #{mode => exact})),
    ?assertEqual([], art:match({<<"food">>, <<>>}, T, #{mode => exact})),

    ?assertEqual([], art:match({<<"f*">>, <<>>}, T, #{mode => exact})),
    ?assertEqual([], art:match({<<"fo*">>, <<>>}, T, #{mode => exact})),
    ?assertEqual([{<<"foo*">>, 1}], art:match(<<"foo*">>, T, #{mode => exact})),
    ?assertEqual([], art:match(<<"food*">>, T, #{mode => exact})),

    ?assertEqual([], art:match({<<"f*">>, <<"x">>}, T, #{mode => exact})),
    ?assertEqual([], art:match({<<"fo*">>, <<"x">>}, T, #{mode => exact})),
    ?assertEqual([], art:match({<<"foo*">>, <<"x">>}, T, #{mode => exact})),
    ?assertEqual([], art:match({<<"food*">>, <<"x">>}, T, #{mode => exact})),

    ?assertEqual(
        [
            {<<"foo">>, 1},
            {<<"foo*">>, 1}
        ],
        art:match(<<"foo">>, T, #{mode => pattern})
    ),

    ?assertEqual([], art:match({<<"foo">>, <<>>}, T)),


    ?assertEqual(
        [{<<"foo*">>, 1}],
        art:match(<<"foo*">>, T, #{mode => pattern})
    ),



    ?assertEqual([], art:match({<<"foo*">>, <<>>}, T, #{mode => pattern})),
    ?assertEqual([], art:match({<<"foo">>, <<"x">>}, T, #{mode => pattern})),
    ?assertEqual([], art:match({<<"foo*">>, <<"x">>}, T, #{mode => pattern})),


    undefined = art_server:store({<<"foo">>, <<"x">>}, 2, T),
    undefined = art_server:store({<<"foo*">>, <<"x">>}, 2, T),

    ?assertEqual([], art:match(<<"f">>, T, #{mode => exact})),
    ?assertEqual([], art:match(<<"fo">>, T, #{mode => exact})),
    ?assertEqual([], art:match(<<"food">>, T, #{mode => exact})),
    ?assertEqual([{<<"foo">>, 1}], art:match(<<"foo">>, T, #{mode => exact})),


    All = [
        {<<"foo">>, 1},
        {{<<"foo">>, <<"x">>}, 2},
        {<<"foo*">>, 1},
        {{<<"foo*">>, <<"x">>}, 2}
    ],

    ?assertEqual(All, art:match(<<"f">>, T, #{mode => pattern})),
    ?assertEqual(All, art:match(<<"fo">>, T, #{mode => pattern})),
    ?assertEqual(All, art:match(<<"foo">>, T, #{mode => pattern})),
    ?assertEqual([], art:match(<<"food">>, T, #{mode => pattern})),
    ?assertEqual([], art:match({<<"f">>, <<>>}, T, #{mode => pattern})),
    ?assertEqual([], art:match({<<"fo">>, <<>>}, T, #{mode => pattern})),
    ?assertEqual(
        [{{<<"foo">>, <<"x">>}, 2}],
        art:match({<<"foo">>, <<>>}, T, #{mode => pattern})
    ),
    ?assertEqual([], art:match({<<"food">>, <<>>}, T, #{mode => pattern})),



    ?assertEqual(
        [{{<<"foo">>, <<"x">>}, 2}],
        art:match({<<"foo">>, <<"x">>}, T, #{mode => exact})
    ),

    ?assertEqual(
        [{{<<"foo">>, <<"x">>}, 2}],
        art:match({<<"foo">>, <<>>}, T, #{mode => pattern})
    ),

    ?assertEqual(
        All,
        art:match(<<"f">>, T, #{mode => pattern})
    ),

    ?assertEqual(
        [{{<<"foo">>, <<"x">>}, 2}],
        art:match({<<"foo">>, <<"x">>}, T, #{mode => pattern})
    ),

    ?assertEqual([{<<"foo*">>, 1}], art:match(<<"foo*">>, T, #{mode => exact})),
    ?assertEqual(
        [
            {<<"foo*">>, 1},
            {{<<"foo*">>, <<"x">>}, 2}
        ],
        art:match(<<"foo*">>, T, #{mode => pattern})
    ),

    ?assertEqual(
        [{{<<"foo*">>, <<"x">>}, 2}],
        art:match({<<"foo*">>, <<>>}, T, #{mode => exact})
    ),

    ?assertEqual(
        [{{<<"foo*">>, <<"x">>}, 2}],
        art:match({<<"foo*">>, <<>>}, T, #{mode => pattern})
    ),

    ?assertEqual(
        [{{<<"foo*">>, <<"x">>}, 2}],
        art:match({<<"foo*">>, <<"x">>}, T, #{mode => exact})
    ),
    ?assertEqual(
        [{{<<"foo*">>, <<"x">>}, 2}],
        art:match({<<"foo*">>, <<"x">>}, T, #{mode => pattern})
    ),


    ok.
