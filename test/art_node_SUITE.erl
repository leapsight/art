-module(art_node_SUITE).
-include_lib("common_test/include/ct.hrl").
-include_lib("stdlib/include/assert.hrl").

-compile(export_all).



all() ->
    [
        first,
        art_node_1,
        art_node_2
    ].


loop() ->
    receive
        stop -> exit(normal);
        _ -> loop()
    end.

init_per_suite(Config) ->
    %% Pid = spawn(fun loop/0),
    application:ensure_all_started(art),
    {ok, _Pid} = art_server:start(test),
    %% _ = ets:setopts(T, {heir, Pid, []}),
    [{trie, test} | Config].

end_per_suite(Config) ->
    T = ?config(trie, Config),
    %% Pid = ets:info(T, heir),
    %% Pid ! stop,
    art_server:delete(T),
    {save_config, Config}.




%% =============================================================================
%% TESTS
%% =============================================================================



create(Config) ->
    T = ?config(trie, Config),
    0 = art:size(T),
    ok.

store(Config) ->
    T = art:get_trie(?config(trie, Config)),
    undefined = art_server:store(<<"a">>, <<"bar">>, T),
    <<"bar">> = art:get(<<"a">>, T),
    undefined = art_server:store(<<"abbbbbbbbb">>, <<"foo">>, T),
    <<"foo">> = art:get(<<"abbbbbbbbb">>, T),
    {value, <<"foo">>} = art_server:store(<<"abbbbbbbbb">>, <<"bar">>, T),
    <<"bar">> = art:get(<<"abbbbbbbbb">>, T),
    %% art:to_dot(T, "../../../../_out/test_1.dot"),
    2 = art:size(T),
    {value, <<"bar">>} = art_server:store(<<"abbbbbbbbb">>, <<"bar">>, T),
    <<"bar">> = art:get(<<"abbbbbbbbb">>, T),
    2 = art:size(T),
    undefined = art_server:store(<<"abbbbbbbbc">>, <<"foo">>, T),
    <<"foo">> = art:get(<<"abbbbbbbbc">>, T),
    %% art:to_dot(T, "../../../../_out/test_2.dot"),
    3 = art:size(T),
    undefined = art_server:store(<<"b">>, <<"bar">>, T),
    <<"bar">> = art:get(<<"b">>, T),
    %% art:to_dot(T, "../../../../_out/test_3.dot"),
    4 = art:size(T),

    undefined = art_server:store(<<"com.myapp">>, <<"100">>, T),
    {value, <<"100">>} = art_server:store(<<"com.myapp">>, <<"1">>, T),

    undefined = art_server:store(<<"com.myapp.event">>, <<"2">>, T),
    undefined = art_server:store(<<"com.myapp.event.type">>, <<"3">>, T),
    undefined = art_server:store(<<"com.myapp.event.type.subtype">>, <<"4">>, T),
    undefined = art_server:store(
        <<"com.myapp.event.type.subtype.1234567890">>, <<"5">>, T),
    undefined = art_server:store(
    <<"this.is.a.unique.very.large.key.that.i.am.using">>, <<"6">>, T),
    10 = art:size(T),

    undefined = art_server:store(<<"abbb">>, <<"foo">>, T),
    %% art:to_dot(T, "../../../../_out/test_4.dot"),
    <<"foo">> = art:get(<<"abbb">>, T),

    <<"1">> = art:get(<<"com.myapp">>, T),
    <<"2">> = art:get(<<"com.myapp.event">>, T),
    <<"3">> = art:get(<<"com.myapp.event.type">>, T),
    <<"4">> = art:get(<<"com.myapp.event.type.subtype">>, T),
    <<"5">> = art:get(<<"com.myapp.event.type.subtype.1234567890">>, T),
    <<"6">> = art:get(
        <<"this.is.a.unique.very.large.key.that.i.am.using">>, T),
    error = art:find(<<"com.myapp.event.type.subtype.1234567890.1">>, T),
    error = art:find(<<"com.myapp.event.type.subtype.1">>, T),
    error = art:find(<<"subtype.1234567890">>, T),
    error = art:find(<<"com.myapp.event.type.subtype.12345678901">>, T).

first(_) ->
    L = [{97, <<"a">>}],
    N = art_node:from_list(L),
    true = art_node:is_node(N),
    1 = art_node:size(N),
    ?assertEqual({97, <<"a">>}, art_node:first(N)).


art_node_1(_) ->
    L = [{97, <<"a">>}, {103, <<"g">>}, {114, <<"r">>}, {122, {<<"z">>, 1}}],
    N = art_node:from_list(L),
    true = art_node:is_node(N),
    4 = art_node:size(N),
    %% N = art_node:from_list(lists:reverse(L)),
    L = art_node:to_orddict(N),

    %% get
    {97, <<"a">>} = art_node:first(N),
    {122, {<<"z">>, 1}} = art_node:last(N),
    undefined = art_node:get(N, 1),
    <<"a">> = art_node:get(N, 97),
    <<"g">> = art_node:get(N, 103),
    <<"r">> = art_node:get(N, 114),
    {<<"z">>, 1} = art_node:get(N, 122),
    undefined = art_node:get(N, 255),

    %% Update
    N1 = art_node:set(N, 97, <<"b">>),
    <<"b">> = art_node:get(N1, 97),
    4 = art_node:size(N1),
    N2 = art_node:set(N1, 97, <<"a">>),
    <<"a">> = art_node:get(N2, 97),

    %% Update
    N3 = art_node:delete(N2, 114),
    undefined = art_node:get(N3, 114),
    3 = art_node:size(N3),

    %% Order
    N4 = art_node:set(N3, 114, <<"r">>),
    L = art_node:to_orddict(N4),
    %% N4 = art_node:from_list(L),

    %% Prefixes
    undefined = art_node:prefix(N4),
    0 = art_node:prefix_len(N4),

    P1 = <<"argie">>,
    _ = art_node:set_prefix(art_node:new(), P1),
    N5 = art_node:set_prefix(N4, P1),
    P1 = art_node:prefix(N5),
    N6 = art_node:set_prefix_len(N5, 16),
    16 = art_node:prefix_len(N6),
    P2 = <<"foo">>,
    N7 = art_node:set_prefix(N6, P2),
    P2 = art_node:prefix(N7),
    ok.


art_node_2(_) ->
    N = art_node:new(),
    Id = art_node:id(N),
    TrieMock = art:get_trie(
        art:new(foo, [public, set, {keypos, 2}, named_table])
    ),
    true = ets:insert(foo, N),
    art_node:store(TrieMock, Id, 0, <<"foo">>),
    1 = art_node:size(TrieMock, Id),
    art_node:store(TrieMock, Id, 1, <<"bar">>),
    art_node:store(TrieMock, Id, 1, <<"foo">>),
    2 = art_node:size(TrieMock, Id),
    art_node:store(TrieMock, Id, 2, <<"foobar">>),
    3 = art_node:size(TrieMock, Id),

    art_node:delete(TrieMock, Id, 2),
    2 = art_node:size(TrieMock, Id),

    art_node:take(TrieMock, Id, 1),
    1 = art_node:size(TrieMock, Id),
    [_] = art_node:to_list(TrieMock, Id),

    art_node:take(TrieMock, Id, 0),
    art_node:take(TrieMock, Id, 0),
    0 = art_node:size(TrieMock, Id),
    [] = art_node:to_list(TrieMock, Id),
    art_node:store(TrieMock, Id, 0, <<"foo">>),
    1 = art_node:size(TrieMock, Id),
    art_node:store(TrieMock, Id, 0, undefined),
    0 = art_node:size(TrieMock, Id).




