-module(prop_art).
-include_lib("proper/include/proper.hrl").
-include("art.hrl").

-define(TRIE, ?MODULE).

%% Model Callbacks
-export([
    command/1,
    initial_state/0,
    next_state/3,
    precondition/2,
    postcondition/3
]).



%% =============================================================================
%% PROPERTIES
%% =============================================================================



prop_test() ->
    ?FORALL(
        Cmds,
        commands(?MODULE),
        begin
            art_server:start_link(?TRIE),
            {History, State, Result} = run_commands(?MODULE, Cmds),
            art_server:stop(?TRIE),
            ?WHENFAIL(
                io:format(
                    "History: ~p\nState: ~p\nResult: ~p\n",
                    [History,State,Result]
                ),
                aggregate(command_names(Cmds), Result =:= ok))
        end
    ).



%% =============================================================================
%% MODEL
%% =============================================================================



%% @doc Initial model value at system start. Should be deterministic.
initial_state() ->
    application:ensure_all_started(art),
    #{}.

%% @doc List of possible commands to run against the system
command(_State) ->
    oneof([
        {call, art_server, set, [key(), value(), ?TRIE]},
        {call, art_server, store, [key(), value(), ?TRIE]},
        {call, art_server, find, [key(), ?TRIE]},
        {call, art_server, delete, [key(), ?TRIE]},
        {call, art_server, take, [key(), ?TRIE]},
        % {call, art_server, first, [?TRIE]},
        % {call, art_server, last, [?TRIE]},
        {call, art_server, match, [key(), ?TRIE]},
        {call, art_server, find_matches, [key(), ?TRIE]},
        {call, art_server, to_list, [?TRIE]}
    ]).


%% @doc Determines whether a command should be valid under the
%% current state.
precondition(_State, {call, _Mod, find_matches, [Key, _]}) ->
    case art_utils:is_pattern(Key) of
        {false, _} -> true;
        _ -> false
    end;

precondition(_State, {call, _Mod, match, [Key, _]}) ->
    case art_utils:is_pattern(Key) of
        %% TODO enable this when model is validated for everything else
        {true, {wildcard, _, _, _}} -> false;
        _ -> true
    end;

precondition(_State, {call, _Mod, _Fun, _Args}) ->
    true.





%% @doc Assuming the postcondition for a call was true, update the model
%% accordingly for the test to proceed.
next_state(State, _Res, {call, art_server, F, [K0, V, ?TRIE]})
when F == set; F == store ->
    K = maybe_unpack(K0),
    case maps:find(K, State) of
        {ok, _} ->
            State;
        error ->
            maps:put(K, V, State)
    end;

next_state(State, _Res, {call, art_server, delete, [K, ?TRIE]}) ->
    maps:remove(maybe_unpack(K), State);

next_state(State, _Res, {call, art_server, take, [K, ?TRIE]}) ->
    maps:remove(maybe_unpack(K), State);

next_state(State, _, _) ->
    State.


%% @doc Given the state `State' *prior* to the call
%% `{call, Mod, Fun, Args}', determine whether the result
%% `Res' (coming from the actual system) makes sense.
postcondition(State, {call, art_server, find, [K, ?TRIE]}, Res) ->
    case maps:find(K, State) of
        {ok, V} when Res == V ->
            true;
        error when Res == error ->
            true;
        _ ->
            false
    end;

postcondition(State, {call, art_server, match, [K, ?TRIE]}, Res) ->
    Expected =
        case art_utils:is_pattern(K) of
            {true, {wildcard, L, Prefix, Rest}} ->
                exit({K, {wildcard, L, Prefix, Rest}});

            {true, {prefix, Prefix, Rest}} ->
                maps:fold(
                    fun(K, V, Acc) ->
                        Len = byte_size(Prefix),
                        case binary:longest_common_prefix(Prefix, K) of
                            Len ->
                                [{K, V}];
                            _ ->
                                Acc
                        end
                    end,
                    [],
                    State
                );
            {false, {Prefix, Rest}} ->
                case art_match:matches(K, Prefix, exact, Rest) of
                    true ->
                        case maps:find(K, State) of
                            {ok, V} -> [{K, V}];
                            error -> []
                        end;
                    false ->
                        []
                end;

            false ->
                case maps:find(K, State) of
                    {ok, V} -> [{K, V}];
                    error -> []
                end
        end,

        lists:sort(Expected) == lists:sort(Res);


postcondition(State, {call, art_server, find_matches, [K, ?TRIE]}, Res) ->
    Expected = maps:fold(
        fun(SK, SV, Acc) ->
            % case is_pattern(SK) of
            %     false when SK == K ->
            %         %% exact match
            %         [{SK, SV} | Acc];
            %     {false, {Prefix, Rest}} ->

            %     {true, {prefix, Prefix, _}} ->

            %     {true, {wildcard, Terms, _Prefix, Rest}} ->
            case subsumes(K, SK) of
                true ->
                    [{SK, SV} | Acc];
                false ->
                    Acc
            end
        end,
        [],
        State
    ),


    case lists:sort(Expected) == lists:sort(Res) of
        true ->
             true;
        false ->
            exit({Expected, Res})
    end;

postcondition(State, {call, art_server, to_list, [?TRIE]}, Res) ->
    lists:sort(maps:to_list(State)) == lists:sort(Res);

postcondition(State, {call, art_server, take, [K, ?TRIE]}, error) ->
    maps:is_key(K, State) == false;

postcondition(State, {call, art_server, _, _}, Res) ->
    true.


subsumes(Term, Term) ->
    true;

subsumes(Key, Pattern) ->
    art_find_matches:matches(Key, Pattern).


%% =============================================================================
%% GENERATORS
%% =============================================================================



key() ->
    % utf8().
    L = [
        <<"*">>,
        <<"**">>,
        <<"***">>,
        <<"*foo">>,
        <<"*foo*bar">>,
        <<"...">>,
        <<".....">>,
        <<"..event.one">>,
        <<".myapp.event.one">>,
        <<"a.b.c">>,
        <<"a.b.c*">>,
        <<"c*", $\31, "1", $\31, "a">>,
        <<"c*", $\31, "1", $\31, "b">>,
        <<"c*", $\31, "2", $\31, "a">>,
        <<"c*">>,
        <<"c**">>,
        <<"com*">>,
        <<"com..event">>,
        <<"com.my*">>,
        <<"com.myapp">>,
        <<"com.myapp*">>,
        <<"com.myapp.event">>,
        <<"com.myapp.event">>,
        <<"com.myapp.event*">>,
        <<"com.myapp.event*.one">>,
        <<"com.myapp.event.one">>,
        <<"com.myapp.event.one.more">>,
        <<"com.myapp.event.type">>,
        <<"com.myapp.event.type.subtype">>,
        <<"com.myapp.event.type.subtype.1234567890">>,
        <<"com.myapp.event.type.subtype.1234567890.1">>,
        <<"f*">>,
        <<"fo*">>,
        <<"foo">>,
        <<"foo*">>,
        <<"subtype.1234567890">>,
        <<"this.is.a.unique.very.large.key.that.i.am.using">>
    ],
    oneof(L).


value() ->
    integer().


maybe_unpack(Bin) when is_binary(Bin) ->
    maybe_unpack(binary:split(Bin, <<?KEY_SEP>>, [global]));

maybe_unpack([Key]) ->
    Key;

maybe_unpack(Fields) when is_list(Fields) ->
    list_to_tuple(Fields).

