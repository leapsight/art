-module(art_find_matches_SUITE).
-include_lib("common_test/include/ct.hrl").
-include_lib("stdlib/include/assert.hrl").

-define(TRACE(Mods),
    begin
        dbg:tracer(),
        dbg:p(all,c),
        _ = [dbg:tpl(Mod, F, x) || {Mod, F} <- Mods],
        ok
    end
).

-compile(export_all).



all() ->
    [
        find_wildcards_1,
        find_wildcards_2,
        find_matches_exact,
        find_matches_prefix,
        find_matches_1,
        find_matches_2,
        find_matches_3,
        find_matches_4,
        find_matches_5,
        find_matches_6,
        find_matches_7,
        find_matches_8,
        find_matches_9,
        find_matches_10,
        find_matches_11,
        find_matches_12
    ].


loop() ->
    receive
        stop -> exit(normal);
        _ -> loop()
    end.


init_per_suite(Config) ->
    %% Pid = spawn(fun loop/0),
    application:ensure_all_started(art),
    {ok, _Pid} = art_server:start(test),
    ok = init_trie(test),
    %% _ = ets:setopts(T, {heir, Pid, []}),
    [{trie, test} | Config].


end_per_suite(Config) ->
    T = ?config(trie, Config),
    %% Pid = ets:info(T, heir),
    %% Pid ! stop,
    art_server:delete(T),
    dbg:stop(),
    {save_config, Config}.


init_per_testcase(_, Config) ->
    dbg:stop(),
    Config.


end_per_testcase(_, Config) ->
    Config.


%% =============================================================================
%% TESTS
%% =============================================================================



%% MATCHING


init_trie(T) ->
    L0 = [
        {<<"*">>, <<"prefix">>},
        {<<"**">>, <<"prefix">>},
        {<<"***">>, <<"prefix">>},
        {<<"*foo">>, <<"prefix">>},
        {<<"*foo*bar">>, <<"prefix">>},
        {<<"c*">>, <<"bar">>},
        {<<"c**">>, <<"prefix">>},
        {<<"c*", $\31, "1", $\31, "a">>, <<"bar">>},
        {<<"c*", $\31, "1", $\31, "b">>, <<"bar">>},
        {<<"c*", $\31, "2", $\31, "a">>, <<"bar">>},
        {<<"com*">>, <<"prefix">>},
        {<<"com.my*">>, <<"prefix">>},
        {<<"com.myapp*">>, <<"prefix">>},
        {<<"com.myapp.event">>, <<"full">>},
        {<<"com.myapp.event*">>, <<"prefix">>},
        {<<"com.myapp.event*.one">>, <<"full">>},
        {<<"com.myapp.event.one">>, <<"full">>},
        {<<"com.myapp.event.one.more">>, <<"full">>},
        {<<"..event.one">>, <<"wild">>},
        {<<".myapp.event.one">>, <<"wild">>},
        {<<"...">>, <<"wild">>},
        {<<".....">>, <<"wild">>},
        {<<"com..event">>, <<"wildcard">>},

        {{<<"foo">>, <<"1">>, <<"bar">>}, 1},
        {{<<"foo">>, <<"2">>, <<"bar">>}, 2},
        {{<<"foo">>, <<"3">>, <<"bar">>}, 3},
        {{<<"foo">>, <<"4">>, <<"bar">>}, 4},

        {{<<"*">>, <<"1">>, <<"bar">>}, prefix_1},
        {{<<"f*">>, <<"2">>, <<"bar">>}, prefix_2},
        {{<<"fo*">>, <<"3">>, <<"bar">>}, prefix_3},
        {{<<"foo*">>, <<"4">>, <<"bar">>}, {4, prefix}}
    ],
    L1 = lists_utils:shuffle(L0),

    _ = [
        undefined = art_server:store(K, V, T) || {K, V} <- L1
    ],
    ok.


find_wildcards_1(_) ->
    % dbg:tracer(), dbg:p(all, c),
    % dbg:tpl(art_find_matches, '_', []),
    T = art:new(),
    art:store(<<"...">>, 0, T),
    art:store(<<"...one">>, 0, T),
    art:store(<<"..event.">>, 0, T),
    art:store(<<"..event.one">>, 0, T),
    art:store(<<".myapp..">>, 0, T),
    art:store(<<".myapp..one">>, 0, T),
    art:store(<<".myapp.event.">>, 0, T),
    art:store(<<".myapp.event.one">>, 0, T),
    art:store(<<"com...">>, 0, T),
    art:store(<<"com...one">>, 0, T),
    art:store(<<"com..event.">>, 0, T),
    art:store(<<"com..event.one">>, 0, T),
    art:store(<<"com.myapp..">>, 0, T),
    art:store(<<"com.myapp..one">>, 0, T),
    art:store(<<"com.myapp.event.">>, 0, T),
    art:store(<<"com.myapp.event.one">>, 0, T),

    %% The following should fail
    art:store(<<"..">>, 0, T),
    art:store(<<"..event">>, 0, T),
    art:store(<<".myapp.">>, 0, T),
    art:store(<<".myapp.event">>, 0, T),
    art:store(<<"com..">>, 0, T),
    art:store(<<"com..">>, 0, T),
    art:store(<<"com..event">>, 0, T),
    art:store(<<"com.myapp.">>, 0, T),
    art:store(<<"com.myapp.event">>, 0, T),

    Expected0 = [
        {<<"...">>, 0},
        {<<"...one">>, 0},
        {<<"..event.">>, 0},
        {<<"..event.one">>, 0},
        {<<".myapp..">>, 0},
        {<<".myapp..one">>, 0},
        {<<".myapp.event.">>, 0},
        {<<".myapp.event.one">>, 0},
        {<<"com...">>, 0},
        {<<"com...one">>, 0},
        {<<"com..event.">>, 0},
        {<<"com..event.one">>, 0},
        {<<"com.myapp..">>, 0},
        {<<"com.myapp..one">>, 0},
        {<<"com.myapp.event.">>, 0},
        {<<"com.myapp.event.one">>, 0}
    ],
    Expected = ordsets:from_list(Expected0),
    art:to_dot(T, "../../../../_out/find_wildcards_1.dot"),
    Result = ordsets:from_list(art:find_matches(<<"com.myapp.event.one">>, T)),

    %% Relaxed condition: allowing duplicates
    ?assertEqual(
        ordsets:to_list(Expected),
        ordsets:to_list(Result),
        {missing, ordsets:subtract(Expected, Result)}
    ),
    %% Strict condition
    ?assertEqual(Expected, Result, "Not strictly correct, we have duplicates").



find_wildcards_2(_) ->
    T = art:new(),
    art:store(<<"...">>, 0, T),
    art:store(<<"...one">>, 0, T),
    art:store(<<"..event.">>, 0, T),
    art:store(<<"..event.one">>, 0, T),
    art:store(<<".myapp..">>, 0, T),
    art:store(<<".myapp..one">>, 0, T),
    art:store(<<".myapp.event.">>, 0, T),
    art:store(<<".myapp.event.one">>, 0, T),
    art:store(<<"com...">>, 0, T),
    art:store(<<"com...one">>, 0, T),
    art:store(<<"com..event.">>, 0, T),
    art:store(<<"com..event.one">>, 0, T),
    art:store(<<"com.myapp..">>, 0, T),
    art:store(<<"com.myapp..one">>, 0, T),
    art:store(<<"com.myapp.event.">>, 0, T),
    art:store(<<"com.myapp.event.one">>, 0, T),

    art:store({<<"...">>, <<"suffix">>}, 0, T),
    art:store({<<"...one">>, <<"suffix">>}, 0, T),
    art:store({<<"..event.">>, <<"suffix">>}, 0, T),
    art:store({<<"..event.one">>, <<"suffix">>}, 0, T),
    art:store({<<".myapp..">>, <<"suffix">>}, 0, T),
    art:store({<<".myapp..one">>, <<"suffix">>}, 0, T),
    art:store({<<".myapp.event.">>, <<"suffix">>}, 0, T),
    art:store({<<".myapp.event.one">>, <<"suffix">>}, 0, T),
    art:store({<<"com...">>, <<"suffix">>}, 0, T),
    art:store({<<"com...one">>, <<"suffix">>}, 0, T),
    art:store({<<"com..event.">>, <<"suffix">>}, 0, T),
    art:store({<<"com..event.one">>, <<"suffix">>}, 0, T),
    art:store({<<"com.myapp..">>, <<"suffix">>}, 0, T),
    art:store({<<"com.myapp..one">>, <<"suffix">>}, 0, T),
    art:store({<<"com.myapp.event.">>, <<"suffix">>}, 0, T),
    art:store({<<"com.myapp.event.one">>, <<"suffix">>}, 0, T),

    %% The following should fail
    art:store(<<"..">>, 0, T),
    art:store(<<"..other">>, 0, T),
    art:store(<<"..event">>, 0, T),
    art:store(<<"..eventmore">>, 0, T),
    art:store({<<"..eventmore">>, <<"suffix">>}, 0, T),
    art:store(<<"..event.on">>, 0, T),
    art:store({<<"..event.on">>, <<"suffix">>}, 0, T),
    art:store(<<"..event.one.two">>, 0, T),
    art:store({<<"..event.one.two">>, <<"suffix">>}, 0, T),
    art:store(<<"..event.onemore">>, 0, T),
    art:store({<<"..event.onemore">>, <<"suffix">>}, 0, T),
    art:store(<<".myapp.">>, 0, T),
    art:store({<<".myapp.">>, <<"suffix">>}, 0, T),
    art:store(<<".myapp.more">>, 0, T),
    art:store({<<".myapp.more">>, <<"suffix">>}, 0, T),
    art:store(<<".myapp.">>, 0, T),
    art:store(<<".myapp.event">>, 0, T),
    art:store(<<"com..">>, 0, T),
    art:store(<<"com..">>, 0, T),
    art:store(<<"com..event">>, 0, T),
    art:store(<<"com.myapp.">>, 0, T),
    art:store(<<"com.myapp.event">>, 0, T),
    art:store(<<"com..event.on">>, 0, T),
    art:store({<<"com..event.on">>, <<"suffix">>}, 0, T),

    Expected0 = [
        {<<"...">>, 0},
        {<<"...one">>, 0},
        {<<"..event.">>, 0},
        {<<"..event.one">>, 0},
        {<<".myapp..">>, 0},
        {<<".myapp..one">>, 0},
        {<<".myapp.event.">>, 0},
        {<<".myapp.event.one">>, 0},
        {<<"com...">>, 0},
        {<<"com...one">>, 0},
        {<<"com..event.">>, 0},
        {<<"com..event.one">>, 0},
        {<<"com.myapp..">>, 0},
        {<<"com.myapp..one">>, 0},
        {<<"com.myapp.event.">>, 0},
        {<<"com.myapp.event.one">>, 0},
        {{<<"...">>, <<"suffix">>}, 0},
        {{<<"...one">>, <<"suffix">>}, 0},
        {{<<"..event.">>, <<"suffix">>}, 0},
        {{<<"..event.one">>, <<"suffix">>}, 0},
        {{<<".myapp..">>, <<"suffix">>}, 0},
        {{<<".myapp..one">>, <<"suffix">>}, 0},
        {{<<".myapp.event.">>, <<"suffix">>}, 0},
        {{<<".myapp.event.one">>, <<"suffix">>}, 0},
        {{<<"com...">>, <<"suffix">>}, 0},
        {{<<"com...one">>, <<"suffix">>}, 0},
        {{<<"com..event.">>, <<"suffix">>}, 0},
        {{<<"com..event.one">>, <<"suffix">>}, 0},
        {{<<"com.myapp..">>, <<"suffix">>}, 0},
        {{<<"com.myapp..one">>, <<"suffix">>}, 0},
        {{<<"com.myapp.event.">>, <<"suffix">>}, 0},
        {{<<"com.myapp.event.one">>, <<"suffix">>}, 0}
    ],
    Expected = ordsets:from_list(Expected0),

    ?assertEqual(Expected, lists:sort(Expected0), "no duplicates"),


    art:to_dot(T, "../../../../_out/find_wildcards_2.dot"),

    Result0 = art:find_matches(<<"com.myapp.event.one">>, T),
    Result = ordsets:from_list(Result0),

    ?assertEqual(Result, lists:sort(Result0), "no duplicates"),

    %% Relaxed condition: allowing duplicates
    ?assertEqual(
        ordsets:to_list(Expected),
        ordsets:to_list(Result),
        {missing, ordsets:subtract(Expected, Result)}
    ),
    %% Strict condition
    ?assertEqual(Expected, Result, "Not strictly correct, we have duplicates").


find_matches_exact(_Config) ->
    T = art:new(),
    A = {
        <<"com.leapsight.test.com.a">>,
        <<"bondy2@127.0.0.1">>,
        <<"1">>,
        <<"1">>
    },
    B = {
        <<"com.leapsight.test.com.a.b">>,
        <<"bondy2@127.0.0.1">>,
        <<"1">>,
        <<"1">>
    },
    C = {
        <<"com.leapsight.test.com.a.b.c">>,
        <<"bondy2@127.0.0.1">>,
        <<"1">>,
        <<"1">>
    },

    art:store(A, a, T),
    art:store(B, b, T),
    art:store(C, c, T),

    art:to_dot(T, "../../../../_out/find_matches_exact.dot"),

    a = art:get(A, T),

    ?assertEqual({ok, a}, art:find(A, T)),

    ?assertEqual(
        [{A, a}],
        art:find_matches(<<"com.leapsight.test.com.a">>, T)
    ),
    ?assertEqual(
        [{A, a}],
        art:find_matches(
            <<"com.leapsight.test.com.a">>,
            #{first => <<"com.leapsight.test.">>},
            T
        )
    ),

    % ?TRACE([
    %     {art_find_matches, '_'},
    %     {art_node, longest_common_prefix}
    % ]),
    ?assertEqual(b, art:get(B, T)),
    ?assertEqual({ok, b}, art:find(B, T)),
    ?assertEqual(
        sets:from_list([{B, b}]),
        sets:from_list(art:find_matches(<<"com.leapsight.test.com.a.b">>, T))
    ),
    ?assertEqual(
        [{B, b}],
        art:find_matches(<<"com.leapsight.test.com.a.b">>, T),
        "We have duplicates"
    ),
    ?assertEqual(c, art:get(C, T)),
    ?assertEqual({ok, c}, art:find(C, T)),
    ?assertEqual(
        [{C, c}],
        art:find_matches(<<"com.leapsight.test.com.a.b.c">>, T)
    ),
    ?assertEqual(
        [],
        art:find_matches(<<"com.leapsight.test.com.a.b.c.d">>, T)
    ),
    ets:delete(T).


find_matches_prefix(_Config) ->
    T = art:new(),
    A = {
        <<"com.leapsight.test.a*">>,
        <<"bondy2@127.0.0.1">>,
        <<"1">>,
        <<"1">>
    },
    B = {
        <<"com.leapsight.test.ab*">>,
        <<"bondy2@127.0.0.1">>,
        <<"1">>,
        <<"1">>
    },
    C = {
        <<"com.leapsight.test.abc*">>,
        <<"bondy2@127.0.0.1">>,
        <<"1">>,
        <<"1">>
    },
    D = {
        <<"com.leapsight.test.abcd*">>,
        <<"bondy2@127.0.0.1">>,
        <<"1">>,
        <<"1">>
    },
    art:store(A, a, T),
    art:store(B, b, T),
    art:store(C, c, T),
    art:store(D, d, T),

    art:to_dot(T, "../../../../_out/find_matches_prefix.dot"),

    % ?TRACE([
    %     {art_find_matches, '_'},
    %     {art_node, longest_common_prefix}
    % ]),

    ?assertEqual(
        [{A, a}],
        art:find_matches(<<"com.leapsight.test.a">>, T)
    ),

    ?assertEqual(
        [{A, a}, {B, b}],
        art:find_matches(<<"com.leapsight.test.ab">>, T)
    ),
    ?assertEqual(
        [{A, a}, {B, b}, {C, c}],
        art:find_matches(<<"com.leapsight.test.abc">>, T)
    ),
    ?assertEqual(
        [{A, a}, {B, b}, {C, c}, {D, d}],
        art:find_matches(<<"com.leapsight.test.abcd">>, T)
    ),
    ?assertEqual(
        [{A, a}, {B, b}, {C, c}, {D, d}],
        art:find_matches(<<"com.leapsight.test.abcde">>, T)
    ),
    ?assertEqual(
        [{A, a}, {B, b}, {C, c}, {D, d}],
        art:find_matches(<<"com.leapsight.test.abcdef">>, T)
    ),
    ets:delete(T).


find_matches_1(Config) ->

    T = ?config(trie, Config),

    Expected0 = ordsets:from_list([
        {{<<"*">>, <<"1">>, <<"bar">>}, prefix_1},
        {{<<"c*">>, <<"1">>, <<"a">>}, <<"bar">>},
        {{<<"c*">>, <<"1">>, <<"b">>}, <<"bar">>},
        {{<<"c*">>, <<"2">>, <<"a">>}, <<"bar">>},
        {<<"*">>, <<"prefix">>},
        {<<"c*">>, <<"bar">>},
        {<<"com*">>, <<"prefix">>},
        {<<"com.my*">>, <<"prefix">>},
        {<<"com.myapp*">>, <<"prefix">>},
        {<<"com.myapp.event*">>, <<"prefix">>},
        {<<"com.myapp.event.one">>, <<"full">>}
    ]),

    art:to_dot(T, "../../../../_out/find_matches_1.dot"),


    Results = ordsets:from_list(art:find_matches(<<"com.myapp.event.one">>, T)),

    ExpectedWilcards = ordsets:from_list([
        {<<"...">>, <<"wild">>},
        {<<"..event.one">>, <<"wild">>},
        {<<".myapp.event.one">>, <<"wild">>}
    ]),

    ?assertEqual(
        true,
        ordsets:is_subset(ExpectedWilcards, Results),
        {missing, ordsets:subtract(ExpectedWilcards, Results)}
    ),

    ?assertEqual(
        true,
        ordsets:is_subset(Expected0, Results)
    ).


find_matches_2(Config) ->
    T = ?config(trie, Config),
    Expected = lists:sort([
        {{<<"*">>, <<"1">>, <<"bar">>}, prefix_1},
        {{<<"c*">>, <<"1">>, <<"a">>}, <<"bar">>},
        {{<<"c*">>, <<"1">>, <<"b">>}, <<"bar">>},
        {{<<"c*">>, <<"2">>, <<"a">>}, <<"bar">>},
        {<<"*">>, <<"prefix">>},
        {<<"c*">>, <<"bar">>},
        {<<"com*">>, <<"prefix">>},
        {<<"com.my*">>, <<"prefix">>},
        {<<"com.myapp*">>, <<"prefix">>},
        {<<"com..event">>, <<"wildcard">>},
        {<<"com.myapp.event">>, <<"full">>},
        {<<"com.myapp.event*">>, <<"prefix">>}
    ]),
    Result = lists:sort(art:find_matches(<<"com.myapp.event">>, T)),

    ExpectedWilcards = [{<<"com..event">>, <<"wildcard">>}],
    ?assertEqual(
        true,
        sets:is_subset(sets:from_list(ExpectedWilcards), sets:from_list(Result))
    ),

    ?assertEqual(
        ordsets:from_list(Expected),
        ordsets:from_list(Result)
    ),

    ?assertEqual(
        true,
        ordsets:is_subset(
            ordsets:from_list(Expected),
            ordsets:from_list(Result)
        )
    ).


find_matches_3(Config) ->
    dbg:stop(),
    T = ?config(trie, Config),
    L = [
        {<<"*">>,<<"prefix">>},
        {{<<"*">>, <<"1">>, <<"bar">>}, prefix_1},
        {{<<"f*">>, <<"2">>, <<"bar">>}, prefix_2},
        {{<<"fo*">>, <<"3">>, <<"bar">>}, prefix_3},
        {{<<"foo*">>, <<"4">>, <<"bar">>}, {4, prefix}},
        {{<<"foo">>, <<"1">>, <<"bar">>}, 1},
        {{<<"foo">>, <<"2">>, <<"bar">>}, 2},
        {{<<"foo">>, <<"3">>, <<"bar">>}, 3},
        {{<<"foo">>, <<"4">>, <<"bar">>}, 4}
    ],
    art:to_dot(T, "../../../../_out/find_matches_3.dot"),

    ?assertEqual(
        lists:sort(L),
        lists:sort(art:find_matches(<<"foo">>, T))
    ).


find_matches_4(Config) ->
    dbg:stop(),
    T = ?config(trie, Config),


    _ = art_server:set({<<"com.leapsight.test.com.example.add2">>,<<"bondy2@127.0.0.1">>,
    integer_to_binary(8434920409082193), integer_to_binary(5964782119195992)},<<"entry_key_here">>, T),


    _ = art_server:set({<<"com.leapsight.test.com.example.add2">>,<<"bondy2@127.0.0.1">>,
    integer_to_binary(8434920409082194), integer_to_binary(5964782119195993)},<<"entry_key_here">>, T),

    art:to_dot(T, "../../../../_out/find_matches_4.dot"),


    Expected = [
        {<<"*">>,<<"prefix">>},
        {{<<"*">>,<<"1">>,<<"bar">>},prefix_1},
        {<<"c*">>,<<"bar">>},
        {{<<"c*">>,<<"1">>,<<"a">>},<<"bar">>},
        {{<<"c*">>,<<"1">>,<<"b">>},<<"bar">>},
        {{<<"c*">>,<<"2">>,<<"a">>},<<"bar">>},
        {<<"com*">>,<<"prefix">>},
        {<<".....">>, <<"wild">>},
        {{<<"com.leapsight.test.com.example.add2">>,
        <<"bondy2@127.0.0.1">>,<<"8434920409082193">>,
        <<"5964782119195992">>},
        <<"entry_key_here">>},
        {{<<"com.leapsight.test.com.example.add2">>,
        <<"bondy2@127.0.0.1">>,<<"8434920409082194">>,
        <<"5964782119195993">>},
        <<"entry_key_here">>}
    ],

    Result = art:find_matches(<<"com.leapsight.test.com.example.add2">>, T),

    ?assertEqual(
        lists:sort(Expected),
        lists:sort(Result)
    ).



find_matches_5(Config) ->
    T = ?config(trie, Config),
    _ = art_server:set({<<"com.leapsight.test.com.example.add2">>,<<"bondy2@127.0.0.1">>,
    integer_to_binary(8434920409082193), integer_to_binary(5964782119195992)},<<"entry_key_here">>, T),

    _ = art_server:set({<<"com.leapsight.test.com.example.add2">>,<<"bondy2@127.0.0.1">>,
    integer_to_binary(8434920409082194), integer_to_binary(5964782119195993)},<<"entry_key_here">>, T),


    L1 = [
        {{<<"com.leapsight.test.com.example.add2">>,
        <<"bondy2@127.0.0.1">>,<<"8434920409082193">>,
        <<"5964782119195992">>},
        <<"entry_key_here">>},
        {{<<"com.leapsight.test.com.example.add2">>,
        <<"bondy2@127.0.0.1">>,<<"8434920409082194">>,
        <<"5964782119195993">>},
        <<"entry_key_here">>}
    ],
    Opts = #{match_spec => [
        {
            {{'_', '$1', '_', '_'}, '_'},
            [{'==', '$1', {const, <<"bondy2@127.0.0.1">>}}],
            ['$_']
        }
    ]},
    Result = art:find_matches(
        <<"com.leapsight.test.com.example.add2">>, Opts, T
    ),
    ?assertEqual(
        lists:sort(L1),
        lists:sort(Result)
    ).



find_matches_6(_) ->
    T = art:new(),
    A = {
        <<"com.leapsight.test.com.example.topic1">>,
        <<"bondy2@127.0.0.1">>,
        <<"6214882136353216">>,
        <<"10000000000000">>
    },
    B = {
        <<"com.leapsight.test.com.example.topic1">>,
        <<"bondy2@127.0.0.1">>,
        <<"1144829057325628">>,
        <<"20000000000000">>
    },
    C = {
        <<"com.leapsight.test.com.example.topic2">>,
        <<"bondy2@127.0.0.1">>,
        <<"1144829057325628">>,
        <<"30000000000000">>
    },
    X = {
        <<"com.leapsight.test.com.example.topic1*">>,
        <<"bondy2@127.0.0.1">>,
        <<"6214882136353216">>,
        <<"40000000000000">>
    },
    art:store(A, a, T),
    art:store(B, b, T),
    art:store(C, c, T),
    art:store(X, x, T),

    %% art:to_dot(T, "../../../../_out/find_matches_6.dot"),

    ?assertEqual(
        lists:sort([{B, b}, {A, a}, {C, c}, {X, x}]),
        lists:sort(art:match(<<"com.leapsight.test.com.example.topic">>, T))
    ),

    ?assertEqual(
        [{C, c}],
        art:match(<<"com.leapsight.test.com.example.topic2">>, T)
    ),
    ?assertEqual(
        sets:from_list([{X, x}, {B, b}, {A, a}]),
        sets:from_list(
            art:find_matches(<<"com.leapsight.test.com.example.topic1">>, T)
        )
    ),

    ?assertEqual(
        lists:sort([{X, x}, {B, b}, {A, a}]),
        lists:sort(
            art:find_matches(<<"com.leapsight.test.com.example.topic1">>, T)
        ),
        "We have duplicates"
    ),


    art:to_dot(T, "../../../../_out/find_matches_6.dot"),

    ?assertEqual(
        [{C, c}],
        art:find_matches(<<"com.leapsight.test.com.example.topic2">>, T)
    ),
    ?assertEqual(
        [{X, x}],
        art:find_matches(<<"com.leapsight.test.com.example.topic10">>, T)
    ),


    D = {
        <<"com.leapsight.test.com.example.topic10">>,
        <<"bondy2@127.0.0.1">>,
        <<"1144829057325628">>,
        <<"7981972733622856">>
    },
    E = {
        <<"com.leapsight.test.com.example.">>,
        <<"bondy2@127.0.0.1">>,
        <<"1144829057325628">>,
        <<"7981972733622856">>
    },
    F = {
        <<"com.leapsight.test.com..topic1">>,
        <<"bondy2@127.0.0.1">>,
        <<"1144829057325628">>,
        <<"7981972733622856">>
    },
    G = {
        <<"com.leapsight.test...topic1">>,
        <<"bondy2@127.0.0.1">>,
        <<"1144829057325628">>,
        <<"7981972733622856">>
    },
    H = {
        <<"com.leapsight.test...">>,
        <<"bondy2@127.0.0.1">>,
        <<"1144829057325628">>,
        <<"7981972733622856">>
    },
    I = {
        <<".....">>,
        <<"bondy2@127.0.0.1">>,
        <<"1144829057325628">>,
        <<"7981972733622856">>
    },
    %% The following should not match
    J = {
        <<"com.leapsight.test....">>,
        <<"bondy2@127.0.0.1">>,
        <<"1144829057325628">>,
        <<"7981972733622856">>
    },
    K = {
        <<"...">>,
        <<"bondy2@127.0.0.1">>,
        <<"1144829057325628">>,
        <<"7981972733622856">>
    },

    art:store(D, d, T),
    art:store(E, e, T),
    art:store(F, f, T),
    art:store(G, g, T),
    art:store(H, h, T),
    art:store(I, i, T),
    art:store(J, j, T),
    art:store(K, k, T),



    ?assertEqual(
        [{B, b}, {A, a}, {X, x}, {D, d}, {C, c}],
        art:match(
            <<"com.leapsight.test.com.example.topic">>, T
        )
    ),

    ?assertEqual(
        [{C, c}],
        art:match(<<"com.leapsight.test.com.example.topic2">>, T)
    ),

    Expected = ordsets:from_list([
        {X, x}, {B, b}, {A, a},
        %% wildcards
        {F, f}, {E, e}, {G, g}, {H, h}, {I, i}
    ]),

    % dbg:tracer(),
    % dbg:p(all, c),
    % dbg:tpl(art_find_matches, 'find_wildcards', []),
    % dbg:tpl(art_find_matches, 'find_wildcards_aux', []),
    % dbg:tpl(art_find_matches, 'search_wildcard_child', x),
    % dbg:tpl(art_find_matches, 'maybe_add_wildcard', x),
    % dbg:tpl(art_find_matches, 'do_fold', []),
    % dbg:tpl(art_find_matches, 'find_wildcard_suffixes', []),
    % dbg:tpl(art_find_matches, 'subsumes', []),
    % dbg:tpl(art_find_matches, 'mayb_run_ms', x),
    % dbg:tpl(art_utils, 'index', x),
    % dbg:tpl(art_node, 'longest_common_prefix', x),
    % dbg:tpl(art_utils, 'is_pattern', x),

    Results = ordsets:from_list(
        art:find_matches(
            <<"com.leapsight.test.com.example.topic1">>, T
        )
    ),

    ?assertEqual(
        Expected,
        Results,
        {missing, ordsets:subtract(Expected, Results)}
    ),

    ?assertEqual(
        lists:sort([{I, i}, {H, h}, {C, c}, {E, e}]),
        lists:sort(
            art:find_matches(<<"com.leapsight.test.com.example.topic2">>, T)
        )
    ),

    ?assertEqual(
        lists:sort([{X, x}, {D, d}, {E, e}, {I, i}, {H, h}]),
        lists:sort(
            art:find_matches(<<"com.leapsight.test.com.example.topic10">>, T)
        )
    ),

    % ?assertEqual(
    %     [{X, x}, {D, d}, {I, i}, {H, h}],
    %     art:find_matches(<<"com.leapsight.test.com.example.topic10">>, T)
    % ),

    ?assertEqual([], art:find_matches(<<"wamp">>, T)),

    ok = art:delete(A, T),
    ok = art:delete(B, T),
    ok = art:delete(C, T),
    ok = art:delete(D, T),
    ok = art:delete(E, T),
    ok = art:delete(F, T),
    ok = art:delete(G, T),
    ok = art:delete(H, T),
    ok = art:delete(I, T),
    ok = art:delete(J, T),
    ok = art:delete(K, T),
    ok = art:delete(X, T),

    ?assertEqual([], art:to_list(T)),

    ?assertEqual(0, art:size(T)).


find_matches_7(_) ->
    T = art:new(),
    L1 = [

        {
            {<<"com.leapsight.test.com.example.*">>,
            <<"2FWIrq">>},
            1
        },
        {{<<"com.leapsight.test.com.example.*">>,
        <<"0YVKhW">>},
        2
        }
        % ,{
        %     {<<"com.leapsight.test.com.example">>, <<"0YVKhW">>},
        %     7
        % }
        ,{
            {<<"com.leapsight.test.com.example.test">>, <<"FULL">>},
            1000
        }
        ,{
            {<<"c*">>, <<"2FWJJK">>},
            3
        }
        ,{
            {<<"com*">>, <<"2FWIrq">>},
            3
        }
        ,{
            {<<"com.leapsight.*">>,

            <<"0YVKhW">>},
            4
        }
        ,{
            {<<"com.leapsight.*">>,

            <<"2FWIrq">>},
            5
        }
        ,{
            {<<"com.leapsight.*">>,

            <<"2FXXX">>},
            6
        }
        % ,
        % {{<<"com.leapsight.test.com.example.e*">>,
        % <<"0YVKhW">>,
        % <<"9999999999999999">>},
        % {entry_key,<<"com.leapsight.test">>,
        %         {pid,<<"<0.30585.0>">>},
        %         <<"0YVKhW">>,4529485780405916,false}
        % }
    ],
    L2 =[
        {{<<"com.leapsight.bondy.bondy.realm.deleted">>,
        <<"2FWIrq">>},
        100},
        {{<<"com.leapsight.bondy.bondy.realm.deleted">>,
        <<"0YVKhW">>},
        200}
    ],

    _ = [art:store(K, V, T) || {K, V} <- L1 ++ L2],

    art:to_dot(T, "../../../../_out/find_matches_7.dot"),

    ?assertEqual(lists:sort(L1 ++ L2), lists:sort(art:to_list(T))),

    % ?TRACE([
    %     {art_find_matches, '_'},
    %     {art_node, longest_common_prefix},
    %     {art_utils, index}
    % ]),

    ?assertEqual(
        lists:sort(L1),
        lists:sort(
            art:find_matches(<<"com.leapsight.test.com.example.test">>, T)
        )
    ).



find_matches_8(_) ->
    T = art:new(),
    L1 = [
        {{<<"*">>, <<"1">>}, 1},
        {{<<"a*">>, <<"3">>}, 1},
        {{<<"a.*">>, <<"4">>}, 1},
        {{<<"a.b*">>, <<"6">>}, 1},
        {{<<"a.b.*">>, <<"8">>}, 1},
        {{<<"a.b.c*">>, <<"10">>}, 1},
        {{<<"a.b.c.*">>, <<"12">>}, 1},
        {{<<"a.b.c.d*">>, <<"14">>}, 1},
        {{<<"a.b.c.d.*">>, <<"16">>}, 1},
        {{<<"a.b.c.d.e*">>, <<"18">>}, 1},
        {{<<"a.b.c.d.e.*">>, <<"20">>}, 1},
        {{<<"a.b.c.d.e.f*">>, <<"22">>}, 1},
        {{<<"a.b.c.d.e.f.*">>, <<"24">>}, 1},
        {{<<"a.b.c.d.e.f.g*">>, <<"26">>}, 1},
        {{<<"a.b.c.d.e.f.g.">>, <<"27">>}, 1},
        {{<<"a.b.c.d.e.f.g.*">>, <<"28">>}, 1},
        {{<<"a.b.c.d.e.f.g.h">>, <<"29">>}, 1},
        {{<<"a.b.c.d.e.f.g.h*">>, <<"30">>}, 1}
    ],
    L2 = [
        {{<<"a">>, <<"2">>}, 1},
        {{<<"a.b">>, <<"5">>}, 1},
        {{<<"a.b.">>, <<"7">>}, 1},
        {{<<"a.b.c">>, <<"9">>}, 1},
        {{<<"a.b.c.">>, <<"11">>}, 1},
        {{<<"a.b.c.d">>, <<"13">>}, 1},
        {{<<"a.b.c.d.">>, <<"15">>}, 1},
        {{<<"a.b.c.d.e">>, <<"17">>}, 1},
        {{<<"a.b.c.d.e.">>, <<"19">>}, 1},
        {{<<"a.b.c.d.e.f">>, <<"21">>}, 1},
        {{<<"a.b.c.d.e.f.g">>, <<"25">>}, 1},
        {{<<"a.b.c.d.e.f.g.h.">>, <<"31">>}, 1},
        {{<<"a.b.c.d.e.f.g.h.*">>, <<"32">>}, 1},
        {{<<"a.b.c.d.e.f.">>, <<"23">>}, 1}
    ],
    All = L1 ++ L2,

    _ = [art:store(K, V, T) || {K, V} <- All],

    art:to_dot(T, "../../../../_out/find_matches_8.dot"),

    ?assertEqual(lists:sort(All), lists:sort(art:to_list(T))),

    ?assertEqual(
        lists:sort(L1),
        lists:sort(
            art:find_matches(<<"a.b.c.d.e.f.g.h">>, T)
        )
    ).



find_matches_9(_) ->
    T = art:new(),
    L1 = [
        {<<"123456789abc*">>, 1},
        {{<<"123456789abc*">>, <<"bar1">>}, bar},
        {{<<"123456789abc*">>, <<"bar2">>}, bar},
        {{<<"123456789abc*">>, <<"foo1">>}, foo},
        {{<<"123456789abc*">>, <<"foo2">>}, foo},
        {{<<"123456789abc*">>, <<"foo3">>}, foo}
    ],
    L2 = [
        {<<"987654321abc*">>, 1},
        {{<<"987654321abc*">>, <<"bar1">>}, bar},
        {{<<"987654321abc*">>, <<"bar2">>}, bar},
        {{<<"987654321abc*">>, <<"foo1">>}, foo},
        {{<<"987654321abc*">>, <<"foo2">>}, foo},
        {{<<"987654321abc*">>, <<"foo3">>}, foo}
    ],

    All = L1 ++ L2,
    _ = [art:store(K, V, T) || {K, V} <- All],

    art:to_dot(T, "../../../../_out/find_matches_9.dot"),

    ?assertEqual(lists:sort(All), lists:sort(art:to_list(T))),

    ?assertEqual(
        lists:sort(L1),
        lists:sort(
            art:find_matches(<<"123456789abcMORE">>, T)
        )
    ).


find_matches_10(_) ->
    T = art:new(),
    E = [
        {<<"*">>, test},
        {<<"...">>, test},
        {<<"...one">>, test},
        {<<"..event.">>, test},
        {<<"..event.one">>, test},
        {<<".myapp..">>, test},
        {<<".myapp..one">>, test},
        {<<".myapp.event.">>, test},
        {<<".myapp.event.one">>, test},
        {<<"c*">>, test},
        {<<"co*">>, test},
        {<<"com*">>, test},
        {<<"com.*">>, test},
        {<<"com...">>, test},
        {<<"com...one">>, test},
        {<<"com..event.">>, test},
        {<<"com..event.one">>, test},
        {<<"com.m*">>, test},
        {<<"com.my*">>, test},
        {<<"com.mya*">>, test},
        {<<"com.myap*">>, test},
        {<<"com.myapp*">>, test},
        {<<"com.myapp.*">>, test},
        {<<"com.myapp..">>, test},
        {<<"com.myapp..one">>, test},
        {<<"com.myapp.e*">>, test},
        {<<"com.myapp.ev*">>, test},
        {<<"com.myapp.eve*">>, test},
        {<<"com.myapp.even*">>, test},
        {<<"com.myapp.event*">>, test},
        {<<"com.myapp.event.">>, test},
        {<<"com.myapp.event.*">>, test},
        {<<"com.myapp.event.o*">>, test},
        {<<"com.myapp.event.on*">>, test},
        {<<"com.myapp.event.one">>, test},
        {<<"com.myapp.event.one*">>, test},
        {{<<"*">>, <<"more">>}, test},
        {{<<"...">>, <<"more">>}, test},
        {{<<"...one">>, <<"more">>}, test},
        {{<<"..event.">>, <<"more">>}, test},
        {{<<"..event.one">>, <<"more">>}, test},
        {{<<".myapp..">>, <<"more">>}, test},
        {{<<".myapp..one">>, <<"more">>}, test},
        {{<<".myapp.event.">>, <<"more">>}, test},
        {{<<".myapp.event.one">>, <<"more">>}, test},
        {{<<"c*">>, <<"more">>}, test},
        {{<<"co*">>, <<"more">>}, test},
        {{<<"com*">>, <<"more">>}, test},
        {{<<"com.*">>, <<"more">>}, test},
        {{<<"com...">>, <<"more">>}, test},
        {{<<"com...one">>, <<"more">>}, test},
        {{<<"com..event.">>, <<"more">>}, test},
        {{<<"com..event.one">>, <<"more">>}, test},
        {{<<"com.m*">>, <<"more">>}, test},
        {{<<"com.my*">>, <<"more">>}, test},
        {{<<"com.mya*">>, <<"more">>}, test},
        {{<<"com.myap*">>, <<"more">>}, test},
        {{<<"com.myapp*">>, <<"more">>}, test},
        {{<<"com.myapp.*">>, <<"more">>}, test},
        {{<<"com.myapp..">>, <<"more">>}, test},
        {{<<"com.myapp..one">>, <<"more">>}, test},
        {{<<"com.myapp.e*">>, <<"more">>}, test},
        {{<<"com.myapp.ev*">>, <<"more">>}, test},
        {{<<"com.myapp.eve*">>, <<"more">>}, test},
        {{<<"com.myapp.even*">>, <<"more">>}, test},
        {{<<"com.myapp.event*">>, <<"more">>}, test},
        {{<<"com.myapp.event.">>, <<"more">>}, test},
        {{<<"com.myapp.event.*">>, <<"more">>}, test},
        {{<<"com.myapp.event.o*">>, <<"more">>}, test},
        {{<<"com.myapp.event.on*">>, <<"more">>}, test},
        {{<<"com.myapp.event.one">>, <<"more">>}, test},
        {{<<"com.myapp.event.one*">>, <<"more">>}, test}
    ],


    _ = [art:store(K, V, T) || {K, V} <- E],
    art:to_dot(T, "../../../../_out/find_matches_10.dot"),

    ?assertEqual(
        lists:sort(E),
        lists:sort(
            art:find_matches(<<"com.myapp.event.one">>, T)
        )
    ).



find_matches_11(_) ->
    T = art:new(),
    L = [
        {{<<"com.leapsight.test.com.example.*">>, <<"1">>}, foo},
        {{<<"com.leapsight.test.com.example.*">>, <<"2">>}, foo}
    ],

    _ = [art:store(K, V, T) || {K, V} <- L],

    art:to_dot(T, "../../../../_out/find_matches_11.dot"),

    ?assertEqual(lists:sort(L), lists:sort(art:to_list(T))),

    ?assertEqual(
        lists:sort(L),
        lists:sort(
            art:find_matches(<<"com.leapsight.test.com.example.event">>, T)
        )
    ).


find_matches_12(_) ->
    T = art:new(),
    L = [
        {{<<"com.foobar.com.foo*">>,
        <<"runner@ale-macbook-2020">>,
        <<"7017622653535683">>,
        <<"3YYQEMrM2KST3L1X1y6DcF7h64H">>,
        <<"2357517347005633">>},
        prefix},
        {{<<"com.foobar.com.a.b*">>,
        <<"runner@ale-macbook-2020">>,
        <<"7017622653535683">>,
        <<"3YYQEMrM2KST3L1X1y6DcF7h64H">>,
        <<"8823230168273796">>},
        prefix},
        {{<<"com.foobar.com.a*">>,
        <<"runner@ale-macbook-2020">>,
        <<"7017622653535683">>,
        <<"3YYQEMrM2KST3L1X1y6DcF7h64H">>,
        <<"4629364017862840">>},
        prefix},
        {{<<"com.foobar.com....">>,
        <<"runner@ale-macbook-2020">>,
        <<"7017622653535683">>,
        <<"3YYQEMrM2KST3L1X1y6DcF7h64H">>,
        <<"2850046167010160">>},
        wildcard},
        {{<<"com.foobar.com.">>,
        <<"runner@ale-macbook-2020">>,
        <<"7017622653535683">>,
        <<"3YYQEMrM2KST3L1X1y6DcF7h64H">>,
        <<"3206850023236040">>},
        wildcard},
        {{<<"com.foobar..a...">>,
        <<"runner@ale-macbook-2020">>,
        <<"7017622653535683">>,
        <<"3YYQEMrM2KST3L1X1y6DcF7h64H">>,
        <<"2966503598412189">>},
        wildcard},
        {{<<"com.foobar...b..">>,
        <<"runner@ale-macbook-2020">>,
        <<"7017622653535683">>,
        <<"3YYQEMrM2KST3L1X1y6DcF7h64H">>,
        <<"473431362402365">>},
        wildcard},
        {{<<"com.foobar....c.">>,
        <<"runner@ale-macbook-2020">>,
        <<"7017622653535683">>,
        <<"3YYQEMrM2KST3L1X1y6DcF7h64H">>,
        <<"2381535752419713">>},
        wildcard},
        {{<<"com.foobar.....d">>,
        <<"runner@ale-macbook-2020">>,
        <<"7017622653535683">>,
        <<"3YYQEMrM2KST3L1X1y6DcF7h64H">>,
        <<"842996519284245">>},
        wildcard},
        {{<<"com.foobar.....">>,
        <<"runner@ale-macbook-2020">>,
        <<"7017622653535683">>,
        <<"3YYQEMrM2KST3L1X1y6DcF7h64H">>,
        <<"3559550539960560">>},
        wildcard}
        ],

        _ = [art:store(K, V, T) || {K, V} <- L],
        art:to_dot(T, "../../../../_out/find_matches_12.dot"),

    Expected = lists:sort([
        {<<"com.foobar.....">>, wildcard},
        {<<"com.foobar.com....">>, wildcard},
        {<<"com.foobar.....d">>, wildcard},
        {<<"com.foobar..a...">>, wildcard},
        {<<"com.foobar...b..">>, wildcard},
        {<<"com.foobar....c.">>, wildcard},
        {<<"com.foobar.com.a*">>, prefix},
        {<<"com.foobar.com.a.b*">>, prefix}
    ]),

    Results0 = lists:sort(
        art:find_matches(
            <<"com.foobar.com.a.b.c.d">>, #{first => <<"com.foobar.">>}, T
        )
    ),
    Results = [{X, Y} || {{X, _, _, _, _}, Y} <- Results0],

    ?assertEqual(
        Expected,
        Results
    ).