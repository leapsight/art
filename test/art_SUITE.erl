-module(art_SUITE).
-include_lib("common_test/include/ct.hrl").
-include_lib("stdlib/include/assert.hrl").

-compile(export_all).



all() ->
    [
        create,
        store,
        remove_1,
        remove_1_1,
        remove_2,
        prepare_tests,
        match_2,
        match_3,
        match_4,
        match_5,
        concurrency_1,
        fold_test,
        perf_test,
        order
    ].


loop() ->
    receive
        stop -> exit(normal);
        _ -> loop()
    end.

init_per_suite(Config) ->
    %% Pid = spawn(fun loop/0),
    application:ensure_all_started(art),
    {ok, _Pid} = art_server:start(test),
    %% _ = ets:setopts(T, {heir, Pid, []}),
    [{trie, test} | Config].

end_per_suite(Config) ->
    T = ?config(trie, Config),
    %% Pid = ets:info(T, heir),
    %% Pid ! stop,
    art_server:delete(T),
    {save_config, Config}.




%% =============================================================================
%% TESTS
%% =============================================================================



create(Config) ->
    T = ?config(trie, Config),
    0 = art:size(T),
    ok.

store(Config) ->
    T = ?config(trie, Config),
    undefined = art_server:store(<<"a">>, <<"bar">>, T),
    ?assertEqual(<<"bar">>, art:get(<<"a">>, T)),
    ?assertEqual({ok, <<"bar">>}, art:find(<<"a">>, T)),

    undefined = art_server:store(<<"abbbbbbbbb">>, <<"foo">>, T),
    ?assertEqual(<<"foo">>, art:get(<<"abbbbbbbbb">>, T)),
    ?assertEqual({ok, <<"foo">>}, art:find(<<"abbbbbbbbb">>, T)),

    {value, <<"foo">>} = art_server:store(<<"abbbbbbbbb">>, <<"bar">>, T),
    ?assertEqual(<<"bar">>, art:get(<<"abbbbbbbbb">>, T)),
    %% art:to_dot(T, "../../../../_out/test_1.dot"),
    2 = art:size(T),
    {value, <<"bar">>} = art_server:store(<<"abbbbbbbbb">>, <<"bar">>, T),
    ?assertEqual(<<"bar">>, art:get(<<"abbbbbbbbb">>, T)),
    2 = art:size(T),
    undefined = art_server:store(<<"abbbbbbbbc">>, <<"foo">>, T),
    ?assertEqual(<<"foo">>, art:get(<<"abbbbbbbbc">>, T)),
    %% art:to_dot(T, "../../../../_out/test_2.dot"),
    3 = art:size(T),
    undefined = art_server:store(<<"b">>, <<"bar">>, T),
    ?assertEqual(<<"bar">>, art:get(<<"b">>, T)),
    %% art:to_dot(T, "../../../../_out/test_3.dot"),
    4 = art:size(T),

    undefined = art_server:store(<<"com.myapp">>, <<"100">>, T),
    {value, <<"100">>} = art_server:store(<<"com.myapp">>, <<"1">>, T),

    undefined = art_server:store(<<"com.myapp.event">>, <<"2">>, T),
    undefined = art_server:store(<<"com.myapp.event.type">>, <<"3">>, T),
    undefined = art_server:store(<<"com.myapp.event.type.subtype">>, <<"4">>, T),
    undefined = art_server:store(
        <<"com.myapp.event.type.subtype.1234567890">>, <<"5">>, T),
    undefined = art_server:store(
    <<"this.is.a.unique.very.large.key.that.i.am.using">>, <<"6">>, T),
    10 = art:size(T),

    undefined = art_server:store(<<"abbb">>, <<"foo">>, T),
    %% art:to_dot(T, "../../../../_out/test_4.dot"),
    ?assertEqual(<<"foo">>, art:get(<<"abbb">>, T)),

    ?assertEqual(<<"1">>, art:get(<<"com.myapp">>, T)),
    ?assertEqual(<<"2">>, art:get(<<"com.myapp.event">>, T)),
    ?assertEqual(<<"3">>, art:get(<<"com.myapp.event.type">>, T)),
    ?assertEqual(<<"4">>, art:get(<<"com.myapp.event.type.subtype">>, T)),
    ?assertEqual(<<"5">>, art:get(<<"com.myapp.event.type.subtype.1234567890">>, T)),
    <<"6">> = art:get(
        <<"this.is.a.unique.very.large.key.that.i.am.using">>, T),
    error = art:find(<<"com.myapp.event.type.subtype.1234567890.1">>, T),
    error = art:find(<<"com.myapp.event.type.subtype.1">>, T),
    error = art:find(<<"subtype.1234567890">>, T),
    error = art:find(<<"com.myapp.event.type.subtype.12345678901">>, T).


remove_1(_) ->
    T = art:new(),
    undefined = art:store(<<"1">>, <<"bar">>, T),
    undefined = art:store(<<"2">>, <<"bar">>, T),
    undefined = art:store(<<"3">>, <<"bar">>, T),
    2 = length(ets:tab2list(T)),
    3 = art:size(T),
    ok = art:delete(<<"1">>, T),
    2 = art:size(T),
    ok = art:delete(<<"2">>, T),
    %% art:to_dot(T, "../../../../_out/remove_1_1.dot"),
    undefined = art:store(<<"1">>, <<"bar">>, T),
    ok = art:delete(<<"3">>, T),
    ok = art:delete(<<"3">>, T),
    1 = art:size(T),
    ok = art:delete(<<"1">>, T),
    0 = art:size(T),
    [{trie, {_, _}, _, _, 0, undefined}] = ets:tab2list(T).


remove_1_1(_) ->
    T = art:new(),
    undefined = art:store(<<"1">>, <<"bar">>, T),
    undefined = art:store(<<"2">>, <<"bar">>, T),
    undefined = art:store(<<"3">>, <<"bar">>, T),
    3 = art:size(T),
    ok = art:delete(<<"3">>, T),
    ok = art:delete(<<"1">>, T),
    ok = art:delete(<<"2">>, T),
    0 = art:size(T),
    [{trie, {_, _}, _, _, 0, undefined}] = ets:tab2list(T).


remove_2(Config) ->
    T = ?config(trie, Config),
    11 = art_server:size(T),
    <<"foo">> = art:get(<<"abbbbbbbbc">>, T),
    %% art:to_dot(T, "../../../../_out/remove_2_0.dot"),
    {value, <<"bar">>} = art_server:take(<<"a">>, T),
    %% art:to_dot(T, "../../../../_out/remove_2_1.dot"),
    <<"bar">> = art:get(<<"abbbbbbbbb">>, T),
    <<"foo">> = art:get(<<"abbbbbbbbc">>, T),
    {value, <<"foo">>} = art_server:take(<<"abbbbbbbbc">>, T),
    9 = art_server:size(T),
    <<"bar">> = art_server:get(<<"abbbbbbbbb">>, T),
    %% art:to_dot(T, "../../../../_out/remove_2_2.dot"),
    undefined = art_server:store(<<"a">>, <<"bar">>, T),
    10 = art_server:size(T).



%% MATCHING


prepare_tests(Config) ->
    T = ?config(trie, Config),
    undefined = art_server:store(<<"*">>, <<"prefix">>, T),
    undefined = art_server:store(<<"**">>, <<"prefix">>, T),
    undefined = art_server:store(<<"***">>, <<"prefix">>, T),
    undefined = art_server:store(<<"*foo">>, <<"prefix">>, T),
    undefined = art_server:store(<<"*foo*bar">>, <<"prefix">>, T),
    undefined = art_server:store(<<"c*">>, <<"bar">>, T),
    undefined = art_server:store(<<"c**">>, <<"prefix">>, T),
    undefined = art_server:store(<<"c*", $\31, "1", $\31, "a">>, <<"bar">>, T),
    undefined = art_server:store(<<"c*", $\31, "1", $\31, "b">>, <<"bar">>, T),
    undefined = art_server:store(<<"c*", $\31, "2", $\31, "a">>, <<"bar">>, T),
    undefined = art_server:store(<<"com*">>, <<"prefix">>, T),
    undefined = art_server:store(<<"com.my*">>, <<"prefix">>, T),
    undefined = art_server:store(<<"com.myapp*">>, <<"prefix">>, T),
    undefined = art_server:store(<<"com.myapp.event*">>, <<"prefix">>, T),
    undefined = art_server:store(<<"com.myapp.event*.one">>, <<"full">>, T),
    undefined = art_server:store(<<"com.myapp.event.one">>, <<"full">>, T),
    undefined = art_server:store(<<"com.myapp.event.one.more">>, <<"full">>, T),
    undefined = art_server:store(<<"..event.one">>, <<"wild">>, T),
    undefined = art_server:store(<<".myapp.event.one">>, <<"wild">>, T),
    undefined = art_server:store(<<"...">>, <<"wild">>, T),
    undefined = art_server:store(<<".....">>, <<"wild">>, T),
    undefined = art_server:store(<<"com..event">>, <<"wildcard">>, T),

    undefined = art_server:store({<<"foo">>, <<"1">>, <<"bar">>}, 1, T),
    undefined = art_server:store({<<"foo">>, <<"2">>, <<"bar">>}, 2, T),
    undefined = art_server:store({<<"foo">>, <<"3">>, <<"bar">>}, 3, T),
    undefined = art_server:store({<<"foo">>, <<"4">>, <<"bar">>}, 4, T),

    undefined = art_server:store({<<"*">>, <<"1">>, <<"bar">>}, prefix_1, T),
    undefined = art_server:store({<<"f*">>, <<"2">>, <<"bar">>}, prefix_2, T),
    undefined = art_server:store({<<"fo*">>, <<"3">>, <<"bar">>}, prefix_3, T),
    undefined = art_server:store({<<"foo*">>, <<"4">>, <<"bar">>}, {4, prefix}, T),
    %% art:to_dot(T, "../../../../_out/test_full.dot"),
    ok.

match_wilcards(Config) ->
    T = ?config(trie, Config),
    L = lists:sort([
        {<<"com.myapp.event.type">>, <<"3">>},
        {<<"com.myapp.event.type.subtype">>, <<"4">>},
        {<<"com.myapp.event.type.subtype.1234567890">>, <<"5">>}
    ]),

    L = art:match(<<"...">>, T, #{mode => wildcard}),

    %% ones
    L = art:match(<<".myapp..">>, T),
    L = art:match(<<"com...">>, T),
    L = art:match(<<"..event.">>, T),
    L = art:match(<<"...type">>, T),

    %% twos
    L = art:match(<<"com.myapp..">>, T),
    L = art:match(<<"com..event.">>, T),
    L = art:match(<<"com...type">>, T),
    L = art:match(<<".myapp.event.">>, T),
    L = art:match(<<".myapp..type">>, T),
    L = art:match(<<"..event.type">>, T),

    %% threes
    L = art:match(<<".myapp.event.type">>, T),
    L = art:match(<<"com..event.type">>, T),
    L = art:match(<<"com.myapp..type">>, T),
    L = art:match(<<"com.myapp.event.">>, T),

    %% fours
    L = art:match(<<"com.myapp.event.type">>, T).

match_2(Config) ->
    T = ?config(trie, Config),

    ?assertEqual(1, art:get({<<"foo">>, <<"1">>, <<"bar">>}, T)),
    ?assertEqual(2, art:get({<<"foo">>, <<"2">>, <<"bar">>}, T)),
    ?assertEqual(3, art:get({<<"foo">>, <<"3">>, <<"bar">>}, T)),
    ?assertEqual(4, art:get({<<"foo">>, <<"4">>, <<"bar">>}, T)),

    ?assertEqual([], art:match(<<"foo">>, T, #{mode => exact})),
    ?assertEqual([], art:match(<<"foo">>, T, #{mode => exact})),
    ?assertEqual([], art:match({<<"foo">>, '_'}, T, #{mode => exact})),

    Expected0 = [
        {{<<"foo">>, <<"1">>, <<"bar">>}, 1},
        {{<<"foo">>, <<"2">>, <<"bar">>}, 2},
        {{<<"foo">>, <<"3">>, <<"bar">>}, 3},
        {{<<"foo">>, <<"4">>, <<"bar">>}, 4}
    ],

    ?assertEqual(
        Expected0,
        art:match({<<"foo">>, '_', '_'}, T, #{mode => exact})
    ),

    Expected1 = Expected0 ++ [{{<<"foo*">>, <<"4">>,<<"bar">>}, {4, prefix}}],

    ?assertEqual(Expected1, art:match(<<"foo">>, T, #{mode => pattern})),

    ?assertEqual([], art:match({<<"foo">>, <<"1">>, <<"bar">>, <<"more">>}, T)),
    ?assertEqual([], art:match(<<"foo*">>, T, #{mode => exact})),
    ?assertEqual(
        {ok, {4, prefix}},
        art:find({<<"foo*">>, <<"4">>,<<"bar">>}, T)
    ),

    ?assertEqual(
        [{{<<"foo*">>, <<"4">>,<<"bar">>}, {4, prefix}}],
        art:match(<<"foo*">>, T, #{mode => pattern})
    ).

match_3(Config) ->
    T = ?config(trie, Config),

    1 = art:get({<<"foo">>, <<"1">>, <<"bar">>}, T),
    2 = art:get({<<"foo">>, <<"2">>, <<"bar">>}, T),
    3 = art:get({<<"foo">>, <<"3">>, <<"bar">>}, T),
    4 = art:get({<<"foo">>, <<"4">>, <<"bar">>}, T),

    L = [
        {{<<"foo">>, <<"1">>, <<"bar">>}, 1},
        {{<<"foo">>, <<"2">>, <<"bar">>}, 2},
        {{<<"foo">>, <<"3">>, <<"bar">>}, 3},
        {{<<"foo">>, <<"4">>, <<"bar">>}, 4},
        {{<<"foo*">>, <<"4">>,<<"bar">>}, {4, prefix}}
    ],

    [
        {{<<"f*">>,<<"2">>,<<"bar">>},prefix_2},
        {{<<"fo*">>,<<"3">>,<<"bar">>},prefix_3}
        | L
    ] = art:match(<<"f">>, T, #{mode => pattern}),
    [] = art:match(<<"f*">>, T, #{mode => exact}),
    [
        {{<<"f*">>,<<"2">>,<<"bar">>},prefix_2}
    ] = art:match(<<"f*">>, T, #{mode => pattern}),
    [

        {{<<"fo*">>,<<"3">>,<<"bar">>},prefix_3}
        | L
    ] = art:match(<<"fo">>, T, #{mode => pattern}),
    [] = art:match(<<"fo*">>, T, #{mode => exact}),
    [
        {{<<"fo*">>,<<"3">>,<<"bar">>},prefix_3}
    ] = art:match(<<"fo*">>, T , #{mode => pattern}),
    L = art:match(<<"foo">>, T, #{mode => pattern}),
    [] = art:match(<<"foo*">>, T, #{mode => exact}).



match_4(Config) ->
    T = ?config(trie, Config),
    L = [
        {{<<"foo">>, <<"1">>, <<"bar">>}, 1}
    ],
    [] = art:match({<<"f*">>, <<"1">>}, T),
    [] = art:match({<<"fo*">>, <<"1">>}, T),
    [] = art:match({<<"foo*">>, <<"1">>}, T),

    [] = art:match({<<"fo">>, <<"1">>}, T),

    L = art:match({<<"foo">>, <<"1">>}, T, #{mode => pattern}),
    ok.




match_5(_) ->
    T = art:new(),
    A = {
        <<"com.leapsight.test,com.example.add2">>,
        <<"bondy1@127.0.0.1">>,
        <<"6214882136353216">>,
        <<"5140678987571469">>
    },
    B = {
        <<"com.leapsight.test,com.example.add2">>,
        <<"bondy2@127.0.0.1">>,
        <<"1144829057325628">>,
        <<"7981972733622856">>
    },
    C = {
        <<"com.leapsight.test,com.example.add">>,
        <<"bondy1@127.0.0.1">>,
        <<"1144829057325628">>,
        <<"7981972733622856">>
    },
    art:store(A, 1, T),
    art:store(B, 2, T),
    art:store(C, 3, T),
    art:to_dot(T, "../../../../_out/match_5.dot"),

    All = lists:usort([{A, 1}, {B, 2}, {C, 3}]),
    Two = lists:usort([{A, 1}, {B, 2}]),
    All = art:match(<<"com.leapsight.test">>, T, #{mode => pattern}),
    All = art:match(<<"com.leapsight.test,">>, T, #{mode => pattern}),
    All = art:match(<<"com.leapsight.test,com">>, T, #{mode => pattern}),
    Two = art:match(
        <<"com.leapsight.test,com.example.add2">>, T, #{mode => pattern}),
    Two = art:match(
        <<"com.leapsight.test,com.example.add2", $\31>>, T, #{mode => pattern}),


    [] = art:match(
        <<"com.leapsight.test,com.example.add2", $\31, "bondy1">>,
        T,
        #{mode => pattern}
    ),
    [{A, 1}] = art:match(
        <<"com.leapsight.test,com.example.add2", $\31, "bondy1@127.0.0.1">>,
        T,
        #{mode => pattern}
    ),
    [{B, 2}] = art:match(
        <<"com.leapsight.test,com.example.add2", $\31, "bondy2@127.0.0.1">>,
        T,
        #{mode => pattern}
    ),
    Two = art:match(
        {<<"com.leapsight.test,com.example.add2">>}, T,
        #{mode => pattern}
    ),
    Two = art:match(
        {<<"com.leapsight.test,com.example.add2">>, <<>>}, T,
         #{mode => pattern}
    ),

    Two = art:match(
        {<<"com.leapsight.test,com.example.add2">>, <<>>, <<>>},
        T,
        #{mode => pattern}
    ),

    Two = art:match(
        {<<"com.leapsight.test,com.example.add2">>, <<>>, <<>>, <<>>},
        T,
        #{mode => pattern}
    ),

    [{A, 1}] = art:match(
        {<<"com.leapsight.test,com.example.add2">>, <<"bondy1@127.0.0.1">>},
        T,
        #{mode => pattern}
    ),
    [{B, 2}] = art:match(
        {<<"com.leapsight.test,com.example.add2">>, <<"bondy2@127.0.0.1">>},
        T,
        #{mode => pattern}
    ),
    [] = art:match(
        {<<"com.leapsight.test,com.example.add2">>, <<"bondy3@127.0.0.1">>},
        T,
        #{mode => pattern}
    ),


    All = art:match(
        <<"com.leapsight.test,com.example.add">>,
        T,
        #{mode => pattern}
    ),
    [{C, 3}] = art:match(
        {<<"com.leapsight.test,com.example.add">>, <<"bondy1@127.0.0.1">>},
        T,
        #{mode => pattern}
    ),

    [{A, 1}] = art:match(
        {<<"com.leapsight.test,com.example.add2">>, <<"bondy1@127.0.0.1">>, <<"6214882136353216">>}, T,
        #{mode => pattern}
    ),
    [{A, 1}] = art:match(
        {<<"com.leapsight.test,com.example.add2">>, <<"bondy1@127.0.0.1">>, <<"6214882136353216">>, <<"5140678987571469">>}, T,
        #{mode => pattern}
    ),
    [] = art:match(
        {<<"com.leapsight.test,com.example.add2">>, <<"bondy1@127.0.0.1">>, <<"621488213635321Z">>, <<"5140678987571469Z">>}, T,
         #{mode => pattern}
        ),

    [{B, 2}] = art:match(
        {<<"com.leapsight.test,com.example.add2">>, <<"bondy2@127.0.0.1">>, <<"1144829057325628">>}, T,
        #{mode => pattern}
    ),
    [{B, 2}] = art:match(
        {<<"com.leapsight.test,com.example.add2">>, <<"bondy2@127.0.0.1">>, <<"1144829057325628">>, <<"7981972733622856">>}, T,
        #{mode => pattern}
    ),

    [] = art:match(<<"com.leapsight.test,foo">>, T,
        #{mode => pattern}
    ).


order(_) ->

    L = [
        {<<"*">>, <<"prefix">>},
        {<<"**">>, <<"prefix">>},
        {<<"***">>, <<"prefix">>},
        {<<"*foo">>, <<"prefix">>},
        {<<"*foo*bar">>, <<"prefix">>},
        {<<"...">>, <<"wild">>},
        {<<".....">>, <<"wild">>},
        {<<"..event.one">>, <<"wild">>},
        {<<".myapp.event.one">>, <<"wild">>},
        {<<"c*", $\31, "1", $\31, "a">>, <<"bar">>},
        {<<"c*", $\31, "1", $\31, "b">>, <<"bar">>},
        {<<"c*", $\31, "2", $\31, "a">>, <<"bar">>},
        {<<"c*">>, <<"bar">>},
        {<<"c**">>, <<"prefix">>},
        {<<"com*">>, <<"prefix">>},
        {<<"com..event">>, <<"wildcard">>},
        {<<"com.my*">>, <<"prefix">>},
        {<<"com.myapp*">>, <<"prefix">>},
        {<<"com.myapp.event*">>, <<"prefix">>},
        {<<"com.myapp.event*.one">>, <<"full">>},
        {<<"com.myapp.event.one">>, <<"full">>},
        {<<"com.myapp.event.one.more">>, <<"full">>},
        {{<<"*">>, <<"1">>, <<"bar">>}, prefix_1},
        {{<<"f*">>, <<"2">>, <<"bar">>}, prefix_2},
        {{<<"fo*">>, <<"3">>, <<"bar">>}, prefix_3},
        {{<<"foo">>, <<"1">>, <<"bar">>}, 1},
        {{<<"foo">>, <<"2">>, <<"bar">>}, 2},
        {{<<"foo">>, <<"3">>, <<"bar">>}, 3},
        {{<<"foo">>, <<"4">>, <<"bar">>}, 4},
        {{<<"foo*">>, <<"4">>, <<"bar">>}, {4, prefix}}
    ],

    %% Relaxed condition: we are not checking structural equivalence, just that
    %% to_list matches


    Runs = 1000,
    _ = [
        begin
            T0 = art:new(),
            _ = [art:store(K, V, T0) || {K, V} <- L],
            Expected0 = art:to_list(T0),

            T1 = art:new(),
            L1 = shuffle(L),
            _ = [art:store(K, V, T1) || {K, V} <- L1],
            Result0 = art:to_list(T1),

            ?assertEqual(Expected0, Result0),


            [
                begin
                    ?assertNotEqual(undefined, art:get(K, T0), K),
                    art:delete(K, T0),
                    Expected1 = art:to_list(T0),

                    ?assertNotEqual(undefined, art:get(K, T1), K),
                    art:delete(K, T1),
                    Result1 = art:to_list(T1),

                    ?assertEqual(Expected1, Result1)
                end
                ||  {K, _} <- L1
            ]


        end
        || _ <- lists:seq(1, Runs)
    ],
    ok.


concurrency_1(_) ->
    dbg:stop(),
    T = art:new(),
    art:store(<<"abcz">>, 1, T),

    L = [spawn(?MODULE, loop, [T, 0, 0, self()]) || _ <- lists:seq(1, 10)],
    timer:sleep(50),
    art:store(<<"abc">>, 1, T),
    %% art:to_dot(T, "../../../../_out/concurrency_1_1.dot"),
    art:store(<<"ab">>, 1, T),
    %% art:to_dot(T, "../../../../_out/concurrency_1_2.dot"),
    art:store(<<"a">>, 1, T),
    %% art:to_dot(T, "../../../../_out/concurrency_1_3.dot"),
    art:store(<<"b">>, 1, T),
    %% art:to_dot(T, "../../../../_out/concurrency_1_4.dot"),
    art:delete(<<"abc">>, T),
    %% art:to_dot(T, "../../../../_out/concurrency_1_5.dot"),
    art:store(<<"abcz1">>, 1, T),
    art:store(<<"abcz2">>, 1, T),
    art:store(<<"abcz1000">>, 1, T),

    Map = collect_concurrency(L, #{}),
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0] = maps:values(Map),
    ok.

loop(_, Cnt, ErrorCnt, Pid) when Cnt == 1000 ->
    Pid ! {errors, ErrorCnt},
    exit(normal);

loop(T, Cnt, ErrorCnt, Pid) ->
    try art:get(<<"abcz">>, T) of
        1 ->
            loop(T, Cnt + 1, ErrorCnt, Pid)
    catch
        _:_ ->
            loop(T, Cnt + 1, ErrorCnt + 1, Pid)
    end.


collect_concurrency([H|T], Acc) ->
    receive
        {errors, Cnt} ->
            collect_concurrency(T, maps:put(H, Cnt, Acc))
    after 5000 ->
        {error, timeout}
    end;

collect_concurrency([], Acc) ->
    Acc.


%% =============================================================================
%% PERF
%% =============================================================================




perf_test(Config) ->
    T = ?config(trie, Config),
    K = <<"com.myapp.event.type">>,
    Fun = fun(X) ->
        [art:get(K, T) || _ <- lists:seq(min(0, X - 1000), X)]
    end,
    L = [timer:tc(Fun, [X]) || X <- lists:seq(1000, 10000, 1000)],
    N = lists:foldl(
        fun({N, _}, Acc) -> Acc + N end,
        0,
        L
    ),
    Avg = trunc(N / 10 / 1000), % microsecs/op
   ct:pal("~p µs/op => ~p ops/sec~n", [Avg, trunc(1000000/Avg)]),

   ct:pal("Starting parallel fold~n"),
   [
        spawn(
            fun() ->
                M = lists:foldl(
                    fun({M, _}, Acc) -> Acc + M end,
                    0,
                    L
                ),
                Avg = trunc(M / 10 / 1000), % microsecs/op
               ct:pal(
                    "~p µs/op => ~p ops/sec~n", [Avg, trunc(1000000/Avg)]
                )
            end
        ) || _ <- lists:seq(1, 16)
    ],
    {ok, T}.


fold_test(_) ->
    T = art:new(),
    Fun = fun(X, Acc) ->
        Bin = list_to_binary(integer_to_list(X)),
        K = <<"com.myapp.event.type.subtype.", Bin/binary>>,
        ok = art:set(K, Bin, Acc),
        Acc
    end,

    TestFun = fun(Key, Val, Acc) -> [{Key, Val} | Acc] end,
    %% 10000
    {TTime1, T} = timer:tc(lists, foldl, [Fun, T, lists:seq(0, 10000)]),
   ct:pal("Inserting ~p elements took ~p µs~n", [10000, TTime1]),
    {Time1, _} = timer:tc(art, fold, [T, TestFun, []]),
   ct:pal("Folding ~p elements took ~p µs~n", [art:size(T), Time1]),

    %% 10000

    {TTime2, T} = timer:tc(lists, foldl, [Fun, T, lists:seq(10001, 20000)]),
   ct:pal("Inserting ~p elements took ~p µs~n", [10000, TTime2]),
    {Time2, _} = timer:tc(art, fold, [T, TestFun, []]),
   ct:pal("Folding ~p elements took ~p µs~n", [art:size(T), Time2]),

    %% 10000
    {TTime3, T} = timer:tc(lists, foldl, [Fun, T, lists:seq(20001, 30000)]),
   ct:pal("Inserting ~p elements took ~p µs~n", [10000, TTime3]),
    {Time3, _} = timer:tc(art, fold, [T, TestFun, []]),
   ct:pal("Folding ~p elements took ~p µs~n", [art:size(T), Time3]),

    %% 10000
    {TTime4, T} = timer:tc(lists, foldl, [Fun, T, lists:seq(30001, 40000)]),
   ct:pal("Inserting ~p elements took ~p µs~n", [10000, TTime4]),
    {Time4, _} = timer:tc(art, fold, [T, TestFun, []]),
   ct:pal("Folding ~p elements took ~p µs~n", [art:size(T), Time4]),

    %% 10000
    {TTime5, T} = timer:tc(lists, foldl, [Fun, T, lists:seq(40001, 50000)]),
   ct:pal("Inserting ~p elements took ~p µs~n", [10000, TTime5]),
    {Time5, _} = timer:tc(art, fold, [T, TestFun, []]),
   ct:pal("Folding ~p elements took ~p µs~n", [art:size(T), Time5]),

   ct:pal("Starting parallel fold~n"),
    Ts0 = erlang:system_time(millisecond),
    Me = self(),
    Pids = [
        spawn(
            fun() ->
                {Time, Acc} = timer:tc(art, fold, [T, TestFun, []]),
                Me ! {ok, self()},
               ct:pal(
                    "Folding ~p elements took ~p µs~n", [length(Acc), Time])
            end
        ) || _ <- lists:seq(1, 16)
    ],
    ok = collect(Pids),
    Elapsed = erlang:system_time(millisecond) - Ts0,
   ct:pal("100 Parallel folds, took ~p ms~n", [Elapsed]),
    {ok, T}.



collect([H|T]) ->
    receive
        {ok, H} ->
            collect(T)
    after 5000 ->
        {error, timeout}
    end;

collect([]) ->
    ok.


%% @private
shuffle([]) ->
    [];

shuffle(List) ->
    %% Determine the log n portion then randomize the list.
    randomize(round(math:log(length(List)) + 0.5), List).


%% @private
randomize(1, List) ->
    randomize(List);

randomize(T, List) ->
    lists:foldl(
        fun(_E, Acc) -> randomize(Acc) end,
        randomize(List),
        lists:seq(1, (T - 1))).


%% @private
randomize(List) ->
    D = lists:map(fun(A) -> {rand:uniform(), A} end, List),
    {_, D1} = lists:unzip(lists:keysort(1, D)),
    D1.




